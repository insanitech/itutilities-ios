//
//  ITAppDelegate.h
//  TestApp
//
//  Created by Anderson Lucas C. Ramos on 14/04/14.
//
//

#import <UIKit/UIKit.h>

@interface ITAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
