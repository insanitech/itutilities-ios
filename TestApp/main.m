//
//  main.m
//  TestApp
//
//  Created by Anderson Lucas C. Ramos on 14/04/14.
//
//

#import <UIKit/UIKit.h>

#import "ITAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([ITAppDelegate class]));
	}
}
