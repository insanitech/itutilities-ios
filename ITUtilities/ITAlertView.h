//
//  ITAlertView.h
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 5/16/12.
//  Copyright (c) 2012 Tangível Tecnologia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ITAlertView;

typedef void (^ITAlertViewButtonBlock)(ITAlertView *alert, NSInteger buttonIndex);

@interface ITAlertView : UIAlertView

@property (nonatomic, strong, readonly) UITextField	*plainTextInput;
@property (nonatomic, strong, readonly) UITextField *secureTextInput;

- (id)initWithTitle:(NSString *)title 
			message:(NSString *)message 
		   delegate:(id)delegate 
  cancelButtonTitle:(NSString *)cancelButtonTitle 
  otherButtonTitles:(NSArray *)otherButtonTitles
	 alertViewStyle:(UIAlertViewStyle)style;
- (id)initWithTitle:(NSString *)title
			message:(NSString *)message 
  cancelButtonTitle:(NSString *)cancelButtonTitle 
  otherButtonTitles:(NSArray *)otherButtonTitles 
	 alertViewStyle:(UIAlertViewStyle)style 
 buttonClickedBlock:(ITAlertViewButtonBlock)block;

@end
