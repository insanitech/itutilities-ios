//
//  ITBase64.h
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 7/6/12.
//  Copyright (c) 2012 42Tecnologia. All rights reserved.
//

#ifndef ITBASE64_H
#define ITBASE64_H

unsigned char *base64_encode(const unsigned char *str, int length, int *ret_length);
unsigned char *base64_decode(const unsigned char *str, int length, int *ret_length, int strict);

#endif
