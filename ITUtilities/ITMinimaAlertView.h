//
//  ITMinimaAlertView.h
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 11/6/12.
//
//

#import <UIKit/UIKit.h>

@class ITMinimaAlertView;

typedef void (^ITMinimaAlertViewCompletion)(ITMinimaAlertView *alert, BOOL confirm);

@interface ITMinimaAlertView : UIView

@property (nonatomic, strong) UIFont *titleFont;
@property (nonatomic, strong) UIFont *messageFont;
@property (nonatomic, strong) UIFont *buttonsFont;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIColor *backgroundViewColor;
@property (nonatomic, strong) UIColor *buttonsTextColor;
@property (nonatomic, strong) UIColor *cancelButtonColor;
@property (nonatomic, strong) UIColor *confirmButtonColor;
@property (nonatomic, strong) UIColor *buttonsHoverColor;
@property (nonatomic, strong) UIImage *confirmButtonImage;
@property (nonatomic, assign) NSTextAlignment messageTextAlignment;
@property (nonatomic, strong, readonly) UIView *contentView;
@property (nonatomic, assign) UIEdgeInsets buttonsContentInset;
@property (nonatomic, assign) NSInteger numberOfLines;

- (id)initWithTitle:(NSString *)title message:(NSString *)message
  cancelButtonTitle:(NSString *)cancelButtonTitle
 confirmButtonTitle:(NSString *)confirmButtonTitle
		 completion:(ITMinimaAlertViewCompletion)block;
- (void)showInView:(UIView *)view;

@end
