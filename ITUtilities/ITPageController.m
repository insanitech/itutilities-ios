//
//  ITPageController.m
//  SocialVoice
//
//  Created by Anderson Lucas C. Ramos on 11/5/12.
//  Copyright (c) 2012 Anderson Lucas C. Ramos. All rights reserved.
//

#import "ITPageController.h"

@interface ITPageController ()

@property (nonatomic, strong) UIScrollView *pager;
@property (nonatomic, strong) NSMutableDictionary *pageViews;
@property (nonatomic, assign) NSUInteger currentPage;
@property (nonatomic, assign) NSUInteger pageCount;

@end

@implementation ITPageController {
	__unsafe_unretained
	id<ITPageControllerDelegate> _delegate;
	NSMutableDictionary *_pageViews;
	UIScrollView *_pager;
}

@synthesize pager = _pager;
@synthesize delegate = _delegate;
@synthesize pageViews = _pageViews;
@synthesize currentPage;
@synthesize pageCount;
@synthesize inMemoryPages = _inMemoryPages;

- (void)dealloc {
	self.delegate = nil;
	self.pageViews = nil;
	self.pager = nil;
	
	NSLog(@"%@ dealloc", NSStringFromClass([self class]));
}

- (id)initWithScrolView:(UIScrollView *)pager {
	self = [super init];
	if (self != nil) {
		self.currentPage = 0;
		self.pager = pager;
		[self.pager setDelegate:self];
		[self.pager setPagingEnabled:YES];
		self.inMemoryPages = 3;
		
		self.pageViews = [NSMutableDictionary dictionary];
	}
	return self;
}

- (void)setInMemoryPages:(NSUInteger)inMemoryPages {
	_inMemoryPages = inMemoryPages;
	if (_inMemoryPages % 2 == 0) {
		_inMemoryPages++;
	}
}

- (NSUInteger)sidePagesNumber {
	return self.inMemoryPages * 0.5f - 0.5f;
}

- (void)notifyRemoving:(UIViewController *)v {
	if ([self.delegate respondsToSelector:@selector(pageController:willRemovePage:)]) {
		[self.delegate pageController:self willRemovePage:v];
	}
}

- (void)notifyAdding:(UIViewController *)v {
	if ([self.delegate respondsToSelector:@selector(pageController:willAddPage:)]) {
		[self.delegate pageController:self willAddPage:v];
	}
}

- (void)cleanPages:(BOOL)all {
	if (all) {
		for (NSString *key in self.pageViews.allKeys) {
			UIViewController *v = [self.pageViews objectForKey:key];
			[self notifyRemoving:v];
			[v.view removeFromSuperview];
		}
		[self.pageViews removeAllObjects];
	} else {
		NSString *key;
		if ([self.pageViews.allKeys containsObject:key = [NSString stringWithFormat:@"%i", (int)(self.currentPage - [self sidePagesNumber] + 1)]]) {
			UIViewController *v = [self.pageViews objectForKey:key];
			[self notifyRemoving:v];
			[v.view removeFromSuperview];
			[self.pageViews removeObjectForKey:key];
		}
		if ([self.pageViews.allKeys containsObject:key = [NSString stringWithFormat:@"%i", (int)(self.currentPage + [self sidePagesNumber] + 1)]]) {
			UIViewController *v = [self.pageViews objectForKey:key];
			[self notifyRemoving:v];
			[v.view removeFromSuperview];
			[self.pageViews removeObjectForKey:key];
		}
	}
}

- (void)setupViewForPage:(NSUInteger)pageNumber {
	if (pageNumber < self.pageCount &&
		![self.pageViews.allKeys containsObject:[NSString stringWithFormat:@"%i", (int)pageNumber]]) {
		UIViewController *pageView = [self.delegate pageController:self controllerForPage:pageNumber];
		[pageView.view setCenter:(CGPoint) {
			self.pager.frame.size.width * pageNumber + self.pager.frame.size.width * 0.5f,
			self.pager.frame.size.height * 0.5f
		}];
		[self notifyAdding:pageView];
		[self.pager addSubview:pageView.view];
		[self.pageViews setObject:pageView forKey:[NSString stringWithFormat:@"%i", (int)pageNumber]];
	}
}

- (void)loadPages {
	if (self.currentPage > 0 && self.currentPage < self.pageCount - [self sidePagesNumber]) {
		for (NSUInteger i = (self.currentPage - [self sidePagesNumber]); i <= self.currentPage + [self sidePagesNumber]; i++) {
			[self setupViewForPage:i];
		}
	} else if (self.currentPage == 0) {
		for (NSUInteger i = 0; i < (self.pageCount > 1 ? [self sidePagesNumber] + 1 : [self sidePagesNumber]); i++) {
			[self setupViewForPage:i];
		}
	} else {
		for (NSUInteger i = self.currentPage; i > self.currentPage - [self sidePagesNumber] + 1; i--) {
			[self setupViewForPage:i];
		}
	}
}

- (void)reloadData {
	NSUInteger lastPageCount = self.pageCount;
	self.pageCount = 0;
	if ([self.delegate respondsToSelector:@selector(pageControllerNumberOfPages:)] &&
		[self.delegate respondsToSelector:@selector(pageController:controllerForPage:)]) {
		self.pageCount = [self.delegate pageControllerNumberOfPages:self];
		
		[self cleanPages:lastPageCount > self.pageCount];
		
		if (self.pageCount - 1 < self.currentPage) {
			self.currentPage = self.pageCount - 1;
			if (self.currentPage < self.pageCount) {
				if ([self.delegate respondsToSelector:@selector(pageControllerCurrentPageDidChange:)]) {
					[self.delegate pageControllerCurrentPageDidChange:self];
				}
			}
		}
		
		if (self.pageCount > 0) {
			[self.pager setContentSize:(CGSize) { self.pageCount * self.pager.frame.size.width, self.pager.frame.size.height }];
			[self.pager setContentOffset:(CGPoint) { self.currentPage * self.pager.frame.size.width, 0 }];
			
			[self loadPages];
		}
	}
}

- (void)updatePages {
	[self cleanPages:NO];
	[self loadPages];
	NSLog(@"In Memory Page Count: %i", (uint)self.pageViews.allKeys.count);
}

- (float)currentPaging {
	return self.pager.contentOffset.x / self.pager.frame.size.width;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
	if (!decelerate) {
		if (self.currentPage != [self currentPaging]) {
			[self updatePages];
		}
	}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView  {
	if (self.currentPage != [self currentPaging]) {
		[self updatePages];
	}
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	NSUInteger pageInt;
	float page = [self currentPaging] - self.currentPage;
	if (page < -0.5f) {
		pageInt = (int)self.currentPage - 1;
	} else if (page < 0.5f) {
		pageInt = (int)self.currentPage;
	} else {
		pageInt = (int)self.currentPage + 1;
	}
	if (self.currentPage != pageInt) {
		self.currentPage = pageInt;
		if (self.currentPage < self.pageCount) {
			if ([self.delegate respondsToSelector:@selector(pageControllerCurrentPageDidChange:)]) {
				[self.delegate pageControllerCurrentPageDidChange:self];
			}
		}
		[self updatePages];
	}
}

- (void)scrollToPage:(NSUInteger)pageNumber {
	[self scrollToPage:pageNumber animated:NO];
}

- (void)scrollToPage:(NSUInteger)pageNumber animated:(BOOL)animated {
	if (pageNumber < self.pageCount) {
		self.currentPage = pageNumber;
		[self.pager setContentOffset:(CGPoint) { self.currentPage * self.pager.frame.size.width, 0 } animated:animated];
	} else {
		[self scrollToPage:self.pageCount - 1];
	}
}

- (UIViewController *)controllerForPage:(NSUInteger)pageNumber {
	NSString *number = [NSString stringWithFormat:@"%lu", (unsigned long)pageNumber];
	if ([self.pageViews.allKeys containsObject:number]) {
		return [self.pageViews objectForKey:number];
	}
	if (pageNumber < self.pageCount &&
		[self.delegate respondsToSelector:@selector(pageController:controllerForPage:)]) {
		return [self.delegate pageController:self controllerForPage:pageNumber];
	}
	return nil;
}

@end