//
//  ITVideoRecorder.h
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 25/03/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreVideo/CoreVideo.h>

@interface ITVideoRecorder : NSObject

@property (nonatomic, assign, readonly) BOOL recording;

- (id)initWithOutputVideoSize:(CGSize)size inputVideoSize:(CGSize)inputSize videoOrientation:(CGAffineTransform)orient;
- (void)addFrameFrom:(CVPixelBufferRef)frame;
- (BOOL)startRecording;
- (BOOL)finishRecording;

@end
