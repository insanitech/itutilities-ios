//
//  ITCommonAnimations.h
//  SocialVoice
//
//  Created by Anderson Lucas C. Ramos on 11/4/12.
//  Copyright (c) 2012 Anderson Lucas C. Ramos. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ITAnimationCompleted)(void);

@interface ITCommonFx : NSObject

+ (void)buttonTouchAnimation:(UIButton *)anim completion:(ITAnimationCompleted)block;
+ (void)buttonHoverAnimation:(UIButton *)anim withHoverColor:(UIColor *)hover completion:(ITAnimationCompleted)block;
+ (void)addCircleMaskToView:(UIView *)mask;

@end
