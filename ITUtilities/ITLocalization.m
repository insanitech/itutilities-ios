//
//  ITLocalization.m
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 3/23/12.
//  Copyright (c) 2012 InsaniTech. All rights reserved.
//

#import "ITLocalization.h"
#import "ITURLLoader.h"
#import "ITUtilities.h"

@interface ITLocalization ()

@property (nonatomic, assign) int version;
@property (nonatomic, strong) ITURLLoader *loader;
@property (nonatomic, strong) ITLocalizationUpdateBlock block;
@property (nonatomic, strong) NSString *lang;
@property (nonatomic, strong) NSMutableDictionary *translations;
@property (nonatomic, strong) NSURL *getURL;

- (NSString *)filePath;
- (void)versionCheckComplete:(ITURLLoaderEvent *)event;

@end

@implementation ITLocalization

@synthesize version = _version;
@synthesize loader = _loader;
@synthesize block = _block;
@synthesize lang = _lang;
@synthesize translations = _translations;
@synthesize getURL = _getURL;

static ITLocalization	*_sharedInstance = nil;

- (void)dealloc {
	self.loader = nil;
	self.block = nil;
	self.lang = nil;
	self.translations = nil;
	self.getURL = nil;
	
	[ITUtilities deallocLog:[self class]];
}

+ (ITLocalization *)sharedInstance {
	return _sharedInstance;
}

+ (void)cleanInstance {
	_sharedInstance = nil;
}

- (id)init {
	self = [super init];
	if (self != nil)
	{
		self.version = -1;
		
		if (![[NSFileManager defaultManager] fileExistsAtPath:[self filePath] isDirectory:&(BOOL){ YES }]) {
			[[NSFileManager defaultManager] createDirectoryAtPath:[self filePath] withIntermediateDirectories:YES attributes:nil error:nil];
		}
		
		[self loadTranslation];
	}
	return _sharedInstance = self;
}

- (NSString *)locale {
	return [[[NSLocale preferredLanguages] objectAtIndex:0] substringWithRange:(NSRange) { 0, 2 }];
}

- (void)loadTranslation {
	NSString *path = [self filePath];
	NSString *filePath = [[path stringByAppendingPathComponent:self.lang = [self locale]] stringByAppendingString:@".json"];
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
		filePath = [path stringByAppendingPathComponent:@"en.json"];
		self.lang = @"en";
		self.version = 0;
	}
	
	if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
		NSDictionary *translation = [NSJSONSerialization JSONObjectWithData:[[NSData alloc] initWithContentsOfFile:filePath]
																	options:0 error:nil];
		
		self.version = [[translation objectForKey:@"version"] intValue];
		
		self.translations = [[NSMutableDictionary alloc] init];
		
		for (NSDictionary *t in [translation objectForKey:@"translations"]) {
			[self.translations setObject:[t objectForKey:@"value"] forKey:[[t objectForKey:@"key"] uppercaseString]];
		}
	}
}

- (void)loadEmbedTranslation {
	NSString *filePath = [[NSBundle mainBundle] pathForResource:self.lang = [self locale] ofType:@"json"];
	if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
		filePath = [[NSBundle mainBundle] pathForResource:@"en" ofType:@"json"];
		self.lang = @"en";
		self.version = 0;
	}
	
	if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
		NSDictionary *translation = [NSJSONSerialization JSONObjectWithData:[[NSData alloc] initWithContentsOfFile:filePath]
																	options:0 error:nil];
		self.version = [[translation objectForKey:@"version"] intValue];
		self.translations = [NSMutableDictionary dictionary];
		for (NSDictionary *t in [translation objectForKey:@"translations"]) {
			[self.translations setObject:[t objectForKey:@"value"] forKey:[[t objectForKey:@"key"] uppercaseString]];
		}
	}
}

- (void)checkForUpdates:(NSURL *)updateURL updateGetURL:(NSURL *)getURL completion:(ITLocalizationUpdateBlock)block {
	self.block = block;
	
	self.loader = [[ITURLLoader alloc] initWithURL:updateURL.absoluteString
										parameters:@{ @"lang": [self locale] } method:@"POST"];
	[self.loader setListenerToEvent:ITURLEventIOError listener:self action:@selector(versionCheckComplete:)];
	[self.loader setListenerToEvent:ITURLEventComplete listener:self action:@selector(versionCheckComplete:)];
	[self.loader open];
}

- (NSString *)filePath {
	NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] objectAtIndex:0];
	url = [url URLByAppendingPathComponent:@"localization" isDirectory:YES];
	return url.path;
}

- (NSString *)valueForKey:(NSString *)key {
	NSString *value = [self.translations objectForKey:[key uppercaseString]];
	if (value == nil) {
		return key;
	}
	return value;
}

- (void)versionCheckComplete:(ITURLLoaderEvent *)event {
	if ([event.data isKindOfClass:[NSData class]]) {
		NSDictionary *result = [NSJSONSerialization JSONObjectWithData:event.data
															   options:0 error:nil];
		
		if (self.lang != [self locale] ||
			self.version < [[result objectForKey:@"version"] intValue]) {
			__unsafe_unretained ITLocalization *_this = self;
			self.loader = [[ITURLLoader alloc] initWithURL:self.getURL.absoluteString
												parameters:@{ @"lang": [self locale] } method:@"POST"];
			[self.loader setListenerToEvent:ITURLEventIOError withBlock:^(ITURLLoaderEvent *event) {
				[_this loadTranslation];
				if (_this.block != nil) {
					_this.block(NO);
				}
			}];
			[self.loader setListenerToEvent:ITURLEventComplete withBlock:^(ITURLLoaderEvent *event) {
				NSData *data = event.data;
				[data writeToFile:[[[_this filePath] stringByAppendingPathComponent:[_this locale]] stringByAppendingString:@".json"] atomically:YES];

				[_this loadTranslation];
				if (_this.block != nil) {
					_this.block(YES);
				}
			}];
			[self.loader open];
		} else {
			if (self.block != nil) {
				self.block(NO);
			}
		}
	} else {
		if (self.block != nil) {
			self.block(NO);
		}
	}
}

@end

NSString *ITLocalizedKey(NSString *key) {
	return [[ITLocalization sharedInstance] valueForKey:key];
}




















