//
//  ITOpenGLESFrameExtractor.m
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 22/03/13.
//
//

#import "ITOpenGLESFrameExtractor.h"
#import <OpenGLES/EAGL.h>
#import <OpenGLES/EAGLDrawable.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import <AVFoundation/AVFoundation.h>
#import "ITOpenGLESTexture.h"
#import "ITVideoRecorder.h"

#define STRINGIZE(x) #x
#define STRINGIZE2(x) STRINGIZE(x)
#define SHADER_STRING(text) @STRINGIZE2(text)

@interface ITVideoRecorder (Extractor)

@property (nonatomic, strong, readonly) AVAssetWriterInputPixelBufferAdaptor *writerInputAdaptor;

@end

@interface ITOpenGLESFrameExtractor ()

@end

@implementation ITOpenGLESFrameExtractor {
	CVOpenGLESTextureCacheRef textureCache;
	CVPixelBufferRef pixelBuffer;
	ITOpenGLESTexture *backTexture;
	ITVideoRecorder *recorder;
	CGSize bufferSize;
	
//	GLuint program;
//	GLuint vertexShader;
//	GLuint fragmentShader;
//	
//	GLuint textureLocation;
//	GLuint positionLocation;
//	GLuint colorLocation;
//	GLuint texCoordLocation;
}

void PrintGLShaderError(GLuint shader);
void PrintGLProgramError(GLuint program);

- (void)dealloc {
	recorder = nil;
	backTexture = nil;
	if (pixelBuffer != NULL) {
		CVPixelBufferRelease(pixelBuffer);
		pixelBuffer = NULL;
	}
	CFRelease(textureCache);
}

- (id)initWithBufferSize:(CGSize)size recorder:(ITVideoRecorder *)rec
{
    self = [super init];
    if (self != nil) {
		recorder = rec;
		bufferSize = size;
		
		pixelBuffer = NULL;
		CVReturn err = CVPixelBufferPoolCreatePixelBuffer(kCFAllocatorDefault, recorder.writerInputAdaptor.pixelBufferPool, &pixelBuffer);
		if (err) {
			NSLog(@"Error at CVPixelBufferPoolCreatePixelBuffer: %d", err);
		}
		err = CVOpenGLESTextureCacheCreate(NULL, NULL, (__bridge CVEAGLContext)(__bridge void *)[EAGLContext currentContext], NULL, &textureCache);
		if (err) {
			NSLog(@"Error at CVOpenGLESTextureCacheCreate: %d", err);
		}
		backTexture = [ITOpenGLESTexture generateCVTexture:pixelBuffer rgbType:RGB_TypeBGRA textureCache:textureCache];
	}
    return self;
}

void PrintGLShaderError(GLuint shader) {
	int length = 1024;
	GLchar infolog[length];
	glGetShaderInfoLog(shader, length, &length, infolog);
	NSLog(@"Error compiling vertex shader: %s", infolog);
}

void PrintGLProgramError(GLuint program) {
	GLchar infolog[1024];
	glGetProgramInfoLog(program, 1024, NULL, infolog);
	NSLog(@"Error linking shader: %s", infolog);
	@throw [[NSException alloc] initWithName:@"ShaderException" reason:[NSString stringWithFormat:@"%s", infolog] userInfo:nil];
}

- (CVPixelBufferRef)extractFrame:(uint)framebuffer {
	CVPixelBufferLockBaseAddress(pixelBuffer, 0);
	glBindFramebuffer(GL_FRAMEBUFFER_OES, framebuffer);
	glBindTexture(GL_TEXTURE_2D, backTexture.texName);
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, backTexture.texSize.width, backTexture.texSize.height);
	CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
	return pixelBuffer;
}

@end















