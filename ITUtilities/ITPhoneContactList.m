//
//  SMPhoneContactList.m
//  Shot
//
//  Created by Anderson Lucas C. Ramos on 1/9/12.
//  Copyright (c) 2012 Tangível Tecnologia. All rights reserved.
//

#import "ITPhoneContactList.h"
@import AddressBookUI;

@interface ITPhoneContact ()

@property (nonatomic, strong) NSString          *fName;
@property (nonatomic, strong) NSString          *lName;
@property (nonatomic, strong) NSMutableArray    *emails;
@property (nonatomic, strong) NSMutableArray    *phones;

@end

@implementation ITPhoneContact {
    NSString                *_firstName;
    NSString                *_lastName;
    NSMutableArray          *_emails;
    NSMutableArray          *_phones;
}

@synthesize fName = _firstName, lName = _lastName, emails = _emails, phones = _phones;

- (void)dealloc {
    self.fName = nil;
    self.lName = nil;
    self.emails = nil;
    self.phones = nil;
}

- (NSString *)firstName {
    return self.fName;
}

- (NSString *)lastName {
    return self.lName;
}

- (NSArray *)emailList {
    return self.emails;
}

- (NSArray *)phoneList {
    return self.phones;
}

- (id)init
{
    self = [super init];
    if (self != nil) {
        self.emails = [NSMutableArray array];
        self.phones = [NSMutableArray array];
    }
    return self;
}

@end

@interface ITPhoneContactList ()

@property (nonatomic, strong) NSMutableArray            *contacts;
@property (nonatomic, strong) PhoneContactListBlock     block;

@end

@implementation ITPhoneContactList {
    NSMutableArray					*_contacts;
    PhoneContactListBlock           _block;
}

@synthesize contacts = _contacts, block = _block;
@synthesize catchType;

- (void)dealloc {
    self.contacts = nil;
    self.block = nil;
	
	NSLog(@"qd phone contact list deallocated");
}

- (NSArray *)contactList {
    return self.contacts;
}

- (id)init
{
    self = [super init];
    if (self != nil)
    {
        self.contacts = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)doABJobWithRef:(ABAddressBookRef)addressBook {
	CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBook);
	CFMutableArrayRef peopleMutable = CFArrayCreateMutableCopy(kCFAllocatorDefault, CFArrayGetCount(people), people);
	NSMutableArray *contacts = (__bridge NSMutableArray *)peopleMutable;
	
	for (NSUInteger i = 0; i < contacts.count; i++) {
		ITPhoneContact *contact = [[ITPhoneContact alloc] init];
		
		ABRecordRef record = (__bridge ABRecordRef)[contacts objectAtIndex:i];
		CFTypeRef emV = ABRecordCopyValue(record, kABPersonEmailProperty);
		CFTypeRef phV = ABRecordCopyValue(record, kABPersonPhoneProperty);
		ABMultiValueRef emailValue = (ABMultiValueRef)emV;
		ABMultiValueRef phoneValue = (ABMultiValueRef)phV;
		NSString *email = nil;
		NSString *phone = nil;
		
		int count = (int)ABMultiValueGetCount(emailValue);
		if (count > 0) {
			for (int x = 0; x < count; x++) {
				CFTypeRef em = ABMultiValueCopyValueAtIndex(emailValue, x);
				email = (__bridge NSString *)em;
				[contact.emails addObject:email];
				CFRelease((__bridge CFTypeRef)email);
				email = nil;
			}
		} else if (catchType == ITPhoneContactListCatchTypeNeedsEmail) {
			CFRelease(emV);
			CFRelease(phV);
			continue;
		}
		
		count = (int)ABMultiValueGetCount(phoneValue);
		if (count > 0)
		{
			CFTypeRef ph = ABMultiValueCopyValueAtIndex(phoneValue, 0);
			phone = (__bridge NSString *)ph;
			[contact.phones addObject:phone];
			CFRelease((__bridge CFTypeRef)phone);
			phone = nil;
		} else if (catchType == ITPhoneContactListCatchTypeNeedsPhoneNumber) {
			CFRelease(emV);
			CFRelease(phV);
			continue;
		}
		
		NSString *firstName = (__bridge NSString *)ABRecordCopyValue(record, kABPersonFirstNameProperty);
		NSString *lastName = (__bridge NSString *)ABRecordCopyValue(record, kABPersonLastNameProperty);
		
		contact.fName = firstName;
		contact.lName = lastName;
		
		[self.contacts addObject:contact];
		
		if (firstName != nil)
			CFRelease((__bridge CFTypeRef)firstName);
		if (lastName != nil)
			CFRelease((__bridge CFTypeRef)lastName);
		
		CFRelease(emV);
		CFRelease(phV);
	}
	
	if (people != NULL) {
		CFRelease(people);
	}
	if (peopleMutable != NULL) {
		CFRelease(peopleMutable);
	}
	
	if (self.block != nil) {
		self.block(self);
	}
	self.block = nil;
}

- (void)loadContactsWithBlock:(PhoneContactListBlock)block
{
    self.block = block;
    
    [self.contacts removeAllObjects];

    dispatch_async(dispatch_get_main_queue(), ^{
        ABAddressBookRef addressBook;
		if ([[[UIDevice currentDevice] systemVersion] intValue] > 5) {
			addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
			if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
				ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
					[self doABJobWithRef:addressBook];
				});
			}
			else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
				[self doABJobWithRef:addressBook];
			}
			else {
				if (self.block != nil) {
					self.block(nil);
				}
				self.block = nil;
			}
		} else {
			addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
			[self doABJobWithRef:addressBook];
		}
		if (addressBook != NULL) {
			CFRelease(addressBook);
		}
    });
}

@end


























