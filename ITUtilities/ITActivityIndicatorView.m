//
//  ITActivityIndicatorView.m
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 7/6/12.
//  Copyright (c) 2012 42Tecnologia. All rights reserved.
//

@import QuartzCore;

#import "ITActivityIndicatorView.h"

@interface ITActivityIndicatorView ()

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;
@property (nonatomic, strong) UIView *customIndicatorView;
@property (nonatomic, strong) UILabel *infoTextLabel;
@property (nonatomic, assign) BOOL fullscreen;

@end

@implementation ITActivityIndicatorView

@synthesize indicatorView = _indicatorView;
@synthesize infoTextLabel = _infoTextLabel;

static ITActivityIndicatorView *_sharedInstance;

- (void)dealloc {
	self.indicatorView = nil;
	self.infoTextLabel = nil;
}

+ (ITActivityIndicatorView *)sharedInstance {
	return _sharedInstance;
}

- (id)initCenterBoxScreenWithInfoText:(NSString *)text indicatorStyle:(UIActivityIndicatorViewStyle)style {
	CGFloat size;
//	if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
		size = [UIScreen mainScreen].bounds.size.width * 0.5;
//	} else {
//		size = [UIScreen mainScreen].bounds.size.height * 0.5;
//	}
	self = [super initWithFrame:(CGRect) { {0, 0}, {size * 1.5f, size} }];
	if (self != nil) {
		[self.layer setCornerRadius:5.0f];
		[self setBackgroundColor:[UIColor colorWithRed:0.2f green:0.2f blue:0.2f alpha:0.75f]];
		
        if (text != nil) {
            self.infoTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, size - size * 0.4, size - 20, size * 0.3)];
            [self.infoTextLabel setBackgroundColor:[UIColor clearColor]];
            [self.infoTextLabel setTextAlignment:NSTextAlignmentCenter];
            [self.infoTextLabel setTextColor:[UIColor whiteColor]];
            [self.infoTextLabel setText:text];
			self.infoTextLabel.numberOfLines = 3;
            [self.infoTextLabel setCenter:(CGPoint) { self.frame.size.width * 0.5f, self.infoTextLabel.center.y }];
            
            [self addSubview:self.infoTextLabel];
            
            self.indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
            [self.indicatorView setCenter:(CGPoint) { size * 1.5f * 0.5f, size * 0.5f - 20 }];
        } else {
            [self setIndicatorView:[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style]];
            [self.indicatorView setCenter:(CGPoint) { size * 1.5f * 0.5f, size * 0.5f }];
        }
		
		[self addSubview:self.indicatorView];
		self.fullscreen = NO;
	}
	return _sharedInstance = self;
}

- (id)initFullScreenWithInfoText:(NSString *)text indicatorStyle:(UIActivityIndicatorViewStyle)style {
	self = [self initCenterBoxScreenWithInfoText:text indicatorStyle:style];
	self.fullscreen = YES;
	return self;
}

- (id)initFullScreenWithInfoText:(NSString *)text indicatorImage:(UIImage *)image
	   fullscreenBackgroundColor:(UIColor *)bgColor boxBackgroundColor:(UIColor *)boxColor {
	CGFloat size;
//	if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
		size = [UIScreen mainScreen].bounds.size.width * 0.8;
//	} else {
//		size = [UIScreen mainScreen].bounds.size.height * 0.5;
//	}
	self = [super initWithFrame:(CGRect) { { 0, 0 }, { size, size * 0.8f } }];
	if (self != nil) {
		[self.layer setCornerRadius:5.0f];
		[self setBackgroundColor:boxColor];
		
		if (text != nil) {
			self.infoTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, size * 0.8f - size * 0.3f, size - 20, size * 0.3f)];
			[self.infoTextLabel setBackgroundColor:[UIColor clearColor]];
			[self.infoTextLabel setTextAlignment:NSTextAlignmentCenter];
			[self.infoTextLabel setTextColor:[UIColor darkGrayColor]];
			[self.infoTextLabel setText:text];
			self.infoTextLabel.numberOfLines = 3;
			[self.infoTextLabel setCenter:(CGPoint) { self.frame.size.width * 0.5f, self.infoTextLabel.center.y }];
			
			[self addSubview:self.infoTextLabel];
			
			UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
			[imageView setContentMode:UIViewContentModeCenter];
			[imageView setFrame:(CGRect) { 10, 10, size - 20, size * 0.5f }];
			
			[self addSubview:imageView];
		} else {
			UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
			[imageView setContentMode:UIViewContentModeCenter];
			[imageView setFrame:(CGRect) { 10, 10, size - 20, size - 20 }];
			
			[self addSubview:imageView];
		}
		
		self.fullscreen = true;
	}
	return _sharedInstance = self;
}

- (void)setText:(NSString *)text {
	[self.infoTextLabel setText:text];
}

- (void)setTextColor:(UIColor *)color {
    [self.infoTextLabel setTextColor:color];
}

- (void)setFont:(UIFont *)font {
    [self.infoTextLabel setFont:font];
}

- (void)setBackgroundColor:(UIColor *)backgroundColor {
    if (self.fullscreen) {
        [self.superview setBackgroundColor:backgroundColor];
    } else {
        [super setBackgroundColor:backgroundColor];
    }
}

- (void)setFullscreenBackgroundColor:(UIColor *)backgroundColor {
    [super setBackgroundColor:backgroundColor];
}

- (void)addToView:(UIView *)superview {
	[self addToView:superview animated:true];
}

- (void)addToView:(UIView *)superview animated:(BOOL)animated {
	if (self.fullscreen) {
		UIGraphicsBeginImageContext(superview.bounds.size);
		[superview drawViewHierarchyInRect:superview.bounds afterScreenUpdates:YES];
		[[[UIColor darkGrayColor] colorWithAlphaComponent:0.25f] setFill];
		CGContextFillRect(UIGraphicsGetCurrentContext(), superview.bounds);
		UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		CIContext *context = [CIContext contextWithOptions:nil];
		CIImage *inputImage = [CIImage imageWithCGImage:snapshotImage.CGImage];
		CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
		[filter setValue:inputImage forKey:kCIInputImageKey];
		[filter setValue:[NSNumber numberWithFloat:2.0f] forKey:kCIInputRadiusKey];
		CIImage *result = [filter valueForKey:kCIOutputImageKey];
		CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
		UIImage *returnImage = [UIImage imageWithCGImage:cgImage];
		CGImageRelease(cgImage);
		
		CGSize size = superview.frame.size;
		if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
			size = (CGSize) { superview.frame.size.height, superview.frame.size.width };
		}
		UIView *view = [[UIView alloc] initWithFrame:(CGRect) { {0, 0}, size }];
		[view setBackgroundColor:[[UIColor alloc] initWithPatternImage:returnImage]];
		[superview addSubview:view];
		[view addSubview:self];
		[self setCenter:(CGPoint) { view.frame.size.width * 0.5f, view.frame.size.height * 0.5f }];
		
		if (animated) {
			[view setAlpha:0.0f];
			
			[UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState
							 animations:^{
								 [view setAlpha:1.0f];
							 } completion:^(BOOL finished) {
								 
							 }];
		}
	} else {
		[superview addSubview:self];
		[self setCenter:(CGPoint) { superview.frame.size.width * 0.5f, superview.frame.size.height * 0.5f }];
		
		if (animated) {
			[self setAlpha:0.0f];
			
			[UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState
							 animations:^{
								 [self setAlpha:1.0f];
							 } completion:^(BOOL finished) {
								 
							 }];
		}
	}

	[self.indicatorView startAnimating];
}

- (void)removeFromSuperview {
	[self removeFromSuperviewAnimated:true];
}

- (void)removeFromSuperviewAnimated:(BOOL)animated
{
	if (animated) {
		[UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState
						 animations:^{
							 if (self.fullscreen) {
								 [self.superview setAlpha:0.0f];
							 } else {
								 [self setAlpha:0.0f];
							 }
						 } completion:^(BOOL finished) {
							 if (self.superview != nil) {
								[self removeFromSuperviewAnimated:false];
							 }
						 }];
	} else {
		if (self.fullscreen) {
			[self.superview removeFromSuperview];
			[super removeFromSuperview];
			[self.indicatorView stopAnimating];
		} else {
			[self.indicatorView stopAnimating];
			[super removeFromSuperview];
		}
	}
}

@end
