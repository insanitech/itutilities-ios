//
//  ITOpenGLESTexture.h
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 25/03/13.
//
//

#import <OpenGLES/ES2/gl.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreVideo/CoreVideo.h>

@class ITOpenGLESFrameExtractor;

enum RGB_Type
{
	RGB_TypeRGBA,
	RGB_TypeBGRA
};
typedef enum RGB_Type RGB_Type;

@interface ITOpenGLESTexture : NSObject

@property (nonatomic, assign, readonly) GLenum			texTarget;
@property (nonatomic, assign, readonly) GLuint 			texName;
@property (nonatomic, assign, readonly) CGSize          texSize;

+ (BOOL)supportsFastTextureUpload;
+ (ITOpenGLESTexture *)generateCVTexture:(CVPixelBufferRef)img rgbType:(RGB_Type)rgb textureCache:(CVOpenGLESTextureCacheRef)cache;
+ (ITOpenGLESTexture *)generateRenderTextureWithSize:(CGSize)size rgbType:(RGB_Type)rgb;
- (void)discart;

@end
