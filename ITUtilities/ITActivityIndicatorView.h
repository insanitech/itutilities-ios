//
//  ITActivityIndicatorView.h
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 7/6/12.
//  Copyright (c) 2012 42Tecnologia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ITActivityIndicatorView : UIView

+ (ITActivityIndicatorView *)sharedInstance;

- (id)initCenterBoxScreenWithInfoText:(NSString *)text indicatorStyle:(UIActivityIndicatorViewStyle)style;
- (id)initFullScreenWithInfoText:(NSString *)text indicatorStyle:(UIActivityIndicatorViewStyle)style;
- (id)initFullScreenWithInfoText:(NSString *)text indicatorImage:(UIImage *)image
	   fullscreenBackgroundColor:(UIColor *)bgColor boxBackgroundColor:(UIColor *)boxColor;
- (void)setTextColor:(UIColor *)color;
- (void)setFont:(UIFont *)font;
- (void)addToView:(UIView *)superview;
- (void)addToView:(UIView *)superview animated:(BOOL)animated;
- (void)setText:(NSString *)text;
- (void)setBackgroundColor:(UIColor *)backgroundColor;
- (void)setFullscreenBackgroundColor:(UIColor *)backgroundColor;
- (void)removeFromSuperview;
- (void)removeFromSuperviewAnimated:(BOOL)animated;

@end
