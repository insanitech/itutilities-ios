//
//  TTSegmentedControl.m
//  Shot
//
//  Created by Anderson Lucas C. Ramos on 12/29/11.
//  Copyright (c) 2011 Tangível Tecnologia. All rights reserved.
//

#import "ITSegmentedControl.h"
#import "ITUtilities.h"

@implementation ITSegmentedControlItem

@synthesize title = _title;
@synthesize shadowColor = _shadowColor;
@synthesize font = _font;
@synthesize textColor = _textColor;
@synthesize selectedTextColor = _selectedTextColor;
@synthesize selectedFont = _selectedFont;
@synthesize selectedImage = _selectedImage;
@synthesize shadowOffset = _shadowOffset;
@synthesize unselectedImage = _unselectedImage;
@synthesize contentStretch = _contentStretch;

- (void)dealloc
{
    [self setTitle:nil];
    [self setSelectedImage:nil];
    [self setUnselectedImage:nil];
    [self setFont:nil];
    [self setTextColor:nil];
    [self setSelectedFont:nil];
    [self setSelectedTextColor:nil];
    [self setShadowColor:nil];
    
    [ITUtilities deallocLog:[self class]];
}

+ (ITSegmentedControlItem *)itemWithCustomSelectedView:(UIView *)selectedView
                                        unselectedView:(UIView *)unselectedView {
    ITSegmentedControlItem *item = [[ITSegmentedControlItem alloc] init];
    UIImage *selectedImage = [ITUtilities screenshotFromView:selectedView];
    UIImage *unselectedImage = [ITUtilities screenshotFromView:unselectedView];
    [item setSelectedImage:selectedImage];
    [item setUnselectedImage:unselectedImage];
    return item;
}

+ (ITSegmentedControlItem *)itemWithTitle:(NSString *)title
                            selectedImage:(UIImage *)selectedImage
                          unselectedImage:(UIImage *)unselectedImage {
    ITSegmentedControlItem *item = [[ITSegmentedControlItem alloc] init];
    [item setTitle:title];
    [item setSelectedImage:selectedImage];
    [item setUnselectedImage:unselectedImage];
    return item;
}

- (id)init {
    self = [super init];
    if (self != nil) {
        
    }
    return self;
}

@end

@interface ITSegmentedControl ()

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, assign) id target;
@property (nonatomic, assign) SEL selector;
@property (nonatomic, assign) BOOL initialized;

- (void)buttonAction:(UIButton *)button;
- (void)adjustButtonColor:(BOOL)clear;

@end

@implementation ITSegmentedControl

@synthesize items = _items;
@synthesize target = _target;
@synthesize selectedSegmentIndex = _selectedSegmentIndex;
@synthesize selector = _selector;
@synthesize initialized = _initialized;

- (void)dealloc {
    self.items = nil;
    self.selector = nil;
    self.target = nil;
    
    [ITUtilities deallocLog:[self class]];
}

- (void)initialize {
    CGFloat buttonWidth = self.frame.size.width / self.items.count;
    CGFloat buttonHeight = self.frame.size.height;
    
    for (NSUInteger i = 0; i < self.items.count; i++) {
        ITSegmentedControlItem *item = [self.items objectAtIndex:i];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:(CGRect) { {i * buttonWidth, 0}, {buttonWidth, buttonHeight} }];
        [button setBackgroundColor:[UIColor clearColor]];
        
        if (item.unselectedImage != nil) {
            [button setBackgroundImage:item.unselectedImage forState:UIControlStateNormal];
        }
        if (item.selectedImage != nil) {
            [button setBackgroundImage:item.selectedImage forState:UIControlStateSelected];
        }
        if (item.textColor != nil) {
            [button setTitleColor:item.textColor forState:UIControlStateNormal];
        }
        if (item.selectedTextColor != nil) {
            [button setTitleColor:item.selectedTextColor forState:UIControlStateSelected];
        }
        if (item.font != nil) {
            [button.titleLabel setFont:item.font];
        }
        if (item.shadowColor != nil) {
            [button.titleLabel setShadowColor:item.shadowColor];
        }
        if (item.title != nil) {
            [button setTitle:item.title forState:UIControlStateNormal];
        }
        [button.titleLabel setShadowOffset:item.shadowOffset];
        
        [button setTag:i];
        [self addSubview:button];
        [button.titleLabel setBackgroundColor:[UIColor clearColor]];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)clear {
    for (UIView *v in self.subviews) {
        [v removeFromSuperview];
    }
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self != nil) {
        [self setTag:-1];
        [self setItems:[[NSMutableArray alloc] init]];
        
        [self initialize];
        [self setInitialized:NO];
        
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (id)initWithItems:(NSArray *)items frame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self != nil) {
        [self setTag:-1];
        [self setItems:items];
        
        [self initialize];
        [self setInitialized:NO];
        
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    
    [self clear];
    [self initialize];
    
    [self adjustButtonColor:NO];
}

- (void)adjustButtonColor:(BOOL)clear
{
    UIButton *b = (UIButton *)[self viewWithTag:self.selectedSegmentIndex];
    
    if ([b isKindOfClass:[UIButton class]]) {
        ITSegmentedControlItem *item = [self.items objectAtIndex:self.selectedSegmentIndex];
        if (!clear) {
            [b setSelected:YES];
            [b.titleLabel setFont:item.selectedFont];
        } else {
            [b setSelected:NO];
            [b.titleLabel setFont:item.font];
        }
    }
}

- (void)valueDidChangeInternal
{
    if (self.target != nil && [self.target respondsToSelector:self.selector]) {
        [self.target performSelectorOnMainThread:self.selector withObject:self waitUntilDone:YES];
    }
}


- (void)setSelectedSegmentIndex:(NSInteger)selectedIndex {
    [self adjustButtonColor:YES];
    _selectedSegmentIndex = selectedIndex;
    [self adjustButtonColor:NO];
    if ([self viewWithTag:self.selectedSegmentIndex] == nil) {
        @throw [[NSException alloc] initWithName:@"Invalid Segment Index Exception" 
                                          reason:@"The given segment index is out of bounds"
                                        userInfo:nil];
    }
    [self valueDidChangeInternal];
}

- (void)setValueDidChangeTarget:(id)target action:(SEL)selector {
    [self setTarget:target];
    [self setSelector:selector];
}

- (void)buttonAction:(UIButton *)button {
    if (button.tag != self.selectedSegmentIndex) {
        [self adjustButtonColor:YES];
        [self setSelectedSegmentIndex:button.tag];
        [self adjustButtonColor:NO];
        [self valueDidChangeInternal];
    }
}

- (void)addItem:(ITSegmentedControlItem *)item {
    [(NSMutableArray *)self.items addObject:item];
    [self clear];
    [self initialize];
    [self adjustButtonColor:NO];
}

- (void)removeItemAtIndex:(ITSegmentedControlItem *)item {
    [(NSMutableArray *)self.items removeObject:item];
    [self clear];
    [self setSelectedSegmentIndex:0];
    [self initialize];
}

@end


























