//
//  ITURLLoader.h
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 7/25/12.
//  Copyright (c) 2012 42Tecnologia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ITURLEventListener;
@class ITURLLoader;
@class ITURLLoaderEvent;

typedef NSString *ITURLEvent;
extern const ITURLEvent ITURLEventComplete;
extern const ITURLEvent ITURLEventIOError;
extern const ITURLEvent ITURLEventProgress;
extern const ITURLEvent ITURLEventStart;

typedef NSString *ITURLMethod;
extern const ITURLMethod ITURLMethod_GET;
extern const ITURLMethod ITURLMethod_POST;
extern const ITURLMethod ITURLMethod_PUT;
extern const ITURLMethod ITURLMethod_POST_JSON;

typedef void (^ITURLEventBlock)(ITURLLoaderEvent *event);

@interface ITURLEventListener : NSObject

@property (nonatomic, strong) id listener;
@property (nonatomic, assign) SEL selector;

- (id)initWithListener:(id)listener action:(SEL)selector;

@end

@interface ITURLLoaderEvent : NSObject

@property (nonatomic, strong) id 						data;
@property (nonatomic, strong) ITURLEvent 				type;
@property (nonatomic, unsafe_unretained) id				target;
@property (nonatomic, assign) long long 				totalBytes;
@property (nonatomic, assign) long long 				bytesLoaded;
@property (nonatomic, assign) NSInteger 				statusCode;

- (id)initWithTarget:(id)target type:(ITURLEvent)type andData:(id)data;

@end

@interface ITURLLoader : NSObject

@property (nonatomic, assign, readonly) BOOL			loading;

+ (void)setListenerToExistingConnections:(ITURLEventBlock)block;

- (id)initWithURL:(NSString *)url parameters:(NSDictionary *)parameters method:(NSString *)method;
- (id)initWithURL:(NSString *)url parameters:(NSDictionary *)parameters method:(NSString *)method files:(NSDictionary *)files;
- (void)setBasicAuthWithLogin:(NSString *)userName andPassword:(NSString *)password;
- (void)setBearerAuthWithToken:(NSString *)token;
- (void)addHeaderField:(NSString *)key andValue:(NSString *)value;
- (void)setListenerToEvent:(const ITURLEvent)event listener:(id)listener action:(SEL)selector;
- (void)setListenerToEvent:(const ITURLEvent)event withBlock:(ITURLEventBlock)block;
- (void)open;
- (void)close;

@end
