//
//  QDSliteDatabase.m
//  QDDatabase
//
//  Created by Anderson Lucas C. Ramos on 3/28/12.
//  Copyright (c) 2012 Tangível Tecnologia. All rights reserved.
//

#import "ITSQLiteDatabase.h"
#import "sqlite3.h"
#import "ITThread.h"
#import "ITUtilities.h"

const ITDatabaseEvent ITDatabaseEventComplete = @"ITURLEventComplete";
const ITDatabaseEvent ITDatabaseEventIOError = @"ITURLEventIOError";
const ITDatabaseEvent ITDatabaseEventProgress = @"ITURLEventProgress";
const ITDatabaseEvent ITDatabaseEventStart = @"ITURLEventStart";

void normalize(sqlite3_context *context, int numValues, sqlite3_value **values);

@interface ITSQLiteDataReader ()

@property (nonatomic) NSRange range;
@property (nonatomic) float percent;
@property (strong) NSString *errorMessage;
@property (strong) NSMutableDictionary *data;
@property (strong) NSDictionary *types;

- (void)addValues:(NSArray *)values forKey:(NSArray *)keys;

@end

@implementation ITSQLiteDataReader

@synthesize range = _range;
@synthesize percent = _percent;
@synthesize data = _data;
@synthesize types = _types;
@synthesize errorMessage = _errorMessage;

- (void)dealloc {
	self.errorMessage = nil;
	self.data = nil;
	self.types = nil;
}

- (id)init {
	self = [super init];
	if (self != nil) {
		self.data = [[NSMutableDictionary alloc] init];
		self.range = (NSRange) { 0, 0 };
	}
	return self;
}

- (void)addValues:(NSArray *)values forKey:(NSArray *)keys {
	if (values.count != keys.count)
		@throw [[NSException alloc] initWithName:@"NSCountException" reason:@"Values and keys must have the same count of items." userInfo:nil];
	for (int i = 0; i < values.count; i++) {
		NSString *key = [keys objectAtIndex:i];
		NSString *value = [values objectAtIndex:i];
		NSMutableArray *array = [self.data objectForKey:key];
		if (array == nil) {
			array = [NSMutableArray array];
			[self.data setObject:array forKey:key];
		}
		[array addObject:value];
	}
	_range.length++;
}

- (BOOL)next {
	_range.location++;
	if (_range.location <= _range.length) {
		return YES;
	}
	return NO;
}

- (NSArray *)columnNames {
	return _data.allKeys;
}

- (NSNumber *)getInt:(NSString *)colummName {
	return [self getInteger:colummName atIndex:_range.location - 1];
}

- (NSNumber *)getInt:(NSString *)colummName atIndex:(NSUInteger)index {
	return [self getInteger:colummName atIndex:index];
}

- (NSNumber *)getInteger:(NSString *)columnName {
	return [self getInteger:columnName atIndex:_range.location - 1];
}

- (NSNumber *)getInteger:(NSString *)columnName atIndex:(NSUInteger)index {
	id obj = [[_data objectForKey:columnName] objectAtIndex:index];
	if (![obj isKindOfClass:[NSNull class]]) {
		return obj;
	}
	return nil;
}

- (NSData *)getData:(NSString *)columnName {
	return [self getData:columnName atIndex:_range.location - 1];
}

- (NSData *)getData:(NSString *)columnName atIndex:(NSUInteger)index {
	id obj = [[_data objectForKey:columnName] objectAtIndex:index];
	if (![obj isKindOfClass:[NSNull class]]) {
		return obj;
	}
	return nil;
}

- (NSNumber *)getDouble:(NSString *)columnName {
	return [self getDouble:columnName atIndex:_range.location - 1];
}

- (NSNumber *)getDouble:(NSString *)columnName atIndex:(NSUInteger)index {
	id obj = [[_data objectForKey:columnName] objectAtIndex:index];
	if (![obj isKindOfClass:[NSNull class]]) {
		return obj;
	}
	return nil;
}

- (NSNumber *)getFloat:(NSString *)columnName {
	return [self getFloat:columnName atIndex:_range.location - 1];
}

- (NSNumber *)getFloat:(NSString *)columnName atIndex:(NSUInteger)index {
	id obj = [[_data objectForKey:columnName] objectAtIndex:index];
	if (![obj isKindOfClass:[NSNull class]]) {
		return obj;
	}
	return nil;
}

- (NSNumber *)getLong:(NSString *)columnName {
	return [self getLong:columnName atIndex:_range.location - 1];
}

- (NSNumber *)getLong:(NSString *)columnName atIndex:(NSUInteger)index {
	id obj = [[_data objectForKey:columnName] objectAtIndex:index];
	if (![obj isKindOfClass:[NSNull class]]) {
		return obj;
	}
	return nil;
}

- (NSString *)getString:(NSString *)columnName {
	return [self getString:columnName atIndex:_range.location - 1];
}

- (NSString *)getString:(NSString *)columnName atIndex:(NSUInteger)index {
	id obj = [[_data objectForKey:columnName] objectAtIndex:index];
	if (![obj isKindOfClass:[NSNull class]]) {
		return obj;
	}
	return nil;
}

- (ITSQLiteDataType)getColumnDataType:(NSString *)columnName {
	return (ITSQLiteDataType)[[_types objectForKey:columnName] integerValue];
}

- (NSUInteger)rowsCount {
	return _range.length;
}

@end

//////////////////////////////////////////////////////////////////////////////////////////

@interface ITSQLiteDatabase ()

@property (strong) NSString *query;
@property (nonatomic) sqlite3 *sqlite;
@property (nonatomic) BOOL connected;
@property (nonatomic) BOOL querying;

@property (strong) ITDatabaseEventBlock start;
@property (strong) ITDatabaseEventBlock progress;
@property (strong) ITDatabaseEventBlock ioError;
@property (strong) ITDatabaseEventBlock complete;

+ (NSString *)databaseFilePath:(NSString *)dbFileName;

- (void)connectDatabase:(NSString *)filePath;
- (void)disconnectDatabase;
- (void)queryDatabase:(ITSQLiteDatabase *)db;
- (void)nonQueryDatabase:(ITSQLiteDatabase *)db query:(NSString *)query;
- (void)dispatchIOError;

@end

@implementation ITSQLiteDatabase

@synthesize sqlite = _sqlite;
@synthesize query = _query;
@synthesize start = _start;
@synthesize progress = _progress;
@synthesize ioError = _ioError;
@synthesize complete = _complete;
@synthesize connected = _connected;
@synthesize querying = _querying;

+ (NSString *)databaseFilePath:(NSString *)dbFileName {
	return [[ITUtilities documentsDirectoryString] stringByAppendingPathComponent:dbFileName];
}

+ (BOOL)databaseExists:(NSString *)dbFileName {
	NSFileManager *file = [NSFileManager defaultManager];
	return [file fileExistsAtPath:[ITSQLiteDatabase databaseFilePath:dbFileName]];
}

+ (ITSQLiteDatabase *)createDatabase:(NSString *)dbFileName {
    NSFileManager *file = [NSFileManager defaultManager];
	NSString *filePath = [ITSQLiteDatabase databaseFilePath:dbFileName];
	
	if ([file fileExistsAtPath:filePath]) {
		NSError *error = nil;
		[file removeItemAtPath:filePath error:&error];
		if (error != nil) {
			NSLog(@"Error: %@", error);
			return nil;
		}
	}
	if ([file createFileAtPath:filePath contents:[NSData data] attributes:nil]) {
        return [[ITSQLiteDatabase alloc] initWithDBName:dbFileName];
    }
    
	return nil;
}

- (void)dealloc {
	self.query = nil;
	self.start = nil;
	self.progress = nil;
	self.ioError = nil;
	self.complete = nil;
	[self disconnectDatabase];
}

- (id)initWithDBName:(NSString *)dbFileName {
    self = [super init];
	if (self != nil) {
		NSFileManager *file = [NSFileManager defaultManager];
		NSString *filePath = [ITSQLiteDatabase databaseFilePath:dbFileName];
		if (![file fileExistsAtPath:filePath]) {
			NSError *error = nil;
			[file copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:dbFileName] 
						  toPath:filePath error:&error];
			
			if (error == nil) {
                [self connectDatabase:filePath];
            }
		} else {
            [self connectDatabase:filePath];
        }
	}
	return self;
}

- (void)setListenerToEvent:(ITDatabaseEvent)eventType resultReader:(ITDatabaseEventBlock)resultBlock {
	if ([eventType isEqualToString:ITDatabaseEventStart]) {
		self.start = resultBlock;
	} else if ([eventType isEqualToString:ITDatabaseEventProgress]) {
		self.progress = resultBlock;
	} else if ([eventType isEqualToString:ITDatabaseEventIOError]) {
		self.ioError = resultBlock;
	} else {
		self.complete = resultBlock;
	}
}

- (void)connectDatabase:(NSString *)filePath {
	if ((self.connected = (sqlite3_open_v2(filePath.UTF8String, &_sqlite, SQLITE_OPEN_READWRITE, NULL) == SQLITE_OK))) {
		sqlite3_create_function_v2(self.sqlite, "normalize", 1, SQLITE_UTF8, NULL, normalize, NULL, NULL, NULL);
	}
}

- (void)disconnectDatabase {
	sqlite3_close(self.sqlite);
}

- (void)executeQuery:(NSString *)query {
	self.query = query;
	self.querying = YES;
	[self performSelectorInBackground:@selector(queryDatabase:) withObject:self];
}

- (void)executeNonQuery:(NSString *)query {
	self.query = query;
	self.querying = YES;
	[self nonQueryDatabase:self query:query];
}

- (void)dispatchStart {
	__unsafe_unretained ITSQLiteDatabase *me = self;
	
	[[[ITThread alloc] init] performOnMainThread:^{
		if (me.start != nil) {
			me.start(ITDatabaseEventStart, nil);
		}
	} waitUntilDone:YES];
}

- (void)dispatchProgress:(float)percent reader:(ITSQLiteDataReader *)reader {
	__unsafe_unretained ITSQLiteDatabase *me = self;
	
	reader.percent = percent;
	
	[[[ITThread alloc] init] performOnMainThread:^{
		if (me.progress != nil) {
			me.progress(ITDatabaseEventProgress, reader);
		}
	} waitUntilDone:YES];
}

- (void)dispatchIOError {
	__unsafe_unretained ITSQLiteDatabase *me = self;
	
	[[[ITThread alloc] init] performOnMainThread:^{
		if (me.ioError != nil) {
			const char *error = sqlite3_errmsg(_sqlite);
			ITSQLiteDataReader *reader = [[ITSQLiteDataReader alloc] init];
			reader.errorMessage = [NSString stringWithFormat:@"%s", error];
			me.ioError(ITDatabaseEventIOError, reader);
		}
	} waitUntilDone:YES];
}

- (void)dispatchComplete:(ITSQLiteDataReader *)reader {
	__unsafe_unretained ITSQLiteDatabase *me = self;
	
	[[[ITThread alloc] init] performOnMainThread:^{
		if (me.complete != nil) {
			me.complete(ITDatabaseEventComplete, reader);
		}
	} waitUntilDone:YES];
}

- (NSDictionary *)getColumnTypes:(sqlite3_stmt *)statement {
	NSMutableDictionary *dic = [NSMutableDictionary dictionary];
	for (int i = 0; i < sqlite3_column_count(statement); i++) {
		NSString *key = [NSString stringWithUTF8String:sqlite3_column_name(statement, i)];
		switch (sqlite3_column_type(statement, i)) {
			case SQLITE_INTEGER:
				[dic setObject:@(ITSQLiteDataTypeInteger) forKey:key];
				break;
				
			case SQLITE_FLOAT:
				[dic setObject:@(ITSQLiteDataTypeFloat) forKey:key];
				break;
				
			case SQLITE_BLOB:
				[dic setObject:@(ITSQLiteDataTypeBlob) forKey:key];
				break;
				
			case SQLITE_TEXT:
				[dic setObject:@(ITSQLiteDataTypeText) forKey:key];
				break;
				
			case SQLITE_NULL:
				[dic setObject:@(ITSQLiteDataTypeNull) forKey:key];
				break;
				
			default: break;
		}
	}
	return dic;
}

- (void)queryDatabase:(ITSQLiteDatabase *)db {
	@autoreleasepool  {
		float percent = 0.0f;
		float partial = 0.0f;
		if (db.connected) {
			const char *q = db.query.UTF8String;
			sqlite3_stmt *statement;
			if (sqlite3_prepare_v2(self.sqlite, q, -1, &statement, NULL) == SQLITE_OK) {
				ITSQLiteDataReader *reader = [[ITSQLiteDataReader alloc] init];
				
				[self dispatchStart];
				
				NSMutableArray *keys = [[NSMutableArray alloc] init];
				for (int i = 0; i < sqlite3_column_count(statement); i++) {
					NSString *columnName = [NSString stringWithUTF8String:sqlite3_column_name(statement, i)];
					[keys addObject:columnName];
				}
				
				partial = 1.0f / (float)sqlite3_data_count(statement);
				
				int stmt = sqlite3_step(statement);
				
				[reader setTypes:[self getColumnTypes:statement]];
				
				if (stmt != SQLITE_ERROR) {
					while (stmt == SQLITE_ROW) {
						NSMutableArray *values = [NSMutableArray array];
						for (int i = 0; i < keys.count; i++) {
//							switch (sqlite3_column_type(statement, i)) {
							switch (sqlite3_value_type(sqlite3_column_value(statement, i))) {
								case SQLITE_INTEGER:
									[values addObject:[NSNumber numberWithInteger:sqlite3_column_int(statement, i)]];
									break;
									
								case SQLITE_FLOAT:
									[values addObject:[NSNumber numberWithDouble:sqlite3_column_double(statement, i)]];
									break;
									
								case SQLITE_BLOB:
									[values addObject:[[NSData alloc] initWithBytes:sqlite3_column_blob(statement, i) length:sqlite3_column_bytes(statement, i)]];
									break;
									
								case SQLITE_TEXT:
									[values addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, i)]];
									break;
									
								case SQLITE_NULL:
									[values addObject:[[NSNull alloc] init]];
//									[values addObject:nil];
									break;
									
								default:
									NSLog(@"Unknown value, possible QDSQLiteDataReader exception...");
									break;
							}
						}
						[reader addValues:values forKey:keys];
						stmt = sqlite3_step(statement);
						percent += partial;
						[self dispatchProgress:percent reader:reader];
					}
					
					[self dispatchComplete:reader];
				} else {
					[db dispatchIOError];
				}
			} else {
				[db dispatchIOError];
			}
			
			sqlite3_finalize(statement);
		} else {
			[db dispatchIOError];
		}
		db.querying = NO;
	}
}

- (ITSQLiteDataReader *)executeSynchronousQuery:(NSString *)query
{
	self.query = query;
	self.querying = YES;
	if (self.connected) {
		const char *q = query.UTF8String;
		sqlite3_stmt *statement;
		if (sqlite3_prepare_v2(self.sqlite, q, -1, &statement, NULL) == SQLITE_OK) {
			ITSQLiteDataReader	*reader = [[ITSQLiteDataReader alloc] init];
			
			NSMutableArray *keys = [[NSMutableArray alloc] init];
			for (int i = 0; i < sqlite3_column_count(statement); i++) {
				NSString *columnName = [NSString stringWithUTF8String:sqlite3_column_name(statement, i)];
				[keys addObject:columnName];
			}
			
			int stmt = sqlite3_step(statement);
			
			[reader setTypes:[self getColumnTypes:statement]];
			
			if (stmt != SQLITE_ERROR) {
				while (stmt == SQLITE_ROW) {
					NSMutableArray *values = [NSMutableArray array];
					for (int i = 0; i < keys.count; i++) {
//							switch (sqlite3_column_type(statement, i)) {
						switch (sqlite3_value_type(sqlite3_column_value(statement, i))) {
							case SQLITE_INTEGER:
								[values addObject:[NSNumber numberWithInteger:sqlite3_column_int(statement, i)]];
								break;
								
							case SQLITE_FLOAT:
								[values addObject:[NSNumber numberWithDouble:sqlite3_column_double(statement, i)]];
								break;
								
							case SQLITE_BLOB:
								[values addObject:[[NSData alloc] initWithBytes:sqlite3_column_blob(statement, i) length:sqlite3_column_bytes(statement, i)]];
								break;
								
							case SQLITE_TEXT:
								[values addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, i)]];
								break;
								
							case SQLITE_NULL:
								[values addObject:[[NSNull alloc] init]];
//								[values addObject:nil];
								break;
								
							default:
								NSLog(@"Unknown value, possible QDSQLiteDataReader exception...");
								break;
						}
					}
					[reader addValues:values forKey:keys];
					stmt = sqlite3_step(statement);
				}
				self.querying = NO;
				return reader;
			}
		} else {
			[self dispatchIOError];
		}
		
		sqlite3_finalize(statement);
	}
	self.querying = NO;
	return nil;
}

- (void)nonQueryDatabase:(ITSQLiteDatabase *)db query:(NSString *)query
{
	if (db.connected) {
		const char *q = query.UTF8String;
		sqlite3_stmt *statement;
		if (sqlite3_prepare_v2(db.sqlite, q, -1, &statement, NULL) == SQLITE_OK) {
			if (sqlite3_step(statement) != SQLITE_DONE) {
				[db dispatchIOError];
			} else {
				[self dispatchComplete:nil];
			}
		} else {
			[db dispatchIOError];
		}
		
		sqlite3_finalize(statement);
	} else {
		[db dispatchIOError];
	}
	db.querying = NO;
}

- (NSInteger)lastInsertRowId {
	return (NSInteger)sqlite3_last_insert_rowid(self.sqlite);
}

@end

void normalize(sqlite3_context *context, int numValues, sqlite3_value **values)
{
	char *str = (char *) sqlite3_value_text(values[0]);
	NSString *text = [NSString stringWithCString:str encoding:NSUTF8StringEncoding];
	
	NSDictionary *patterns = [NSDictionary dictionaryWithObjectsAndKeys:
							  @"[áãàâäÁÃÀÂÄ]", @"a",
							  @"[éèêëÉÈÊË]", @"e",
							  @"[íìîïÍÌÎÏ]", @"i",
							  @"[óòõôöÓÒÕÔÖ]", @"o",
							  @"[úùûüÚÙÛÜ]", @"u",
							  @"[çÇ]", @"c",
							  nil];
	NSError *err = NULL;
	NSRegularExpression *regex = nil;
	for (NSString *replacement in [patterns keyEnumerator]) {
		regex = [NSRegularExpression regularExpressionWithPattern:[patterns objectForKey:replacement] options:NSRegularExpressionCaseInsensitive error:&err];
		text = [regex stringByReplacingMatchesInString:text options:0 range:NSMakeRange(0, [text length]) withTemplate:replacement];
	}
	
	sqlite3_result_text(context, [text UTF8String], -1, NULL);
}