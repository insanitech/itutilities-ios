//
//  ITPageController.h
//  SocialVoice
//
//  Created by Anderson Lucas C. Ramos on 11/5/12.
//  Copyright (c) 2012 Anderson Lucas C. Ramos. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ITPageController;

@protocol ITPageControllerDelegate <NSObject>

@required
- (NSUInteger)pageControllerNumberOfPages:(ITPageController *)pageController;
- (UIViewController *)pageController:(ITPageController *)pageController controllerForPage:(NSUInteger)pageNumber;
@optional
- (void)pageController:(ITPageController *)pageController willAddPage:(UIViewController *)page;
- (void)pageController:(ITPageController *)pageController willRemovePage:(UIViewController *)page;
- (void)pageControllerCurrentPageDidChange:(ITPageController *)pageController;

@end

@interface ITPageController : NSObject
	<UIScrollViewDelegate>

@property (nonatomic, strong, readonly) UIScrollView *pager;
@property (nonatomic, assign) id<ITPageControllerDelegate> delegate;
@property (nonatomic, assign, readonly) NSUInteger currentPage;
@property (nonatomic, assign) NSUInteger inMemoryPages;

- (id)initWithScrolView:(UIScrollView *)pager;
- (void)scrollToPage:(NSUInteger)pageNumber;
- (void)scrollToPage:(NSUInteger)pageNumber animated:(BOOL)animated;
- (void)reloadData;
- (UIViewController *)controllerForPage:(NSUInteger)pageNumber;

@end
