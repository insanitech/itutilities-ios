//
//  ITActionSheet.m
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 22/07/13.
//
//

#import "ITActionSheet.h"
#import "ITUtilities.h"

@interface ITActionSheet ()
    <UIActionSheetDelegate>

@property (strong, nonatomic) ITActionSheetButtonBlock completion;

@end

@implementation ITActionSheet

@synthesize completion = _completion;

- (void)dealloc {
    [self setCompletion:nil];
    
    [ITUtilities deallocLog:[self class]];
}

- (id)initWithTitle:(NSString *)title
  cancelButtonTitle:(NSString *)cancelButtonTitle
destructiveButtonTitle:(NSString *)destructiveButtonTitle
  otherButtonTitles:(NSArray *)otherButtonTitles completion:(ITActionSheetButtonBlock)block {
    self = [super initWithTitle:title delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil
              otherButtonTitles:nil];
    if (self != nil) {
        for (NSUInteger i = 0; i < otherButtonTitles.count; i++) {
            [self addButtonWithTitle:[otherButtonTitles objectAtIndex:i]];
        }
        if (cancelButtonTitle != nil) {
            [self addButtonWithTitle:cancelButtonTitle];
            [self setCancelButtonIndex:otherButtonTitles.count];
        }
        if (destructiveButtonTitle != nil) {
            [self addButtonWithTitle:destructiveButtonTitle];
            if (cancelButtonTitle != nil) {
                [self setDestructiveButtonIndex:otherButtonTitles.count + 1];
            } else {
                [self setDestructiveButtonIndex:otherButtonTitles.count];
            }
        }
        [self setCompletion:block];
    }
    return self;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (self.completion != nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.completion((ITActionSheet *)actionSheet, buttonIndex);
        });
    }
}

- (NSString *)buttonTitleAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex < self.numberOfButtons &&
        buttonIndex > -1) {
        return [super buttonTitleAtIndex:buttonIndex];
    }
    return @"";
}

@end
