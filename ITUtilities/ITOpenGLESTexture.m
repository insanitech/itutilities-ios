//
//  ITOpenGLESTexture.m
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 25/03/13.
//
//

#import "ITOpenGLESTexture.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreVideo/CoreVideo.h>
#import "ITOpenGLESFrameExtractor.h"

@implementation ITOpenGLESTexture
{
	GLuint					_texture;
	CVPixelBufferRef		_pBuffer;
	CVOpenGLESTextureRef	_glTexture;
	BOOL					_cvTexture;
    CGSize                  _texSize;
}

@synthesize texTarget, texName, texSize = _texSize;

- (void)setTexSize:(CGSize)texSize {
    _texSize = texSize;
}

- (void)dealloc {
	[self discart];
}

- (id)init {
	self = [super init];
	if (self != nil) {
		_texture = 0;
		_pBuffer = NULL;
		_glTexture = NULL;
		_cvTexture = NO;
	}
	return self;
}

+ (BOOL)supportsFastTextureUpload
{
    return (CVOpenGLESTextureCacheCreate != NULL);
}

+ (ITOpenGLESTexture *)generateCVTexture:(CVPixelBufferRef)img rgbType:(RGB_Type)rgb textureCache:(CVOpenGLESTextureCacheRef)cache
{
	ITOpenGLESTexture *tex = [[ITOpenGLESTexture alloc] init];
	
    int width = (int)CVPixelBufferGetWidth(img);
	int height = (int)CVPixelBufferGetHeight(img);
	int bytesPerRow = (int)CVPixelBufferGetBytesPerRow(img);
	if ([ITOpenGLESTexture supportsFastTextureUpload])
	{
		tex->_cvTexture = YES;
		glActiveTexture(GL_TEXTURE0);
		CVReturn err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
														 cache,
														 img,
														 NULL,
														 GL_TEXTURE_2D,
														 GL_RGBA,
														 bytesPerRow / 4,
														 height,
														 rgb == RGB_TypeBGRA ? GL_BGRA : GL_RGBA,
														 GL_UNSIGNED_BYTE,
														 0,
														 &tex->_glTexture);
		if (err) {
			NSLog(@"Error at CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
		}
		GLenum target = CVOpenGLESTextureGetTarget(tex->_glTexture);
		glBindTexture(target, CVOpenGLESTextureGetName(tex->_glTexture));
		glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}
	else
	{
		tex->_cvTexture = NO;
		void *imageData = CVPixelBufferGetBaseAddress(img);
		glGenTextures(1, &tex->_texture);
		glBindTexture(GL_TEXTURE_2D, tex->_texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bytesPerRow / 4, height, 0,
					 rgb == RGB_TypeBGRA ? GL_BGRA : GL_RGBA, GL_UNSIGNED_BYTE, imageData);
	}
    
    tex.texSize = (CGSize) { width, height };
	
	return tex;
}

+ (ITOpenGLESTexture *)generateRenderTextureWithSize:(CGSize)size rgbType:(RGB_Type)rgb {
	ITOpenGLESTexture *tex = [[ITOpenGLESTexture alloc] init];
	tex->_cvTexture = NO;
	glGenTextures(1, &tex->_texture);
	glBindTexture(GL_TEXTURE_2D, tex->_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.width, size.height, 0, rgb == RGB_TypeBGRA ? GL_BGRA : GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	tex.texSize = size;
	return tex;
}

- (GLenum)texTarget
{
	if (_cvTexture)
		return CVOpenGLESTextureGetTarget(_glTexture);
	else
		return GL_TEXTURE_2D;
}

- (GLuint)texName
{
	if (_cvTexture)
		return CVOpenGLESTextureGetName(_glTexture);
	else
		return _texture;
}

- (void)discart
{
	if (_cvTexture &&
		_glTexture != NULL) {
		if (_pBuffer != NULL) {
			CVPixelBufferRelease(_pBuffer);
		}
		CFRelease(_glTexture);
		_glTexture = NULL;
		_pBuffer = NULL;
	} else if (!_cvTexture) {
		glDeleteTextures(1, &_texture);
		_texture = 0;
	}
}

@end

