//
//  ITNavigationController.h
//  ITNavigationController
//
//  Created by Anderson Lucas C. Ramos on 3/30/12.
//  Copyright (c) 2012 42 Tecnologia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ITNavigationControllerDelegate;

@interface ITViewControllerInfo : NSObject

@property (strong, nonatomic) UIViewController *viewController;
@property (strong, nonatomic) UIImage *buttonImage;

- (id)init;

@end

//////////////////////////////////////////////////////////////////////////////////////////

@interface ITNavigationController : UINavigationController

@property (nonatomic, readonly) UIViewController							*rightController;
@property (nonatomic, readonly) UIViewController							*leftController;
@property (strong, nonatomic) id<ITNavigationControllerDelegate>			delegateForLateralControllers;

- (id)initWithRootViewController:(UIViewController *)rootViewController;
- (void)setRightViewController:(ITViewControllerInfo *)info;
- (void)setLeftViewController:(ITViewControllerInfo *)info;
- (void)showRightController:(BOOL)animated;
- (void)showLeftController:(BOOL)animated;
- (void)showRootController:(BOOL)animated;
- (void)showRootController:(BOOL)animated withCompletion:(void (^)(void))completion;
- (void)pushViewController:(UIViewController *)viewController
				  animated:(BOOL)animated;
- (void)pushViewController:(UIViewController *)viewController 
				  animated:(BOOL)animated 
				completion:(void (^)(ITNavigationController *nav, UIViewController *controller, BOOL animated))completion;
- (BOOL)showingLateralController;

@end
