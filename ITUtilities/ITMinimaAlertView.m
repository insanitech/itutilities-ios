//
//  ITMinimaAlertView.m
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 11/6/12.
//
//

#import "ITMinimaAlertView.h"
#import "ITCommonFx.h"
#import "ITUtilities.h"
@import QuartzCore;

@interface ITMinimaAlertView ()

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *cancelButtonTitle;
@property (nonatomic, strong) NSString *confirmButtonTitle;
@property (nonatomic, strong) ITMinimaAlertViewCompletion completion;

@end

@implementation ITMinimaAlertView

@synthesize titleFont = _titleFont;
@synthesize messageFont = _messageFont;
@synthesize buttonsFont = _buttonsFont;
@synthesize textColor = _textColor;
@synthesize backgroundViewColor = _backgroundViewColor;
@synthesize buttonsTextColor = _buttonsTextColor;
@synthesize cancelButtonColor = _cancelButtonColor;
@synthesize confirmButtonColor = _confirmButtonColor;
@synthesize buttonsHoverColor = _buttonsHoverColor;
@synthesize buttonsContentInset = _buttonsContentInset;
@synthesize confirmButtonImage = _confirmButtonImage;
@synthesize numberOfLines = _numberOfLines;
@synthesize messageTextAlignment = _messageTextAlignment;

@synthesize contentView = _contentView;
@synthesize title = _title;
@synthesize message = _message;
@synthesize cancelButtonTitle = _cancelButtonTitle;
@synthesize confirmButtonTitle = _confirmButtonTitle;
@synthesize completion = _completion;

- (void)dealloc {
	self.titleFont = nil;
	self.messageFont = nil;
	self.buttonsFont = nil;
	self.textColor = nil;
	self.backgroundViewColor = nil;
	self.buttonsTextColor = nil;
	self.cancelButtonColor = nil;
	self.buttonsHoverColor = nil;
	self.confirmButtonColor = nil;
	self.confirmButtonImage = nil;
	
	self.contentView = nil;
	self.title = nil;
	self.message = nil;
	self.cancelButtonTitle = nil;
	self.confirmButtonTitle = nil;
	self.completion = nil;
}

- (id)initWithTitle:(NSString *)title message:(NSString *)message
  cancelButtonTitle:(NSString *)cancelButtonTitle
 confirmButtonTitle:(NSString *)confirmButtonTitle
		 completion:(ITMinimaAlertViewCompletion)block
{
    self = [super init];
    if (self) {
		self.title = title;
		self.message = message;
		self.cancelButtonTitle = cancelButtonTitle;
		self.confirmButtonTitle = confirmButtonTitle;
		self.completion = block;
		self.messageTextAlignment = NSTextAlignmentCenter;
		self.textColor = [UIColor whiteColor];
    }
    return self;
}

- (void)showInView:(UIView *)view {
	UIWindow *window = [[UIApplication sharedApplication].windows objectAtIndex:0];
	[self setFrame:window.frame];
	[self setAlpha:0.0f];
	[self setClipsToBounds:YES];
	
	UIImageView *imageView = [[UIImageView alloc] initWithImage:[ITUtilities boxblurFromImage:[ITUtilities screenshotFromView:view] withBlur:0.15f]];
	[imageView setContentMode:UIViewContentModeScaleAspectFit];
	[imageView setFrame:(CGRect) { {0, 0}, {self.frame.size.width, self.frame.size.height} }];
	[self addSubview:imageView];
	
	UIView *greyView = [[UIView alloc] initWithFrame:imageView.frame];
	[greyView setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.8f]];
	[self addSubview:greyView];
	
	self.contentView = [[UIView alloc] init];
	[self.contentView setBackgroundColor:self.backgroundViewColor];
	[self.contentView setAutoresizesSubviews:YES];
	UIViewAutoresizing mask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
	
	float spacing = 10;
	UILabel *titleLabel = [[UILabel alloc] initWithFrame:(CGRect) { {spacing, spacing}, {300, 40} }];
	[titleLabel setBackgroundColor:[UIColor clearColor]];
	[titleLabel setFont:self.titleFont];
	[titleLabel setTextColor:self.textColor];
	[titleLabel setText:self.title];
	
//	CGSize size = [self.title sizeWithFont:self.titleFont forWidth:300 lineBreakMode:NSLineBreakByWordWrapping];
    CGRect rect = [self.title boundingRectWithSize:(CGSize) { 300, CGFLOAT_MAX }
                                           options:NSStringDrawingUsesDeviceMetrics | NSStringDrawingUsesFontLeading
                                        attributes:@{NSFontAttributeName: self.titleFont}
                                           context:nil];
	[titleLabel setFrame:(CGRect) { titleLabel.frame.origin, rect.size }];
	
	UILabel *messageLabel = [[UILabel alloc] initWithFrame:(CGRect) { {spacing * 3, spacing * 3 + rect.size.height}, {320 - (spacing * 6), 40} }];
	[messageLabel setBackgroundColor:[UIColor clearColor]];
	[messageLabel setFont:self.messageFont];
	[messageLabel setTextColor:self.textColor];
	[messageLabel setText:self.message];
	[messageLabel setTextAlignment:NSTextAlignmentCenter];
	[messageLabel setLineBreakMode:NSLineBreakByWordWrapping];
	
	int lines = (int)self.numberOfLines;
	
    [messageLabel setNumberOfLines:lines];
	[messageLabel setFrame:(CGRect) { messageLabel.frame.origin, {320 - (spacing * 6), lines * messageLabel.font.lineHeight} }];
	
	UIButton *confirmButton = [[UIButton alloc] initWithFrame:(CGRect) {
		{160, spacing * 3 + messageLabel.frame.origin.y + messageLabel.frame.size.height}, {160, 50}
	}];
	[confirmButton setBackgroundColor:self.confirmButtonColor];
	[confirmButton.titleLabel setFont:self.buttonsFont];
	[confirmButton setTitle:self.confirmButtonTitle forState:UIControlStateNormal];
	[confirmButton setTitleColor:self.buttonsTextColor forState:UIControlStateNormal];
	[confirmButton setTag:1];
	[confirmButton setContentEdgeInsets:self.buttonsContentInset];
	[confirmButton setImage:self.confirmButtonImage forState:UIControlStateNormal];
	[confirmButton addTarget:self action:@selector(confirmButton:) forControlEvents:UIControlEventTouchDown];
	
	UIButton *cancelButton = [[UIButton alloc] initWithFrame:(CGRect) {
		{0, confirmButton.frame.origin.y},
		confirmButton.frame.size
	}];
	[cancelButton setBackgroundColor:self.cancelButtonColor];
	[cancelButton.titleLabel setFont:self.buttonsFont];
	[cancelButton setTitle:self.cancelButtonTitle forState:UIControlStateNormal];
	[cancelButton setTitleColor:self.buttonsTextColor forState:UIControlStateNormal];
	[cancelButton setTag:0];
	[cancelButton setContentEdgeInsets:self.buttonsContentInset];
	if (self.cancelButtonTitle != nil) {
		[cancelButton addTarget:self action:@selector(confirmButton:) forControlEvents:UIControlEventTouchDown];
	}
	
	[self.contentView setFrame:(CGRect) { {0, 0}, {320, confirmButton.frame.origin.y + confirmButton.frame.size.height} }];
	
	[titleLabel setAutoresizingMask:mask];
	[messageLabel setAutoresizingMask:mask];
	[confirmButton setAutoresizingMask:mask];
	[self.contentView addSubview:titleLabel];
	[self.contentView addSubview:messageLabel];
	[self.contentView addSubview:confirmButton];
	if (cancelButton != nil) {
		[cancelButton setAutoresizingMask:mask];
		[self.contentView addSubview:cancelButton];
	}
	
	[messageLabel setTextAlignment:self.messageTextAlignment];
	
	self.contentView.transform = CGAffineTransformMakeScale(1.0f, 0.01f);
	[self addSubview:self.contentView];
	[self bringSubviewToFront:self.contentView];
	self.contentView.center = self.center;
	
	[window addSubview:self];
	
	[UIView animateWithDuration:0.20f delay:0.0f options:UIViewAnimationCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState
					 animations:^{
						 [self setAlpha:1.0f];
						 self.contentView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
					 } completion:nil];
}

- (void)close {
	[UIView animateWithDuration:0.20f delay:0.0f options:UIViewAnimationCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState
					 animations:^{
						 self.contentView.transform = CGAffineTransformMakeScale(1.0f, 0.01f);
						 [self setAlpha:0.0f];
					 } completion:^(BOOL finished) {
						 for (UIView *v in self.subviews) {
							 [v removeFromSuperview];
						 }
						 [self removeFromSuperview];
					 }];
}

- (void)confirmButton:(UIButton *)sender {
	__unsafe_unretained ITMinimaAlertView *_this = self;
	[ITCommonFx buttonHoverAnimation:sender withHoverColor:self.buttonsHoverColor completion:^{
		if (self.completion != nil) {
			self.completion(_this, sender.tag != 0);
		};
		[self close];
	}];
}

@end


























