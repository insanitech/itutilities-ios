//
//  ITInterfaceOrientation.h
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 27/12/12.
//
//

#import <UIKit/UIKit.h>

@class ITInterfaceOrientation;

typedef void (^ITInterfaceOrientationChanged)(ITInterfaceOrientation *newOrientation);

@interface ITInterfaceOrientation : NSObject

@property (nonatomic, assign, readonly) UIInterfaceOrientation current;

- (id)init;
- (void)startDetectionWithBlock:(ITInterfaceOrientationChanged)block;
- (void)stopDetection;

@end
