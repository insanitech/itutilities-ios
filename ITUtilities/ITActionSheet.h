//
//  ITActionSheet.h
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 22/07/13.
//
//

#import <UIKit/UIKit.h>

@class ITActionSheet;

typedef void (^ITActionSheetButtonBlock)(ITActionSheet *actionSheet, NSInteger buttonIndex);

@interface ITActionSheet : UIActionSheet

- (id)initWithTitle:(NSString *)title
  cancelButtonTitle:(NSString *)cancelButtonTitle
destructiveButtonTitle:(NSString *)destructiveButtonTitle
  otherButtonTitles:(NSArray *)otherButtonTitles
         completion:(ITActionSheetButtonBlock)block;

@end
