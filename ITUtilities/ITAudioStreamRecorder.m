//
//  Speex.m
//  Shot
//
//  Created by Anderson Lucas C. Ramos on 9/1/11.
//  Copyright 2011 Tangivel Tecnologia. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <SpeexLib/ITSpeexEncDec.h>
#import "ITAudioStreamRecorder.h"
#import "ITAudioStreamPlayer.h"
#import "ITAudioQueueCore.h"

static void HandleInputBuffer (void *aqData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer, const AudioTimeStamp *inStartTime, 
                               UInt32 inNumPackets, const AudioStreamPacketDescription *inPacketDesc);
static void DeriveBufferSize (AudioQueueRef audioQueue, AudioStreamBasicDescription ASBDescription, Float64 seconds, UInt32 *outBufferSize);

@interface ITAudioStreamRecorder (private)

- (void)releaseData;
- (id<ITAudioStreamRecorderDelegate>)delegate;
- (ITSpeexEncoder *)encoder;

@end

ITAudioStreamRecorder *_streamRecorder;

@implementation ITAudioStreamRecorder
@synthesize encodeSpeex = _encodeSpeex;

- (id)init
{
    self = [super init];
    if (self) 
    {
		_recordState = malloc(sizeof(AQRecorderState));
		AQRecorderState *recState = (AQRecorderState *)_recordState;
        recState->mDataFormat.mFormatID = AUDIO_FORMAT;
        recState->mDataFormat.mSampleRate = AUDIO_SAMPLE_RATE;
        recState->mDataFormat.mChannelsPerFrame = AUDIO_CHANNELS_PER_FRAME;
        recState->mDataFormat.mBitsPerChannel = AUDIO_BITS_PER_CHANNEL;
        recState->mDataFormat.mBytesPerPacket = AUDIO_BYTES_PER_PACKET;
        recState->mDataFormat.mBytesPerFrame = AUDIO_BYTES_PER_FRAME;
        recState->mDataFormat.mFramesPerPacket = AUDIO_FRAMES_PER_PACKET;
        recState->mDataFormat.mFormatFlags = AUDIO_FLAG_CANONICAL;
        _encodeSpeex = NO;
        _encoder = [[ITSpeexEncoder alloc] initWithBandType:SpeexBandType_Narrow sampleRate:AUDIO_SAMPLE_RATE];
		recState->mIsRunning = NO;
    }
    
    return _streamRecorder = self;
}

- (BOOL)isRecording
{
    return ((AQRecorderState *)_recordState)->mIsRunning;
}

- (ITSpeexEncoder *)encoder
{
    return _encoder;
}

- (BOOL)startRecording
{
    if (!_initialized)
    {
        OSStatus result = AudioQueueNewInput(&((AQRecorderState *)_recordState)->mDataFormat, 
											 HandleInputBuffer, 
											 ((AQRecorderState *)_recordState), 
											 NULL, kCFRunLoopCommonModes, 0, 
											 &((AQRecorderState *)_recordState)->mQueue);
        if (result == noErr)
        {
            ((AQRecorderState *)_recordState)->bufferByteSize = AUDIO_BUFFER_BYTE_SIZE;
            
            for (int i = 0; i < kNumberBuffers; ++i)
            {
                AudioQueueAllocateBuffer(((AQRecorderState *)_recordState)->mQueue, 
										 ((AQRecorderState *)_recordState)->bufferByteSize, 
										 &((AQRecorderState *)_recordState)->mBuffers[i]);
                AudioQueueEnqueueBuffer(((AQRecorderState *)_recordState)->mQueue, 
										((AQRecorderState *)_recordState)->mBuffers[i], 0, NULL);
            }
            
            ((AQRecorderState *)_recordState)->mCurrentPacket = 0;
            ((AQRecorderState *)_recordState)->mIsRunning = YES;
            
            AudioQueueStart(((AQRecorderState *)_recordState)->mQueue, NULL);
            _initialized = YES;
        }
        else
        {
            NSLog(@"Error startRecording: %i", (int)result);
            _initialized = NO;
        }
    }
    else
    {
        ((AQRecorderState *)_recordState)->mIsRunning = YES;
        AudioQueueStart(((AQRecorderState *)_recordState)->mQueue, NULL);
    }
    
    return ((AQRecorderState *)_recordState)->mIsRunning;
}

- (void)stopRecording
{
    AudioQueueFlush(((AQRecorderState *)_recordState)->mQueue);
    AudioQueuePause(((AQRecorderState *)_recordState)->mQueue);
    ((AQRecorderState *)_recordState)->mIsRunning = NO;
}

- (void)releaseData
{
    AudioQueueStop(((AQRecorderState *)_recordState)->mQueue, YES);
    AudioQueueFlush(((AQRecorderState *)_recordState)->mQueue);
    AudioQueueDispose(((AQRecorderState *)_recordState)->mQueue, YES);
}

- (AQRecorderState)recState
{
    return *((AQRecorderState *)_recordState);
}

- (void)setStreamDelegate:(id<ITAudioStreamRecorderDelegate>)del
{
    _delegate = del;
}

- (id<ITAudioStreamRecorderDelegate>)delegate
{
    return _delegate;
}

- (UInt32)bufferByteSize
{
    return ((AQRecorderState *)_recordState)->bufferByteSize;
}

- (void)setGiveOutway:(BOOL)ow
{
    _giveOutway = ow;
}

void HandleInputBuffer (void *aqData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer, const AudioTimeStamp *inStartTime, 
                        UInt32 inNumPackets, const AudioStreamPacketDescription *inPacketDesc)
{
    AQRecorderState *pAqData = (AQRecorderState *) aqData;
       
    @autoreleasepool 
    {
        if ([_streamRecorder delegate] != nil &&
            _streamRecorder->_giveOutway)
        {
            if ([_streamRecorder encodeSpeex])
            {
                short *sData = calloc(inNumPackets, sizeof(short));
                memcpy(sData, inBuffer->mAudioData, inBuffer->mAudioDataByteSize);
                SpeexEncodedPackets *data = [[_streamRecorder encoder] encodePacket:sData size:inBuffer->mAudioDataByteSize];
                [[_streamRecorder delegate] recorderBufferEnqueued:data];
                FreeSpeexEncPacket(data);
                free(sData);
            }
            
            [[_streamRecorder delegate] recorderBufferEnqueued:inBuffer->mAudioData size:inBuffer->mAudioDataByteSize];
        }
    }
    
    AudioQueueEnqueueBuffer(pAqData->mQueue, inBuffer, 0, NULL);
}

- (void)dealloc
{
    [self releaseData];
	free((void *)_recordState);
}

@end
