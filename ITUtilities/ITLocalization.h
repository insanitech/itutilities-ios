//
//  ITLocalization.h
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 3/23/12.
//  Copyright (c) 2012 InsaniTech. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ITLocalizationUpdateBlock)(BOOL updated);

@interface ITLocalization : NSObject

@property (nonatomic, strong, readonly) NSString *lang;

+ (ITLocalization *)sharedInstance;
+ (void)cleanInstance;

- (id)init;
- (NSString *)valueForKey:(NSString *)key;
- (void)checkForUpdates:(NSURL *)updateURL updateGetURL:(NSURL *)getURL completion:(ITLocalizationUpdateBlock)block;
- (void)loadEmbedTranslation;

@end

NSString *ITLocalizedKey(NSString *key);
