//
//  ITRecordManager.h
//  SocialVoice
//
//  Created by Anderson Lucas C. Ramos on 11/4/12.
//  Copyright (c) 2012 Anderson Lucas C. Ramos. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ITAudioRecordCompletion)(NSData *);
typedef void (^ITAudioPlayCompletion)(BOOL finished);

@interface ITAudioManager : NSObject

@property (nonatomic, assign) NSTimeInterval maxRecordTime;

- (id)init;
- (void)startRecoringWithCompletion:(ITAudioRecordCompletion)completion;
- (void)finishRecording;
- (void)playEncodedAudioData:(NSData *)audioData completion:(ITAudioPlayCompletion)completion;
- (void)stopPlayingAudioData;
- (BOOL)isRecording;
- (float)currentPlaying;

@end