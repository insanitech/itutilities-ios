//
//  ITTypes.h
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 11/2/12.
//
//

#import <Foundation/Foundation.h>

#ifndef BYTE
typedef int8_t byte;
#endif
#ifndef UBYTE
typedef uint8_t ubyte;
#endif
