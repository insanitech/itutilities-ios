//
//  NSMutableArray+Queue.m
//  Shot
//
//  Created by Anderson Lucas C. Ramos on 9/6/11.
//  Copyright 2011 Tangivel Tecnologia. All rights reserved.
//

#import "ITQueue.h"

@interface ITQueueItem : NSObject

@property (strong, nonatomic) id data;
@property (strong, nonatomic) ITQueueItem *next;
@property (strong, nonatomic) ITQueueItem *prev;

- (id)init;

@end

@implementation ITQueueItem

@synthesize data = _data;
@synthesize next = _next;
@synthesize prev = _prev;

- (void)dealloc {
	[self setData:nil];
	[self setNext:nil];
	[self setPrev:nil];
}

- (id)init {
	self = [super init];
	if (self != nil) {
		[self setData:nil];
		[self setNext:nil];
		[self setPrev:nil];
	}
	return self;
}

@end

@interface ITQueue ()

@property (strong, nonatomic) ITQueueItem *first;
@property (strong, nonatomic) ITQueueItem *next;
@property (strong, nonatomic) ITQueueItem *last;

@end

@implementation ITQueue

//@synthesize first = _first;
//@synthesize next = _next;
//@synthesize last = _last;

- (id)init
{
    self = [super init];
    if (self != nil)
    {
        [self setFirst:nil];
		[self setNext:nil];
		[self setLast:nil];
        _count = 0;
    }
    return self;
}

- (NSInteger)count
{
    return _count;
}

- (void)setCount:(NSInteger)c
{
    _count = c;
}

- (id)dequeue
{
    if (self.count == 1)
    {
        ITQueueItem *remove = self.first;
        [self setLast:nil];
		[self setFirst:nil];
        id ret = remove.data;
        _count--;
        return ret;
    }
    else if (_count > 1)
    {
        ITQueueItem *remove = self.last;
		[self setLast:self.last.prev];
		[self.last setNext:nil];
		id ret = remove.data;
        _count--;
        return ret;
    }
    _count = 0;
    return nil;
}

- (void)enqueue:(id)object
{
    if (self.first == NULL) {
		[self setFirst:[[ITQueueItem alloc] init]];
		[self.first setData:object];
		[self setLast:self.first];
    } else {
        ITQueueItem *new = [[ITQueueItem alloc] init];
		[new setData:object];
		[new setNext:self.first];
		[self.first setPrev:new];
		[self setFirst:new];
    }
    _count++;
}

- (id)objectAtIndex:(NSInteger)index
{
    id retData = NULL;
    if (index == 0 && _count > 0) {
        retData = self.first.data;
    } else if (index == _count - 1) {
        retData = self.last.data;
    } else if (index > 0 && index < _count) {
        ITQueueItem *item = self.first;
        for (int i = 0; i < _count; i++) {
            if (index == i) {
                retData = item.data;
                break;
            }
            item = self.first.next;
        }
    }
    return retData;
}

- (void)dealloc
{
    while (_count > 0) {
		[self dequeue];
	}
}

@end

@implementation ITMutableQueue

- (id)removeObject:(id)object
{
    if (object != NULL) {
        ITQueueItem *item = self.first;
        while (item != object && item != NULL) {
            item = self.first.next;
		}
        
        if (item != NULL) {
            if (item != self.first && item != self.last) {
                ITQueueItem *prev = item.prev;
                ITQueueItem *next = item.next;
				[prev setNext:next];
				[next setPrev:prev];
            } else {
                if (item == self.first) {
					[self setFirst:item.next];
                } else if (item == self.last) {
					[self.last.prev setNext:nil];
				}
            }
            _count--;
            id itemData = item.data;
			[item setNext:nil];
			[item setPrev:nil];
            return itemData;
        }
    }
    return NULL;
}

@end


















