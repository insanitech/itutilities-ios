//
//  MaritalPickerView.m
//  AntSocial
//
//  Created by Anderson Lucas C. Ramos on 5/21/12.
//  Copyright (c) 2012 Tangível Tecnologia. All rights reserved.
//

#import "ITDatePickerView.h"

@interface ITDatePickerView ()

@property (strong, nonatomic) UIDatePicker *picker;
@property (strong, nonatomic) NSDate *pickedDate;
@property (strong, nonatomic) UIToolbar *accessoryToolbar;
@property (strong, nonatomic) ITPickerDidFinishPicking block;
@property (strong, nonatomic) ITPickerAccessoryButtonPressed backBlock;
@property (strong, nonatomic) ITPickerAccessoryButtonPressed nextBlock;
@property (assign, nonatomic) BOOL isShowing;

@end

@implementation ITDatePickerView

@synthesize picker = _picker;
@synthesize pickedDate = _pickedDate;
@synthesize accessoryToolbar = _accessoryToolbar;
@synthesize block = _block;
@synthesize backBlock = _backBlock;
@synthesize nextBlock = _nextBlock;
@synthesize isShowing = _isShowing;

- (void)dealloc {
    [self setPicker:nil];
    [self setPickedDate:nil];
    [self setAccessoryToolbar:nil];
    [self setBlock:nil];
    [self setBackBlock:nil];
    [self setNextBlock:nil];
}

- (id)init {
    self = [super init];
    if (self != nil) {
		[self setBackgroundColor:[UIColor blackColor]];
		self.picker = [[UIDatePicker alloc] init];
        [self.picker setMaximumDate:[NSDate date]];
        [self.picker setDatePickerMode:UIDatePickerModeDate];
        CGRect f = self.picker.frame;
        f.size.height += 44;
        [self setFrame:f];
        [self.picker setBackgroundColor:[UIColor blackColor]];
        f.size.height -= 44;
        f.origin.y = 44;
        [self.picker setFrame:f];
        [self addSubview:self.picker];
        
        [self setAccessoryToolbar:[[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)]];
        
        self.accessoryToolbar.barStyle = UIBarStyleDefault;
        [self.accessoryToolbar setItems: [NSArray arrayWithObjects:
                                    [[UIBarButtonItem alloc] initWithTitle:@"Anterior" style:UIBarButtonItemStylePlain target:self action:@selector(previous:)],
                                    [[UIBarButtonItem alloc] initWithTitle:@"Póximo" style:UIBarButtonItemStylePlain target:self action:@selector(next:)],
                                    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                    [[UIBarButtonItem alloc] initWithTitle:@"Fechar" style:UIBarButtonItemStyleDone target:self action:@selector(doneButton:)],
                                    nil]];
        for (UIBarButtonItem *item in self.accessoryToolbar.items) {
            [item setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor blueColor]} forState:UIControlStateNormal];
        }
        
        [self addSubview:self.accessoryToolbar];
        [self setIsShowing:NO];
    }
    return self;
}

- (void)setDidFinishPicking:(ITPickerDidFinishPicking)result {
	[self setBlock:result];
}

- (void)setBackButtonPressed:(ITPickerAccessoryButtonPressed)block {
    [self setBackBlock:block];
}

- (void)setNextButtonPressed:(ITPickerAccessoryButtonPressed)block {
    [self setNextBlock:block];
}

- (void)previous:(id)sender {
    if (self.backBlock != nil) {
        self.backBlock(self);
    }
}

- (void)next:(id)sender {
    if (self.nextBlock != nil) {
        self.nextBlock(self);
    }
}

- (void)doneButton:(UIButton *)button {
	if (self.block != nil) {
		self.pickedDate = self.picker.date;
		self.block(self, self.pickedDate);
	}
}

- (void)showPicker {
    if (!self.isShowing) {
        UIWindow *window = [[UIApplication sharedApplication].windows objectAtIndex:0];

        [window addSubview:self];
        CGRect frame = self.frame;
        frame.origin.y = window.frame.size.height;
        [self setFrame:frame];
        [window bringSubviewToFront:self];
        
        [UIView animateWithDuration:0.15f delay:0.0f
                            options:UIViewAnimationCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.center = (CGPoint) {
                                 window.frame.size.width * 0.5f,
                                 window.frame.size.height - self.frame.size.height * 0.5f
                             };
                         } completion:^(BOOL finished) {
                             [self setIsShowing:YES];
                         }];
    }
}

- (void)hidePicker {
    if (self.isShowing) {
        UIWindow *window = [[UIApplication sharedApplication].windows objectAtIndex:0];
        [UIView animateWithDuration:0.15f delay:0.0f
                            options:UIViewAnimationCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             [self setFrame:(CGRect) {
                                 {0, window.frame.size.height},
                                 self.frame.size
                             }];
                         } completion:^(BOOL finished) {
                             [self removeFromSuperview];
                             [self setIsShowing:NO];
                         }];
    }
}

@end




















