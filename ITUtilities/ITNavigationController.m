//
//  ITNavigationController.m
//  ITNavigationController
//
//  Created by Anderson Lucas C. Ramos on 3/30/12.
//  Copyright (c) 2012 Tangível Tecnologia. All rights reserved.
//

@import QuartzCore;

#import "ITNavigationController.h"
#import "ITNavigationControllerDelegate.h"
#import "ITUtilities.h"
#import "ITTimer.h"

static const float kMenuSlideDuration 	= 0.3f;

@implementation ITViewControllerInfo

@synthesize buttonImage = _buttonImage;
@synthesize viewController = _viewController;

- (void)dealloc {
    [self setButtonImage:nil];
    [self setViewController:nil];
    
    [ITUtilities deallocLog:[self class]];
}

- (id)init
{
	self = [super init];
	if (self != nil)
	{
		
	}
	return self;
}

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@interface ITNavigationController (private) <UIGestureRecognizerDelegate, UIPopoverControllerDelegate>

+ (UIBarButtonItem *)barButtonWithImage:(UIImage *)image target:(id)target action:(SEL)selector;
+ (UIBarButtonItem *)barButtonWithImage:(UIImage *)image title:(NSString *)title target:(id)target action:(SEL)selector;

- (void)showLeft:(UIBarButtonItem *)sender;
- (void)showRight:(UIBarButtonItem *)sender;

- (void)pan:(UIPanGestureRecognizer *)gesture;

- (UIViewController *)leftController;
- (UIViewController *)rightController;

- (UIBarButtonItem *)rightBarButton;
- (UIBarButtonItem *)leftBarButton;

- (void)iPadRightViewHide:(UIButton *)sender;

- (void)showShadow:(BOOL)show;

@end

@implementation ITNavigationController
{	
	UIViewController							*_rightController;
	UIViewController							*_leftController;
	
	UIPopoverController						*_popover;
	UIView										*_rightView;
	
	UIPanGestureRecognizer						*_panGesture;
	
	struct
	{
		BOOL canShowRight : 1;
		BOOL canShowLeft : 1;
		BOOL showingRightController	: 1;
		BOOL showingLeftController : 1;
		BOOL leftSide : 1;
	} _flags;
}

@synthesize delegateForLateralControllers				= _delegateForLateralControllers;

+ (UIBarButtonItem *)barButtonWithImage:(UIImage *)image target:(id)target action:(SEL)selector {
    return [ITNavigationController barButtonWithImage:image title:nil target:target action:selector];
}

+ (UIBarButtonItem *)barButtonWithImage:(UIImage *)image title:(NSString *)title target:(id)target action:(SEL)selector
{
    UIImage *buttonImage = image;
	if (buttonImage != nil)
	{
		UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
		if (title != nil)
		{
			UIEdgeInsets edge = UIEdgeInsetsZero;
			edge.left = 5;
			[doneButton setContentEdgeInsets:edge];
			[doneButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:[UIFont systemFontSize] - 1]];
			[doneButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
			[doneButton setTitle:title forState:UIControlStateNormal];
			[doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		}
		[doneButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
		[doneButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
		CGRect r = doneButton.frame;
		r.size = image.size;
		doneButton.frame = r;
		return [[UIBarButtonItem alloc] initWithCustomView:doneButton];
	}
	return [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:target action:selector];
}

- (void)initiate
{
	_flags.canShowLeft = NO;
	_flags.canShowRight = NO;
	_flags.showingLeftController = NO;
	_flags.showingRightController = NO;
	
	_rightController = nil;
	_leftController = nil;

	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
	{
		_panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
		[_panGesture setDelegate:self];
		[self.view addGestureRecognizer:_panGesture];
	}
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self != nil)
	{
		[self initiate];
	}
	return self;
}

- (id)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithRootViewController:rootViewController];
    if (self != nil)
	{
        if (self.topViewController == nil) {
            [super pushViewController:rootViewController animated:NO];
        }
        
		[self initiate];
    }
    return self;
}

- (BOOL)shouldAutorotate {
    return [self.visibleViewController shouldAutorotate];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.visibleViewController supportedInterfaceOrientations];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return [self.visibleViewController preferredStatusBarStyle];
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return [self.visibleViewController preferredStatusBarUpdateAnimation];
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
//{
//	return [self.visibleViewController shouldAutorotateToInterfaceOrientation:toInterfaceOrientation];
//}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
		[self showShadow:NO];
		CGRect viewFrame = self.view.frame;
		CGRect rightVFrame = self.rightController.view.frame;
		CGRect leftVFrame = self.leftController.view.frame;
		
		rightVFrame.origin.x = [ITUtilities deviceSizeToInterface:toInterfaceOrientation].width - rightVFrame.size.width;
		rightVFrame.size.height = [ITUtilities deviceSizeToInterface:toInterfaceOrientation].height - 20;
		leftVFrame.size.height = [ITUtilities deviceSizeToInterface:toInterfaceOrientation].height - 20;
		if (_flags.showingRightController) {
			viewFrame.origin.x = rightVFrame.origin.x - viewFrame.size.width;
			viewFrame.size.height = rightVFrame.size.height + 20;
		}
		if (_flags.showingLeftController) {
			viewFrame.origin.x = leftVFrame.size.width;
			viewFrame.size.height = leftVFrame.size.height + 20;
		}
		
		__unsafe_unretained ITNavigationController *_this = self;
		if (_flags.showingLeftController || _flags.showingRightController) {
			[UIView animateWithDuration:duration + 0.05f animations:^ {
				[_this.view setFrame:viewFrame];
				if (_this->_flags.showingRightController)
					[_this.rightController.view setFrame:rightVFrame];
				else if (_this->_flags.showingLeftController)
					[_this.leftController.view setFrame:leftVFrame];
			
				CGRect navBarFrame = _this.navigationBar.frame;
				navBarFrame.size.height = UIInterfaceOrientationIsLandscape(toInterfaceOrientation) ? 32.0f : 44.0f;
				[_this.navigationBar setFrame:navBarFrame];
			} completion:^(BOOL finished) {
				[_this showShadow:YES];
			}];
		} else {
			[UIView animateWithDuration:duration + 0.05f animations:^ {
				CGRect navBarFrame = _this.navigationBar.frame;
				navBarFrame.size.height = UIInterfaceOrientationIsLandscape(toInterfaceOrientation) ? 32.0f : 44.0f;
				[_this.navigationBar setFrame:navBarFrame];
			}];
		}
		
		if (!_flags.showingRightController) {
			[self.rightController.view setFrame:rightVFrame];
        }
		if (!_flags.showingLeftController) {
			[self.leftController.view setFrame:leftVFrame];
        }
	}
	else
	{
		if (_flags.showingRightController) {
			[self.rightController.view.superview setFrame:(CGRect) {
                {0, 0}, [ITUtilities deviceSizeToInterface:toInterfaceOrientation]
            }];
        }
	}
	
	[self.visibleViewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[self.visibleViewController didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer {
	CGPoint translation = [gestureRecognizer translationInView:self.view];
	if (ABS(translation.x) > 2.5f) {
		return YES;
    }
	return NO;
}

- (void)pan:(UIPanGestureRecognizer *)gesture {
	if (gesture.state == UIGestureRecognizerStateBegan) {
		CGPoint beginPoint = [gesture locationInView:self.view];
		_flags.leftSide = (beginPoint.x < self.view.frame.size.width * 0.5f);
	}
	
	if (gesture.state == UIGestureRecognizerStateChanged) {
		CGPoint locSuperview = [gesture locationInView:self.view.superview];

		CGRect f = gesture.view.frame;
		f.origin.y = gesture.view.frame.origin.y;
		
		if (_flags.leftSide) {
			f.origin.x = locSuperview.x - 15.0f;
		} else {
			f.origin.x = locSuperview.x - self.view.frame.size.width + 15.0f;
        }
		
		if (f.origin.x > 0 && _flags.canShowLeft) {
			if (self.leftController.view.superview == nil) {
				if (self.rightController.view.superview != nil) {
					[self.rightController.view removeFromSuperview];
                }
				
				[self showShadow:YES];
				[self.view.superview insertSubview:[self leftController].view belowSubview:self.view];
			}
		} else if (f.origin.x < 0 && _flags.canShowRight) {
			if (self.rightController.view.superview == nil) {
				if (self.leftController.view.superview != nil) {
					[self.leftController.view removeFromSuperview];
                }
				
				[self showShadow:YES];
				[self.view.superview insertSubview:[self rightController].view belowSubview:self.view];
				CGRect r = self.rightController.view.frame;
				r.origin.x = [ITUtilities deviceSize].width - r.size.width;
				[self.rightController.view setFrame:r];
			}
		} else {
			[self showShadow:NO];
			if (self.leftController.view.superview != nil) {
				[self.leftController.view removeFromSuperview];
            }
			if (self.rightController.view.superview != nil) {
				[self.rightController.view removeFromSuperview];
            }
		}
		
		if (f.origin.x < 0 && !_flags.canShowRight) {
			f.origin.x = 0;
        }
		if (f.origin.x > 0 && !_flags.canShowLeft) {
			f.origin.x = 0;
        }
		
		[gesture.view setFrame:f];
	}
	
	if (gesture.state == UIGestureRecognizerStateFailed ||
		gesture.state == UIGestureRecognizerStateCancelled ||
		gesture.state == UIGestureRecognizerStateEnded) {
		CGPoint translation = [gesture translationInView:self.view.superview];
		CGFloat finalWidth = [ITUtilities deviceSize].width;
			
		if (translation.x > 0) {
			if (self.view.center.x > finalWidth) {
				[self showLeftController:YES];
            } else {
				[self showRootController:YES];
            }
		} else {
			if (self.view.center.x < 0) {
				[self showRightController:YES];
            } else {
				[self showRootController:YES];
            }
		}
	}
}

- (BOOL)showingLateralController {
    return _flags.showingLeftController || _flags.showingRightController;
}

- (UIViewController *)leftController {
	return _leftController;
}

- (UIViewController *)rightController {
	return _rightController;
}

- (UIBarButtonItem *)leftBarButton {
	return self.leftController.navigationItem.leftBarButtonItem;
}

- (UIBarButtonItem *)rightBarButton {
	return self.rightController.navigationItem.rightBarButtonItem;
}

- (void)setLeftViewController:(ITViewControllerInfo *)info {
	NSAssert(self.viewControllers.count > 0, @"Must have a root controller set.");
	if (info != nil) {
		[info.viewController.view setClipsToBounds:YES];
//		[info.viewController.view.layer setCornerRadius:4.0f];
		UIBarButtonItem *leftButton = [ITNavigationController barButtonWithImage:info.buttonImage target:self action:@selector(showLeft:)];
		[self.visibleViewController.navigationItem setLeftBarButtonItem:leftButton];
		_leftController = info.viewController;
		_flags.canShowLeft = YES;
	}
	else 
	{
		UIViewController *controller = [self leftController];
		if (controller != nil) {
            _leftController = nil;
        }
		[self.visibleViewController.navigationItem setLeftBarButtonItem:nil];
		_flags.canShowLeft = NO;
	}
}

- (void)setRightViewController:(ITViewControllerInfo *)info {
	NSAssert(self.viewControllers.count > 0, @"Must have a root controller set.");
	if (info != nil && info.viewController != nil) {
		[info.viewController.view setClipsToBounds:YES];
//		[info.viewController.view.layer setCornerRadius:4.0f];
		UIBarButtonItem *rightButton = [ITNavigationController barButtonWithImage:info.buttonImage target:self action:@selector(showRight:)];
		[self.visibleViewController.navigationItem setRightBarButtonItem:rightButton];
		_rightController = info.viewController;
		_flags.canShowRight = YES;
	} else {
		UIViewController *controller = [self rightController];
		if (controller != nil) {
            _rightController = nil;
        }
		[self.visibleViewController.navigationItem setRightBarButtonItem:nil];
		_flags.canShowRight = NO;
	}
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
	[self pushViewController:viewController animated:animated completion:nil];
}

- (void)pushViewController:(UIViewController *)viewController 
				  animated:(BOOL)animated 
				completion:(void (^)(ITNavigationController *, UIViewController *, BOOL))completion {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
		if (!_flags.showingLeftController &&
			!_flags.showingRightController) {
			[super pushViewController:viewController animated:animated];
			if (completion != nil && animated) {
				[[[[ITTimer alloc] initWithTimeInterval:0.5f userInfo:nil repeats:NO]
				 	setBlockTarget:^(ITTimer *timer) {
						completion(self, viewController, animated);
				 	}] start];
			} else if (completion != nil) {
				completion(self, viewController, animated);
            }
		} else if (_flags.showingLeftController) {
			CGRect frame = self.view.frame;
			frame.origin.x = [ITUtilities deviceSize].width;
			__unsafe_unretained ITNavigationController *_this = self;
			[UIView animateWithDuration:kMenuSlideDuration 
								  delay:kMenuSlideDuration
								options:UIViewAnimationOptionTransitionNone
							 animations:^ {
				[self.view setFrame:frame];
			} completion:^(BOOL finished) {
				UIViewController *root = [self.viewControllers objectAtIndex:0];
				if (viewController == root) {
					[super popToViewController:viewController animated:NO];
				} else if ([self visibleViewController] != viewController) {
					[super pushViewController:viewController animated:NO];
                }
				[_this showRootController:YES];
				if (completion != nil && animated) {
					[[[[ITTimer alloc] initWithTimeInterval:0.5f userInfo:nil repeats:NO] setBlockTarget:^(ITTimer *timer) {
						completion(_this, viewController, animated);
					}] start];
                } else if (completion != nil) {
					completion(_this, viewController, animated);
                }
			}];
		} else if (_flags.showingRightController) {
			UIView *animationView = [[UIView alloc] initWithFrame:(CGRect) { { [ITUtilities deviceSize].width, 0 }, [ITUtilities deviceSize] }];
			[animationView setBackgroundColor:[UIColor clearColor]];
			
			[animationView addSubview:viewController.view];
			CGRect r = self.view.superview.frame;
			r.size.width = [ITUtilities deviceSize].width * 2;
			[self.view.superview setFrame:r];
			
			[self.view.superview addSubview:animationView];
			
			[UIView animateWithDuration:0.3f
							 animations:^
			 {
				 CGRect r = self.view.superview.frame;
				 r.origin.x = -[ITUtilities deviceSize].width;
				 [self.view.superview setFrame:r];
			 } 
							 completion:^(BOOL finished) 
			 {            
				 [viewController.view removeFromSuperview];
				 [animationView removeFromSuperview];
				 
				 CGRect r = viewController.view.frame;
				 r.origin.y = 0;
				 [viewController.view setFrame:r];
				 
				 r = self.view.superview.frame;
				 r.origin.x = 0;
				 r.size.width = [ITUtilities deviceSize].width;
				 [self.view.superview setFrame:r];
				 
				 if ([self visibleViewController] != viewController)
					 [super pushViewController:viewController animated:NO];
				 
				 if (completion != nil)
					 completion(self, viewController, animated);
			 }];
		}
	} else {
		if (_flags.showingLeftController) {
			[_popover dismissPopoverAnimated:YES];
			_flags.showingLeftController = NO;
			if (viewController != self.visibleViewController) {
				if ([self.viewControllers containsObject:viewController]) {
					[super popToViewController:viewController animated:YES];
				} else {
					[super pushViewController:viewController animated:animated];
				}
				if (completion != nil && animated) {
					[[[[ITTimer alloc] initWithTimeInterval:0.5f userInfo:nil repeats:NO] setBlockTarget:^(ITTimer *timer) {
						completion(self, viewController, animated);
					}] start];
				} else if (completion != nil) {
					completion(self, viewController, animated);
                }
			}
		}
		
		if (_flags.showingRightController) {
			[self showRight:nil];
			_flags.showingRightController = NO;
			[[[[ITTimer alloc] initWithTimeInterval:0.5f userInfo:nil repeats:NO] setBlockTarget:^(ITTimer *timer) {
				[super pushViewController:viewController animated:animated];
				if (completion != nil && animated) {
					[timer setBlockTarget:^(ITTimer *timer) {
						completion(self, viewController, animated);
					}];
					[timer start];
				} else if (completion != nil) {
					completion(self, viewController, animated);
				}
			}] start];
		}
	}
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
	if (_popover == popoverController) {
		_flags.showingLeftController = NO;
		[_popover dismissPopoverAnimated:YES];
		_popover = nil;
	}
}

- (void)showLeft:(UIBarButtonItem *)sender {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
		if (_rightController.view.superview != nil) {
			[_rightController.view removeFromSuperview];
        }
		if (!_flags.showingLeftController) {
			[self showLeftController:YES];
        } else {
			[self showRootController:YES];
        }
	} else if (!_flags.showingLeftController) {
		_flags.showingLeftController = YES;
		_popover = [[UIPopoverController alloc] initWithContentViewController:self.leftController];
		[_popover setDelegate:self];
		[_popover presentPopoverFromRect:(CGRect) { {0, 0}, {20, 20} } inView:self.visibleViewController.view
				permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
	} else {
		_flags.showingLeftController = NO;
		[_popover dismissPopoverAnimated:YES];
		_popover = nil;
	}
}

- (void)showRight:(UIBarButtonItem *)sender
{
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
	{
		if (_leftController.view.superview != nil)
			[_leftController.view removeFromSuperview];
		if (!_flags.showingRightController)
			[self showRightController:YES];
		else
			[self showRootController:YES];
	}
	else if (!_flags.showingRightController)
	{
		_flags.showingRightController = YES;
		UIViewController *right = self.rightController;
		
		_rightView = [[UIView alloc] initWithFrame:(CGRect) { { [ITUtilities deviceSize].width, 0 }, [ITUtilities deviceSize] }];
		[_rightView setBackgroundColor:[UIColor clearColor]];

		UIButton *hideButton = [[UIButton alloc] initWithFrame:(CGRect) { CGPointZero, _rightView.frame.size }];
		[hideButton setBackgroundColor:[UIColor clearColor]];
		[hideButton addTarget:self action:@selector(iPadRightViewHide:) forControlEvents:UIControlEventTouchUpInside];
		[_rightView addSubview:hideButton];
		
		[_rightView addSubview:right.view];
		
		CGRect rightFrame = right.view.frame;
		rightFrame.origin.x = _rightView.frame.size.width - rightFrame.size.width;
		[right.view setFrame:rightFrame];
		
		[self.view.superview insertSubview:_rightView aboveSubview:self.view];

		CGRect frame = _rightView.frame;
		frame.origin.x = 0;
		[UIView animateWithDuration:0.5f animations:^
		{
			[_rightView setFrame:frame];
		}];
		[self showShadow:YES];
	}
	else
	{
		[self iPadRightViewHide:nil];
	}
}

- (void)iPadRightViewHide:(UIButton *)sender
{
	_flags.showingRightController = NO;
	CGRect frame = _rightView.frame;
	frame.origin.x = [ITUtilities deviceSize].width;
	__unsafe_unretained ITNavigationController *_this = self;
	[UIView animateWithDuration:0.5f animations:^ {
		 [_rightView setFrame:frame];
	 } completion:^(BOOL finished) {
		 [_this.rightController.view removeFromSuperview];
		 [_rightView removeFromSuperview];
		 _rightView = nil;
	 }];
	[self showShadow:NO];
}

- (void)showLeftController:(BOOL)animated {
	if (_flags.canShowLeft)
	{
		__unsafe_unretained ITNavigationController *_this = self;
		if ([_delegateForLateralControllers respondsToSelector:@selector(navigationController:willShowViewController:)]) {
			[_delegateForLateralControllers navigationController:self willShowViewController:[self leftController]];
        }
		
		_flags.showingLeftController = YES;
		[self showShadow:YES];
		
		UIView *view = self.leftController.view;
		CGRect vFrame = view.frame;
		vFrame.origin.y = 0;
		[view setFrame:vFrame];
		
		[self.view.superview insertSubview:view belowSubview:self.view];
		
		CGRect frame = self.view.frame;
		frame.origin.x = view.frame.size.width - 6;
		
		if (animated) {
			[UIView animateWithDuration:kMenuSlideDuration animations:^ {
				[self.view setFrame:frame];
			} completion:^(BOOL finished) {
				[_this.visibleViewController.view setUserInteractionEnabled:NO];
				if ([_delegateForLateralControllers respondsToSelector:@selector(navigationController:didShowViewController:)])
					[_delegateForLateralControllers navigationController:_this didShowViewController:[_this leftController]];
			}];
		} else {
			[self.view setFrame:frame];
			[_this.visibleViewController.view setUserInteractionEnabled:NO];
			if ([_delegateForLateralControllers respondsToSelector:@selector(navigationController:didShowViewController:)])
				[_delegateForLateralControllers navigationController:_this didShowViewController:[_this leftController]];
		}
	}
}

- (void)showRightController:(BOOL)animated {
	if (_flags.canShowRight) {
		__unsafe_unretained ITNavigationController *_this = self;
		if ([_delegateForLateralControllers respondsToSelector:@selector(navigationController:willShowViewController:)]) {
			[_delegateForLateralControllers navigationController:self willShowViewController:[self rightController]];
        }
		
		_flags.showingRightController = YES;
		[self showShadow:YES];
		
		UIView *view = self.rightController.view;
		CGRect vFrame = view.frame;
		vFrame.origin.x = [ITUtilities deviceSize].width - vFrame.size.width;
		[view setFrame:vFrame];
		
		[self.view.superview insertSubview:view belowSubview:self.view];
		
		CGRect frame = self.view.frame;
		frame.origin.x = view.frame.origin.x - frame.size.width + 6;
		
		if (animated)
		{
			[UIView animateWithDuration:kMenuSlideDuration animations:^ {
				 [_this.view setFrame:frame];
			 } completion:^(BOOL finished) {
				 [_this.visibleViewController.view setUserInteractionEnabled:NO];
				 if ([_delegateForLateralControllers respondsToSelector:@selector(navigationController:didShowViewController:)]) {
					 [_delegateForLateralControllers navigationController:_this didShowViewController:[_this rightController]];
                 }
			 }];
		}
		else
		{
			[self.view setFrame:frame];
			[_this.visibleViewController.view setUserInteractionEnabled:NO];
			if ([_delegateForLateralControllers respondsToSelector:@selector(navigationController:didShowViewController:)]) {
				[_delegateForLateralControllers navigationController:_this didShowViewController:[_this rightController]];
            }
		}
	}
}

- (void)showRootController:(BOOL)animated {
	[self showRootController:animated withCompletion:nil];
}

- (void)showRootController:(BOOL)animated withCompletion:(void (^)(void))completion
{
    __unsafe_unretained ITNavigationController *_this = self;
	UIViewController *showingController;
	if (_flags.showingLeftController)
		showingController = [self leftController];
	if (_flags.showingRightController)
		showingController = [self rightController];
	
	if ([_delegateForLateralControllers respondsToSelector:@selector(navigationController:willHideViewController:)])
		[_delegateForLateralControllers navigationController:self willHideViewController:showingController];
	
	if (_flags.showingLeftController) {
		[_popover dismissPopoverAnimated:animated];
	}
	if (_flags.showingRightController && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
		[self iPadRightViewHide:(UIButton *)self.rightBarButton.customView];
	}
	
	_flags.showingRightController = NO;
	_flags.showingLeftController = NO;
	
	CGRect frame = self.view.frame;
	frame.origin.x = 0;
	if (animated) {
		[UIView animateWithDuration:kMenuSlideDuration delay:0.0f
							options:UIViewAnimationOptionBeginFromCurrentState
						 animations:^{
							 [_this.view setFrame:frame];
						 } completion:^(BOOL finished) {
							 [_this showShadow:NO];
							 [_this.visibleViewController.view setUserInteractionEnabled:YES];
							 if (showingController.view.superview != nil) {
								 [showingController.view removeFromSuperview];
							 }
							 if ([_delegateForLateralControllers respondsToSelector:@selector(navigationController:didHideViewController:)]) {
								 [_delegateForLateralControllers navigationController:_this didHideViewController:showingController];
							 }
							 
							 if (completion != nil) {
								 completion();
							 }
						 }];
		
//		[UIView animateWithDuration:kMenuSlideDuration animations:^ {
//			 [_this.view setFrame:frame];
//		 } completion:^(BOOL finished) {
//			 [_this showShadow:NO];
//			 [_this.visibleViewController.view setUserInteractionEnabled:YES];
//			 if (showingController.view.superview != nil) {
//				 [showingController.view removeFromSuperview];
//             }
//			 if ([_delegateForLateralControllers respondsToSelector:@selector(navigationController:didHideViewController:)]) {
//				 [_delegateForLateralControllers navigationController:_this didHideViewController:showingController];
//             }
//		 }];
    } else {
		[_this showShadow:NO];
		[_this.view setFrame:frame];
		[_this.visibleViewController.view setUserInteractionEnabled:YES];
		if (showingController.view.superview != nil) {
			[showingController.view removeFromSuperview];
        }
		if ([_delegateForLateralControllers respondsToSelector:@selector(navigationController:didHideViewController:)]) {
			[_delegateForLateralControllers navigationController:_this didHideViewController:showingController];
        }
		
		if (completion != nil) {
			completion();
		}
	}
}

- (void)showShadow:(BOOL)show
{
	UIView *shadowView = nil;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
		shadowView = self.view;
	} else {
		shadowView = _rightController.view;
	}
	shadowView.layer.masksToBounds = NO;
    shadowView.layer.shadowOpacity = show ? 0.8f : 0.0f;
	
    if (show)  {
		shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
        shadowView.layer.cornerRadius = 4.0f;
        shadowView.layer.shadowOffset = CGSizeZero;
        shadowView.layer.shadowRadius = 4.0f;
        shadowView.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.view.bounds].CGPath;
    }
}

@end
























