//
//  ITCommonAnimations.m
//  SocialVoice
//
//  Created by Anderson Lucas C. Ramos on 11/4/12.
//  Copyright (c) 2012 Anderson Lucas C. Ramos. All rights reserved.
//

#import "ITCommonFx.h"
#import "ITUtilities.h"
#import "ITTimer.h"
@import QuartzCore;

@implementation ITCommonFx

+ (void)buttonTouchAnimation:(UIButton *)anim completion:(ITAnimationCompleted)block {
	[anim setEnabled:NO];
	CGRect frame = anim.frame;
	[UIView animateWithDuration:0.15f delay:0.0f
						options:UIViewAnimationOptionCurveEaseInOut
					 animations:^{
						 [anim setFrame:[ITUtilities rectForCenter:anim.center
														   andSize:(CGSize) {
															   frame.size.width * 1.05f,
															   frame.size.height * 1.05f
														   }]];
					 } completion:^(BOOL finished) {
						 [UIView animateWithDuration:0.15f delay:0.0f
											 options:UIViewAnimationOptionCurveEaseInOut
										  animations:^{
											  [anim setFrame:frame];
										  } completion:^(BOOL finished) {
											  [anim setEnabled:YES];
											  if (block != nil) {
												  block();
											  }
										  }];
					 }];
}

+ (void)buttonHoverAnimation:(UIButton *)anim withHoverColor:(UIColor *)hover completion:(ITAnimationCompleted)block {
	[anim setEnabled:NO];
	UIColor *lastColor = anim.backgroundColor;
	[anim setBackgroundColor:hover];
	[[[[ITTimer alloc] initWithTimeInterval:0.20f userInfo:nil repeats:NO]
	  setBlockTarget:^(ITTimer *timer) {
		  [anim setEnabled:YES];
		  [anim setBackgroundColor:lastColor];
		  if (block != nil) {
			  block();
		  }
	  }] start];
}

+ (void)addCircleMaskToView:(UIView *)maskView {
	CALayer *layer = [[CALayer alloc] init];
	UIImage *mask = [ITUtilities imageNamed:@"mask1" extension:@"png"];
	layer.contents = (id)mask.CGImage;
	layer.frame = (CGRect) {
		CGPointZero,
		maskView.frame.size
	};
	[maskView.layer setMask:layer];
}

@end
