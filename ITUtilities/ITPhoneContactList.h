//
//  SMPhoneContactList.h
//  Shot
//
//  Created by Anderson Lucas C. Ramos on 1/9/12.
//  Copyright (c) 2012 Tangível Tecnologia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ITPhoneContactList;

typedef void (^PhoneContactListBlock)(ITPhoneContactList *list);

@interface ITPhoneContact : NSObject

@property (nonatomic, readonly) NSString        *firstName;
@property (nonatomic, readonly) NSString        *lastName;

- (id)init;
- (NSArray *)emailList;
- (NSArray *)phoneList;

@end

enum ITPhoneContactListCatchType {
    ITPhoneContactListCatchTypeNeedsEmail,
    ITPhoneContactListCatchTypeNeedsPhoneNumber,
    ITPhoneContactListCatchTypeAll
};
typedef enum ITPhoneContactListCatchType ITPhoneContactListCatchType;

@interface ITPhoneContactList : NSObject

@property (nonatomic, assign) ITPhoneContactListCatchType   catchType;

- (id)init;
- (void)loadContactsWithBlock:(PhoneContactListBlock)block;
- (NSArray *)contactList;

@end
