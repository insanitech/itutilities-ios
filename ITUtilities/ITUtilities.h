//
//  Util.h
//  Util
//
//  Created by Anderson Lucas C. Ramos on 9/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ITTimer.h"
#import "ITDateFormatter.h"
#import "ITQueue.h"
#import "ITThread.h"
#import "ITBase64.h"
#import "ITLocalization.h"
#import "ITPhoneContactList.h"
#import "ITTypes.h"
#import "ITURLLoader.h"
#import "ITSQLiteDatabase.h"

#import "ITUIClasses.h"

@interface ITUtilities : NSObject

+ (float)currentRotationFromView:(UIView *)v;
+ (NSString *)nibNameForDevice:(NSString *)name;
+ (void)loadNibName:(NSString *)name forViewController:(UIViewController *)controller;
+ (void)loadAbsoluteNibName:(NSString *)name forViewController:(UIViewController *)controller;
+ (CGSize)deviceSizeToInterface:(UIInterfaceOrientation)orientation;
+ (CGSize)deviceSize;
+ (BOOL)is_iPad;
+ (BOOL)is_iPhone5_Resolution;
+ (BOOL)is_iPhone4_Resolution;
+ (void)scrollViewScrollToBottom:(UIScrollView *)scrollview;
+ (UIImage *)imageNamed:(NSString *)name extension:(NSString *)ext;
+ (UIImage *)imageWithContentsOfFile:(NSString *)fileName;
+ (UIImage *)imageWithData:(NSData *)data;
+ (UIImage *)imageFromColor:(UIColor *)color imageSize:(CGSize)size;
+ (UIImage *)squareImageFromImage:(UIImage *)outputImage;
+ (UIImage *)squareResizedImageFromImage:(UIImage *)outputImage newSize:(float)sSize;
+ (UIImage *)screenshotFromView:(UIView *)view;
+ (UIImage *)boxblurFromImage:(UIImage *)source withBlur:(CGFloat)blur;
+ (UIColor *)colorWithRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(NSInteger)alpha;
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert;
+ (NSString *)hexStringForColor:(UIColor *)color;
+ (CGRect)rectForCenter:(CGPoint)center andSize:(CGSize)size;
+ (CGPoint)centerForRect:(CGRect)rect;
//#elif TARGET_OS_MAC
//+ (void)loadNibName:(NSString *)name forWindowController:(NSWindowController *)owner;
//+ (void)setBackgroundColor:(NSColor *)color forView:(NSView *)view;
//+ (NSRect)rectForCenter:(NSPoint)center andSize:(NSSize)size;
//+ (NSPoint)centerForRect:(NSRect)rect;
//+ (NSImage *)squareResizedImageFromImage:(NSImage *)outputImage newSize:(float)sSize;
//#endif

+ (BOOL)isRetinaDisplay;
+ (NSString *)platform;
+ (NSURL *)documentsDirectoryURL;
+ (NSString *)documentsDirectoryString;
+ (NSURL *)libraryDirectoryURL;
+ (NSString *)libraryDirectoryString;
+ (NSURL *)cachesDirectoryURL;
+ (NSString *)cachesDirectoryString;
+ (void)deallocLog:(Class)c;

@end
