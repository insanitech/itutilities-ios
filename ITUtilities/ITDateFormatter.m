//
//  TTDateFormatter.m
//  Shot
//
//  Created by Anderson Lucas C. Ramos on 12/21/11.
//  Copyright (c) 2011 Tangível Tecnologia. All rights reserved.
//

#import "ITDateFormatter.h"

@implementation ITDateFormatter

+ (NSString *)weekPriorityDayForDate:(NSDate *)date
{
    NSDate *current = [NSDate date];
    
    // timestamp for message and now
    NSTimeInterval currentTimeStamp = [current timeIntervalSince1970];
    NSTimeInterval messageTimeStamp = [date timeIntervalSince1970];
    
    // one day value, sevendays value
    NSTimeInterval day = 60 * 60 * 24;
    NSTimeInterval sevenDays = day * 7;
    
    // getting today middle night date
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *todayComponents = [gregorian components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:[NSDate date]];
    NSInteger theDay = [todayComponents day];
    NSInteger theMonth = [todayComponents month];
    NSInteger theYear = [todayComponents year];
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:theDay]; 
    [components setMonth:theMonth]; 
    [components setYear:theYear];
    
    NSDate *todayDate = [gregorian dateFromComponents:components];
    /////////
    
    NSTimeInterval diference = currentTimeStamp - messageTimeStamp;
    NSTimeInterval todayBegining = [todayDate timeIntervalSince1970];
    NSTimeInterval elapsedTimeUntilToday = todayBegining - messageTimeStamp;
    
    components = [[NSCalendar currentCalendar] components:
                  NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear
                                                 fromDate:[NSDate date]];
    
    NSInteger currentDay = [components day];
    components = [[NSCalendar currentCalendar] components:
                  NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitWeekday
                                                 fromDate:date];
    NSInteger dateDay = [components day];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSString *dateString = @"";
    if (diference < day && currentDay == dateDay)
    {
        dateString = @"Today";
    }
    else if (elapsedTimeUntilToday < day)
    {
        dateString = @"Yesterday";
    }
    else if (diference < sevenDays)
    {
        NSArray *weekDays = [[NSArray alloc] initWithObjects:@"Sunday", 
							 @"Monday", 
							 @"Tuesday", 
							 @"Wednesday", 
							 @"Thursday", 
							 @"Friday", 
							 @"Saturday", nil];
        
        NSInteger weekDay = [components weekday] - 1;
        dateString = [weekDays objectAtIndex:weekDay];
    }
    else
    {
        [formatter setDateFormat:@"dd/MM/YYYY"];
        dateString = [formatter stringFromDate:date];
    }
    
    return dateString;
}

+ (NSString *)weekDayForDate:(NSDate *)date
{
    NSArray *weekDays = [[NSArray alloc] initWithObjects:@"Sunday", 
						 @"Monday", 
						 @"Tuesday", 
						 @"Wednesday", 
						 @"Thursday", 
						 @"Friday", 
						 @"Saturday", nil];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:
                  				NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitWeekday
                                                                   fromDate:date];
    
    NSInteger weekDay = [components weekday] - 1;
    return [weekDays objectAtIndex:weekDay];
}

+ (NSString *)localizedDateStringForDate:(NSDate *)date
{
    NSArray *months = [[NSArray alloc] initWithObjects:@"January", @"February", @"March", 
                                                       @"April", @"May", @"June", @"July", 
                                                       @"August", @"September", @"October", 
                                                       @"November", @"December", nil];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:
                                    		NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear
                                                                   fromDate:date];
    NSInteger day = [components day];
    NSString *month = [months objectAtIndex:[components month] - 1];
    NSInteger year = [components year];
    return [NSString stringWithFormat:@"%@ %i, %i", month, (int)day, (int)year];
}

+ (NSString *)shortDateForDate:(NSDate *)date
{
    NSArray *months = [[NSArray alloc] initWithObjects:@"Jan", @"Feb", @"Mar", 
                       @"Apr", @"May", @"Jun", @"Jul", 
                       @"Aug", @"Sep", @"Oct", 
                       @"Nov", @"Dec", nil];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:
                                    NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear
                                                                   fromDate:date];
    NSInteger day = [components day];
    NSString *month = [months objectAtIndex:[components month] - 1];
    NSInteger year = [components year];
    return [NSString stringWithFormat:@"%@ %i, %i", month, (int)day, (int)year];
}

+ (NSString *)hoursForDate:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    return [formatter stringFromDate:date];
}

+ (NSString *)dateStringForDate:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss dd/MM/YYYY"];
    return [formatter stringFromDate:date];
}

+ (NSDictionary *)howLateForDate:(NSDate *)date
{
	NSTimeZone *tz = [NSTimeZone localTimeZone];
	NSInteger seconds = [tz secondsFromGMT];
	seconds = [[NSDate date] timeIntervalSince1970] + seconds;
	NSDate *current = [NSDate dateWithTimeIntervalSince1970:seconds];
	
//	NSDateComponents *currentComps = [[NSCalendar currentCalendar] components:
//									   NSSecondCalendarUnit | NSMinuteCalendarUnit | NSHourCalendarUnit | NSDayCalendarUnit |
//									   NSWeekCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit
//																	 fromDate:current];
//	NSDateComponents *messageComps = [[NSCalendar currentCalendar] components:
//									  NSSecondCalendarUnit | NSMinuteCalendarUnit | NSHourCalendarUnit | NSDayCalendarUnit |
//									  NSWeekCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit
//																	 fromDate:date];
//	
	NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
//	if (messageComps.year == currentComps.year) {
//		if (messageComps.month == currentComps.month) {
//			if (messageComps.week == currentComps.week) {
//				if (messageComps.day == currentComps.day) {
//					if (messageComps.hour == currentComps.hour) {
//						if (messageComps.minute == currentComps.minute) {
//							[dic setObject:@(currentComps.second - messageComps.second) forKey:@"time"];
//							[dic setObject:@"seconds ago" forKey:@"caption"];
//						} else {
//							[dic setObject:@(currentComps.minute - messageComps.minute) forKey:@"time"];
//							[dic setObject:@"minutes ago" forKey:@"caption"];
//						}
//					} else {
//						[dic setObject:@(currentComps.hour - messageComps.hour) forKey:@"time"];
//						[dic setObject:@"hours ago" forKey:@"caption"];
//					}
//				} else {
//					[dic setObject:@(currentComps.day - messageComps.day) forKey:@"time"];
//					[dic setObject:@"days ago" forKey:@"caption"];
//				}
//			} else {
//				[dic setObject:@(currentComps.week - messageComps.week) forKey:@"time"];
//				[dic setObject:@"weeks ago" forKey:@"caption"];
//			}
//		} else {
//			[dic setObject:@(currentComps.month - messageComps.month) forKey:@"time"];
//			[dic setObject:@"months ago" forKey:@"caption"];
//		}
//	} else {
//		[dic setObject:@(currentComps.year - messageComps.year) forKey:@"time"];
//		[dic setObject:@"years ago" forKey:@"caption"];
//	}
	
	float minute = 60;
	float hour = minute * 60;
	float day = hour * 24;
	float week = day * 7;
	float month = day * 30;
	float yeah = day * 365;
	
	float time = [current timeIntervalSince1970] - [date timeIntervalSince1970];
	
	if (time > yeah) {
		int value = (int)(time / yeah);
		[dic setObject:@(value) forKey:@"time"];
		[dic setObject:value == 1 ? @"year ago" : @"years ago" forKey:@"caption"];
	} else if (time > month) {
		int value = (int)(time / month);
		[dic setObject:@(value) forKey:@"time"];
		[dic setObject:value == 1 ? @"month ago" : @"months ago" forKey:@"caption"];
	} else if (time > week) {
		int value = (int)(time / week);
		[dic setObject:@(value) forKey:@"time"];
		[dic setObject:value == 1 ? @"week ago" : @"weeks ago" forKey:@"caption"];
	} else if (time > day) {
		int value = (int)(time / day);
		[dic setObject:@(value) forKey:@"time"];
		[dic setObject:value == 1 ? @"day ago" : @"days ago" forKey:@"caption"];
	} else if (time > hour) {
		int value = (int)(time / hour);
		[dic setObject:@(value) forKey:@"time"];
		[dic setObject:value == 1 ? @"hour ago" : @"hours ago" forKey:@"caption"];
	} else if (time > minute) {
		int value = (int)(time / minute);
		[dic setObject:@(value) forKey:@"time"];
		[dic setObject:value == 1 ? @"minute ago" : @"minutes ago" forKey:@"caption"];
	} else {
		int value = (int)time;
		[dic setObject:@(value) forKey:@"time"];
		[dic setObject:value == 1 ? @"second ago" : @"seconds ago" forKey:@"caption"];
	}
	
	return dic;
}

@end


















