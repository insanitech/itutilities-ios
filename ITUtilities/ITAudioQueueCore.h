//
//  AudioQueueCore.h
//  Shot
//
//  Created by Anderson Lucas C. Ramos on 9/1/11.
//  Copyright 2011 Tangivel Tecnologia. All rights reserved.
//

#ifndef Shot_AudioQueueCore_h
#define Shot_AudioQueueCore_h

#include <Foundation/Foundation.h>
#include <AudioToolbox/AudioToolbox.h>

#define kNumberBuffers                  4

#define AUDIO_FORMAT                    kAudioFormatLinearPCM;
#define AUDIO_SAMPLE_RATE               8000.0
#define AUDIO_CHANNELS_PER_FRAME        1
#define AUDIO_BITS_PER_CHANNEL          16
#define AUDIO_BYTES_PER_FRAME           (AUDIO_CHANNELS_PER_FRAME * sizeof(SInt16))
#define AUDIO_BYTES_PER_PACKET          AUDIO_BYTES_PER_FRAME
#define AUDIO_FRAMES_PER_PACKET         1
#define AUDIO_FLAG_CANONICAL            (kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked)
#define AUDIO_BUFFER_BYTE_SIZE          960

typedef struct 
{
    AudioStreamBasicDescription  mDataFormat;
    AudioQueueRef                mQueue;
    AudioQueueBufferRef          mBuffers[kNumberBuffers];
    AudioFileID                  mAudioFile;
    UInt32                       bufferByteSize;
    SInt64                       mCurrentPacket;
    BOOL                         mIsRunning;
} AQRecorderState;

typedef struct  
{
    AudioStreamBasicDescription    mDataFormat;
    AudioQueueRef                  mQueue;
    AudioQueueBufferRef            mBuffers[kNumberBuffers];
    UInt32                         bufferByteSize;
    UInt32							mNumFreeBuffers;
    BOOL                           mIsRunning;
} AQPlayerState;

#endif
