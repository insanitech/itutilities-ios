//
//  MaritalPickerView.h
//  AntSocial
//
//  Created by Anderson Lucas C. Ramos on 5/21/12.
//  Copyright (c) 2012 Tangível Tecnologia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ITDatePickerView;

typedef void (^ITPickerDidFinishPicking)(ITDatePickerView *picker, NSDate *pickedDate);
typedef void (^ITPickerAccessoryButtonPressed)(ITDatePickerView *picker);

@interface ITDatePickerView : UIView

@property (strong, nonatomic, readonly) UIDatePicker *picker;
@property (strong, nonatomic, readonly) UIToolbar *accessoryToolbar;;
@property (strong, nonatomic, readonly) NSDate *pickedDate;
@property (assign, nonatomic, readonly) BOOL isShowing;

- (id)init;
- (void)showPicker;
- (void)hidePicker;
- (void)setBackButtonPressed:(ITPickerAccessoryButtonPressed)block;
- (void)setNextButtonPressed:(ITPickerAccessoryButtonPressed)block;
- (void)setDidFinishPicking:(ITPickerDidFinishPicking)result;

@end
