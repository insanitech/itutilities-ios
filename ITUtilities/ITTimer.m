//
//  ITTimer.m
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 4/2/12.
//  Copyright (c) 2012 Tangível Tecnologia. All rights reserved.
//

#import "ITTimer.h"

@interface ITTimer ()

@property (nonatomic, strong) BlockTarget target;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) BOOL isValid;

- (void)timerWorker:(NSTimer *)timer;

@end

@implementation ITTimer

@synthesize
	target = _target,
	timer = _timer,
	userInfo = _userInfo,
	isValid = _isValid;

- (void)dealloc {
	self.target = nil;
	self.timer = nil;
}

- (instancetype)initWithTimeInterval:(NSTimeInterval)ti userInfo:(id)userInfo repeats:(BOOL)yesOrNo {
	if (self = [super init]) {
		self.timer = [NSTimer timerWithTimeInterval:ti target:self selector:@selector(timerWorker:) userInfo:userInfo repeats:yesOrNo];
	}
	return self;
}

- (instancetype)setBlockTarget:(BlockTarget)target {
	self.target = target;
	return self;
}

- (id)userInfo {
	return self.timer.userInfo;
}

- (void)start {
	[[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
}

- (void)stop {
	[self.timer invalidate];
}

- (BOOL)isValid {
	return self.timer.isValid;
}

- (void)timerWorker:(NSTimer *)timer {
	self.target(self);
}

@end