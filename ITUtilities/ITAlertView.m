//
//  ITAlertView.m
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 5/16/12.
//  Copyright (c) 2012 Tangível Tecnologia. All rights reserved.
//

#import "ITAlertView.h"

@interface ITAlertView ()
	<UIAlertViewDelegate>

@property (nonatomic, strong) UITextField *plainTextInput;
@property (nonatomic, strong) UITextField *secureTextInput;
@property (nonatomic, strong) ITAlertViewButtonBlock block;

@end

@implementation ITAlertView

@synthesize plainTextInput = _plainTextInput,
			secureTextInput = _secureTextInput,
			block = _block;

- (void)dealloc
{
	self.block = nil;
	self.plainTextInput = nil;
	self.secureTextInput = nil;
}

- (id)initWithTitle:(NSString *)title 
			message:(NSString *)message 
		   delegate:(id)delegate 
  cancelButtonTitle:(NSString *)cancelButtonTitle 
  otherButtonTitles:(NSArray *)otherButtonTitles 
	 alertViewStyle:(UIAlertViewStyle)style
{
    self = [super initWithTitle:title 
						message:message 
					   delegate:delegate 
			  cancelButtonTitle:cancelButtonTitle 
			  otherButtonTitles:[otherButtonTitles objectAtIndex:0], nil];
	for (NSUInteger i = 1; i < otherButtonTitles.count; i++) {
		[self addButtonWithTitle:[otherButtonTitles objectAtIndex:i]];
    }
    if (self)
	{
		if ([[[UIDevice currentDevice] systemVersion] intValue] < 5)
		{
			if (style == UIAlertViewStylePlainTextInput)
			{
				self.plainTextInput = [[UITextField alloc] initWithFrame:(CGRect) { {10, 40}, {265, 31} }];
				[self.plainTextInput setBorderStyle:UITextBorderStyleRoundedRect];
				[self.plainTextInput setTextColor:[UIColor blackColor]];
				[self.plainTextInput setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
				[self addSubview:self.plainTextInput];
			}
			else if (style == UIAlertViewStyleSecureTextInput)
			{
				self.secureTextInput = [[UITextField alloc] initWithFrame:(CGRect) { {10, 40}, {265, 31} }];
				[self.secureTextInput setBorderStyle:UITextBorderStyleRoundedRect];
				[self.secureTextInput setTextColor:[UIColor blackColor]];
				[self.secureTextInput setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
				[self.secureTextInput setSecureTextEntry:YES];
				[self addSubview:self.secureTextInput];
			}
		} else {
			[self setAlertViewStyle:style];
        }
    }
    return self;
}

- (id)initWithTitle:(NSString *)title 
			message:(NSString *)message 
  cancelButtonTitle:(NSString *)cancelButtonTitle 
  otherButtonTitles:(NSArray *)otherButtonTitles 
	 alertViewStyle:(UIAlertViewStyle)style 
 buttonClickedBlock:(ITAlertViewButtonBlock)blk
{
	self = [self initWithTitle:title 
					   message:message 
					  delegate:self 
			 cancelButtonTitle:cancelButtonTitle 
			 otherButtonTitles:otherButtonTitles 
				alertViewStyle:style];
	if (self != nil)
	{
		self.block = blk;
	}
	return self;
}

- (UITextField *)plainTextInput
{
	if (_plainTextInput == nil) {
		for (UITextField *field in self.subviews)
		{
			if ([field isKindOfClass:[UITextField class]] &&
				![(UITextField *)field isSecureTextEntry])
			{
				[self setPlainTextInput:field];
				break;
			}
		}
    }
    if (_plainTextInput == nil) {
        return [self textFieldAtIndex:0];
    }
	return _plainTextInput;
}

- (UITextField *)secureTextInput
{
	if (_secureTextInput == nil) {
		for (UITextField *field in self.subviews)
		{
			if ([field isKindOfClass:[UITextField class]] &&
				[(UITextField *)field isSecureTextEntry])
			{
                [self setSecureTextInput:field];
				break;
			}
		}
    }
    if (_secureTextInput == nil) {
        @try {
            return [self textFieldAtIndex:1];
        }
        @catch (NSException *exception) {
            return [self textFieldAtIndex:0];
        }
    }
	return _secureTextInput;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (self.block != nil) {
		self.block((ITAlertView *)alertView, buttonIndex);
	}
	self.block = nil;
}

@end
