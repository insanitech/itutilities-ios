//
//  ITOpenGLESFrameExtractor.h
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 22/03/13.
//
//

#import <CoreGraphics/CoreGraphics.h>
#import <OpenGLES/EAGL.h>
#import <CoreVideo/CoreVideo.h>

@class ITVideoRecorder;

@interface ITOpenGLESFrameExtractor : NSObject

- (id)initWithBufferSize:(CGSize)size recorder:(ITVideoRecorder *)recorder;
- (CVPixelBufferRef)extractFrame:(uint)framebuffer;

@end
