//
//  Speex.h
//  Shot
//
//  Created by Anderson Lucas C. Ramos on 9/1/11.
//  Copyright 2011 Tangivel Tecnologia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <SpeexLib/ITSpeexEncDec.h>

@protocol ITAudioStreamRecorderDelegate <NSObject>

@required
- (void)recorderBufferEnqueued:(SpeexEncodedPackets *)encData;
- (void)recorderBufferEnqueued:(void *)audioData size:(int)size;

@end

@class ITAudioStreamPlayer;
@class ITSpeexEncoder;

@interface ITAudioStreamRecorder : NSObject
{
    id<ITAudioStreamRecorderDelegate> _delegate;
	CFTypeRef _recordState;
    ITSpeexEncoder *_encoder;
    BOOL _encodeSpeex;
    BOOL _initialized;
    BOOL _giveOutway;
}

@property (nonatomic, assign) BOOL encodeSpeex;

- (BOOL)startRecording;
- (void)stopRecording;
- (BOOL)isRecording;
- (void)setStreamDelegate:(id<ITAudioStreamRecorderDelegate>)del;
- (UInt32)bufferByteSize;
- (void)setGiveOutway:(BOOL)ow;

@end