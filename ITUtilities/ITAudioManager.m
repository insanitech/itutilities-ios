//
//  ITRecordManager.m
//  SocialVoice
//
//  Created by Anderson Lucas C. Ramos on 11/4/12.
//  Copyright (c) 2012 Anderson Lucas C. Ramos. All rights reserved.
//

#import "ITAudioManager.h"
#import <ITUtilities/ITAudioStreamRecorder.h>
#import <AVFoundation/AVFoundation.h>
#import <SpeexLib/ITSpeexEncDec.h>
#import <ITUtilities/ITTimer.h>

@interface ITAudioManager ()
	<ITAudioStreamRecorderDelegate, AVAudioPlayerDelegate>

@property (nonatomic, strong) ITAudioStreamRecorder *recorder;
@property (nonatomic, strong) NSMutableData *recordedData;
@property (nonatomic, strong) AVAudioPlayer *player;
@property (nonatomic, strong) ITSpeexDecoder *decoder;
@property (nonatomic, strong) ITSpeexEncoder *encoder;
@property (nonatomic, strong) ITAudioRecordCompletion recordCompletion;
@property (nonatomic, strong) ITAudioPlayCompletion playCompletion;
@property (nonatomic, strong) ITTimer *recorderTimer;

@end

@implementation ITAudioManager {
	ITAudioStreamRecorder *_recorder;
	NSMutableData *_recordedData;
	AVAudioPlayer *_player;
	ITSpeexDecoder *_decoder;
	ITSpeexEncoder *_encoder;
	ITAudioRecordCompletion _recordCompletion;
	ITAudioPlayCompletion _playCompletion;
	ITTimer *_recorderTimer;
}

@synthesize player = _player;
@synthesize recorder = _recorder;
@synthesize recordedData = _recordedData;
@synthesize decoder = _decoder;
@synthesize encoder = _encoder;
@synthesize recordCompletion = _recordCompletion;
@synthesize playCompletion = _playCompletion;
@synthesize recorderTimer = _recorderTimer;
@synthesize maxRecordTime;

- (void)dealloc {
	self.player = nil;
	self.recorder = nil;
	self.recordedData = nil;
	self.decoder = nil;
	self.encoder = nil;
	self.recordCompletion = nil;
	self.playCompletion = nil;
	[self.recorderTimer invalidate];
	self.recorderTimer = nil;
	
	NSLog(@"%@ dealloc", NSStringFromClass([self class]));
}

- (id)init
{
    self = [super init];
    if (self != nil) {
        self.decoder = [[ITSpeexDecoder alloc] initWithBandType:SpeexBandType_Narrow];
		self.encoder = [[ITSpeexEncoder alloc] initWithBandType:SpeexBandType_Narrow sampleRate:8000];
		self.maxRecordTime = 15;
    }
    return self;
}

- (void)startRecoringWithCompletion:(ITAudioRecordCompletion)completion {
	self.recordCompletion = completion;
	if (!self.recorder.isRecording) {
		self.recordedData = [[NSMutableData alloc] init];
		self.recorder = [[ITAudioStreamRecorder alloc] init];
		[self.recorder setGiveOutway:YES];
		[self.recorder setEncodeSpeex:NO];
		[self.recorder setStreamDelegate:self];
		[self.recorder startRecording];
		self.recorderTimer = [ITTimer timerWithTimeInterval:self.maxRecordTime block:^(ITTimer *timer) {
			[self finishRecording];
		} userInfo:nil repeats:NO];
	}
}

- (void)finishRecording {
	if (self.recorder.isRecording) {
		[self.recorderTimer invalidate];
		self.recorderTimer = nil;
		[self.recorder stopRecording];
		self.recorder = nil;
		if (self.recordCompletion != nil) {
			self.recordCompletion(self.recordedData);
			self.recordedData = nil;
		}
	}
}

- (BOOL)isRecording {
	return self.recorder.isRecording;
}

- (void)playEncodedAudioData:(NSData *)audioData completion:(ITAudioPlayCompletion)completion {
	self.playCompletion = completion;
	if (self.player == nil) {
		short *waveData = NULL;
		int audioDataLength = 0;
		[self.decoder decodeData:(void *)audioData.bytes totalLength:audioData.length
						 outData:&waveData outLength:&audioDataLength];
		NSData *playableData = [NSData dataWithBytesNoCopy:[ITWAVEUtil waveFileFormatted:waveData size:audioDataLength :1 :8000 :16]
													length:audioDataLength + 44];
		NSError *error = nil;
		self.player = [[AVAudioPlayer alloc] initWithData:playableData error:&error];
		[self.player setDelegate:self];
		[self.player play];
		if (error != nil) {
			NSLog(@"%@", error);
		}
		free(waveData);
	} else {
		[self stopPlayingAudioData];
		[self playEncodedAudioData:audioData completion:completion];
	}
}

- (void)stopPlayingAudioData {
	if (self.player != nil) {
		[self.player stop];
		self.player = nil;
		if (self.playCompletion != nil) {
			self.playCompletion(NO);
		}
	}
}

- (float)currentPlaying {
	if (self.player == nil) {
		return 0;
	}
	return self.player.currentTime / self.player.duration;
}

- (void)recorderBufferEnqueued:(SpeexEncodedPackets *)encData { }

- (void)recorderBufferEnqueued:(void *)audioData size:(int)size {
	SpeexEncodedData *encData = [self.encoder encodeData:audioData totalLength:size];
	[self.recordedData appendBytes:encData->encodedData length:encData->byteAmount];
	FreeSpeexEncData(encData);
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
	self.player = nil;
	if (self.playCompletion != nil) {
		self.playCompletion(flag);
	}
}

@end

























