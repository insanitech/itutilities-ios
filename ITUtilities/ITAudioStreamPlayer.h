//
//  AudioPlayer.h
//  Shot
//
//  Created by Anderson Lucas C. Ramos on 9/1/11.
//  Copyright 2011 Tangivel Tecnologia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ITUtilities/ITTypes.h>

#pragma mark TTAudioStreamPlayer Delegate

@protocol ITAudioStreamPlayerDelegate <NSObject>

@optional
- (void)didFinishPlayingAudioData;

@end

#pragma mark -

#pragma mark TTAudioChunk Class

@interface ITAudioChunk : NSObject 
{
@public
    short   *audioData;
    int     audioSize;
}

- (id)initWithData:(short *)data size:(int)size;

@end

#pragma mark -

#pragma mark Class Imports

@class ITAudioStreamRecorder;
@class ITSpeexDecoder;
@class ITQueue;

#pragma mark -

#pragma mark TTAudioStreamPlayer Class

@interface ITAudioStreamPlayer : NSObject
{
    CFTypeRef                   		_playerState;
    ITQueue                           	*_mArray;
    ITSpeexDecoder                    	*_decoder;
    BOOL                            	_initialized;
    id<ITAudioStreamPlayerDelegate>   	_delegate;
}

- (void)playStream;
- (void)stopStream;
- (BOOL)isPlaying;
- (void)clearBuffers;
- (BOOL)hasPacketsOnQueue;
- (void)setDelegate:(id<ITAudioStreamPlayerDelegate>)del;
- (ITAudioChunk *)receivedBuffer:(byte *)data length:(int)len speexEncoded:(BOOL)enc;
- (void)setAudioOutputSpeaker:(BOOL)spk;

@end

#pragma mark -



















