//
//  TTSegmentedControl.h
//  Shot
//
//  Created by Anderson Lucas C. Ramos on 12/29/11.
//  Copyright (c) 2011 Tangível Tecnologia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ITSegmentedControlItem : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIImage *selectedImage;
@property (nonatomic, strong) UIImage *unselectedImage;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIFont *selectedFont;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIColor *selectedTextColor;
@property (nonatomic, strong) UIColor *shadowColor;
@property (nonatomic, assign) CGSize shadowOffset;
@property (nonatomic, assign) CGRect contentStretch;

+ (ITSegmentedControlItem *)itemWithCustomSelectedView:(UIView *)selectedView
                                        unselectedView:(UIView *)unselectedView;
+ (ITSegmentedControlItem *)itemWithTitle:(NSString *)title
                            selectedImage:(UIImage *)selectedImage
                          unselectedImage:(UIImage *)unselectedImage;
- (id)init;

@end

///// * Separator * /////

@interface ITSegmentedControl : UIView

@property (nonatomic, assign) NSInteger selectedSegmentIndex;
@property (nonatomic, strong, readonly) NSArray *items;

- (id)initWithCoder:(NSCoder *)aDecoder;
- (id)initWithItems:(NSArray *)items frame:(CGRect)frame;
- (void)setFrame:(CGRect)frame;
- (void)setValueDidChangeTarget:(id)target action:(SEL)selector;
- (void)addItem:(ITSegmentedControlItem *)item;
- (void)removeItemAtIndex:(ITSegmentedControlItem *)item;

@end
