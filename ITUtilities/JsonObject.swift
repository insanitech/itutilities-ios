//
//  ITBasicJsonObject.swift
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 16/12/15.
//
//

import Foundation;

open class JsonObject: NSObject {
	fileprivate var intern: [String: AnyObject]! = [String: AnyObject]();
	
	public override init() {
		super.init()
	}
	
	public init(other: JsonObject) {
		self.intern = other.intern;
	}
	
	public required init(object: [String: AnyObject]) {
		self.intern = object;
	}
	
	deinit {
		self.intern = nil;
	}
	
	open func jsonGetValue<T>(_ key: String) -> T? {
		let t: T? = self.intern[key] as? T;
		if (t != nil) {
			return t;
		}
		return "" as? T;
	}
	
	open func jsonSetValue<T>(_ key: String, value: T) {
		self.intern[key] = value as AnyObject;
	}
	
	open func jsonObject() -> [String: AnyObject] {
		return self.intern;
	}
	
	internal func setJsonObject(_ object: [String: AnyObject]!) {
		self.intern = object;
	}
}
