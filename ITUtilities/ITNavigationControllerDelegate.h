//
//  ITNavigationControllerDelegate.h
//  ITNavigationController
//
//  Created by Anderson Lucas C. Ramos on 3/30/12.
//  Copyright (c) 2012 42 Tecnologia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ITNavigationController;

@protocol ITNavigationControllerDelegate <NSObject>

@optional
- (void)navigationController:(ITNavigationController *)navigationController willShowViewController:(UIViewController *)viewController;
- (void)navigationController:(ITNavigationController *)navigationController didShowViewController:(UIViewController *)viewController;
- (void)navigationController:(ITNavigationController *)navigationController willHideViewController:(UIViewController *)viewController;
- (void)navigationController:(ITNavigationController *)navigationController didHideViewController:(UIViewController *)viewController;

@end
