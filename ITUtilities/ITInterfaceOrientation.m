//
//  ITInterfaceOrientation.m
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 27/12/12.
//
//

#import "ITInterfaceOrientation.h"
#import "ITUtilities.h"
@import CoreMotion;

@interface ITInterfaceOrientation ()
	<UIAccelerometerDelegate>

@property (nonatomic, strong) ITInterfaceOrientationChanged block;
@property (nonatomic, strong) CMMotionManager *accelerometer;
@property (nonatomic, assign) UIInterfaceOrientation current;

@end

@implementation ITInterfaceOrientation

@synthesize block = _block;
@synthesize accelerometer = _accelerometer;
@synthesize current = _current;

- (void)dealloc {
	[self setBlock:nil];
    [self setAccelerometer:nil];
    
    [ITUtilities deallocLog:[self class]];
}

- (id)init {
	self = [super init];
	if (self != nil) {
		[self setCurrent:UIInterfaceOrientationPortrait];
		[self setAccelerometer:[[CMMotionManager alloc] init]];
	}
	return self;
}

- (void)accelerometerDidAccelerate:(CMAccelerometerData *)accelerationData {
	UIInterfaceOrientation newOrient = self.current;
	if (ABS(accelerationData.acceleration.x) < 0.5f && accelerationData.acceleration.y > 0.5f && ABS(accelerationData.acceleration.z) < 0.5f) {
		newOrient = UIInterfaceOrientationPortraitUpsideDown;
	} else if (accelerationData.acceleration.x > 0.5f && ABS(accelerationData.acceleration.y) < 0.5f && ABS(accelerationData.acceleration.z) < 0.5f) {
		newOrient = UIInterfaceOrientationLandscapeLeft;
	} else if (accelerationData.acceleration.x < -0.5f && ABS(accelerationData.acceleration.y) < 0.5f && ABS(accelerationData.acceleration.z) < 0.5f) {
		newOrient = UIInterfaceOrientationLandscapeRight;
	} else if (ABS(accelerationData.acceleration.x) < 0.5f && accelerationData.acceleration.y < -0.5f && ABS(accelerationData.acceleration.z) < 0.5f) {
		newOrient = UIInterfaceOrientationPortrait;
	}
	
	if (newOrient != self.current) {
		[self setCurrent:newOrient];
		self.block(self);
	}
}

- (void)startDetectionWithBlock:(ITInterfaceOrientationChanged)block {
	[self setBlock:block];
    [self.accelerometer startAccelerometerUpdatesToQueue:[NSOperationQueue mainQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
        if (error == nil) {
            [self accelerometerDidAccelerate:accelerometerData];
        }
    }];
}

- (void)stopDetection {
	[self setBlock:nil];
    [self.accelerometer stopAccelerometerUpdates];
}

@end
