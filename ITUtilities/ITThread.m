//
//  ITThread.m
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 28/01/14.
//
//

#import "ITThread.h"

@interface ITThread ()

@property (strong, nonatomic) Runnable runnable;
@property (strong, nonatomic) RunnableParam runnableParam;

@end

@implementation ITThread

@synthesize runnable = _runnable;
@synthesize runnableParam = _runnableParam;

+ (void)waitForSeconds:(NSTimeInterval)timeInterval {
	[NSThread sleepForTimeInterval:timeInterval];
}

- (void)dealloc {
    [self setRunnable:nil];
    [self setRunnableParam:nil];
}

- (id)initWithRunnable:(Runnable)runnable {
    self = [super initWithTarget:self selector:@selector(threadRun) object:nil];
    if (self != nil) {
        [self setRunnable:runnable];
    }
    return self;
}

- (id)initWithRunnable:(RunnableParam)runnable withParameter:(id)param {
    self = [super initWithTarget:self selector:@selector(threadRun:) object:param];
    if (self != nil) {
        [self setRunnableParam:runnable];
    }
    return self;
}

- (void)threadRun {
    if (self.runnable != nil) {
        self.runnable();
    }
}

- (void)threadRun:(NSObject *)param {
    if (self.runnableParam != nil) {
        self.runnableParam(param);
    }
}

- (void)performOnMainThread:(Runnable)runnable waitUntilDone:(BOOL)wait {
    [self setRunnable:runnable];
    [self performSelectorOnMainThread:@selector(threadRun) withObject:nil waitUntilDone:wait];
}

- (void)performOnMainThread:(RunnableParam)runnable withParameter:(id)param waitUntilDone:(BOOL)wait {
    [self setRunnableParam:runnable];
    [self performSelectorOnMainThread:@selector(threadRun:) withObject:param waitUntilDone:wait];
}

@end
