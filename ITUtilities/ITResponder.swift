//
//  FirstResponder.swift
//  Lupus Index
//
//  Created by Anderson Lucas C. Ramos on 11/03/15.
//  Copyright (c) 2015 DFSoluções. All rights reserved.
//

import Foundation;

struct ITFirstResponder {
	static weak var currentFirstResponder: UIResponder? = nil;
}
extension UIResponder {
	class func findFirstResponder() -> UIResponder? {
		ITFirstResponder.currentFirstResponder = nil;
		UIApplication.shared.sendAction(#selector(UIResponder.findFirstResponder(_:)), to: nil, from: nil, for: nil);
		return ITFirstResponder.currentFirstResponder;
	}
	
	@objc fileprivate func findFirstResponder(_ sender: AnyObject?) {
		ITFirstResponder.currentFirstResponder = self;
	}
}
