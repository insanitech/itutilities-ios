//
//  Util.m
//  Util
//
//  Created by Anderson Lucas C. Ramos on 9/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ITUtilities.h"
@import QuartzCore;
@import Accelerate;
#include <sys/types.h>
#include <sys/sysctl.h>
#include <mach/mach.h>
#include <stdarg.h>

@implementation ITUtilities

#if TARGET_OS_IPHONE
+ (float)currentRotationFromView:(UIView *)v
{
    CGFloat radians = atan2f(v.transform.b, v.transform.a); 
    return radians;
}

+ (NSString *)nibNameForDevice:(NSString *)name
{
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		name = [name stringByAppendingString:@"_iPad"];
	return name;
}

+ (void)loadNibName:(NSString *)name forViewController:(UIViewController *)controller {
	[[NSBundle mainBundle] loadNibNamed:[ITUtilities nibNameForDevice:name] owner:controller options:nil];
}

+ (void)loadAbsoluteNibName:(NSString *)name forViewController:(UIViewController *)controller {
    [[NSBundle mainBundle] loadNibNamed:name owner:controller options:nil];
}

+ (CGSize)deviceSizeToInterface:(UIInterfaceOrientation)orientation
{
	CGSize final;
	{
		final.width = [UIScreen mainScreen].bounds.size.width;
		final.height = [UIScreen mainScreen].bounds.size.height;
		if (UIInterfaceOrientationIsLandscape(orientation)) {
			final.width = [UIScreen mainScreen].bounds.size.height;
			final.height = [UIScreen mainScreen].bounds.size.width;
		}
	}
	return final;
}

+ (CGSize)deviceSize
{
	return [ITUtilities deviceSizeToInterface:[UIApplication sharedApplication].statusBarOrientation];
}

+ (void)scrollViewScrollToBottom:(UIScrollView *)scrollview {
	CGPoint bottomOffset = CGPointMake(0, scrollview.contentSize.height - scrollview.bounds.size.height);
	[scrollview setContentOffset:bottomOffset animated:YES];
}

+ (BOOL)is_iPad {
	return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
}

+ (BOOL)is_iPhone5_Resolution {
	return [UIScreen mainScreen].bounds.size.height == 568;
}

+ (BOOL)is_iPhone4_Resolution {
	CGSize screenSize = [UIScreen mainScreen].bounds.size;
	return (screenSize.width == 320 && screenSize.height == 480) || (screenSize.height == 320 && screenSize.width == 480);
}

+ (UIImage *)imageNamed:(NSString *)name extension:(NSString *)ext {
    NSString *fileName;
    if ([ITUtilities is_iPad]) {
        if ([ITUtilities isRetinaDisplay] && [name rangeOfString:@"_iPad@2x"].location == NSNotFound) {
            fileName = [name stringByAppendingString:@"_iPad@2x"];
        } else if ([name rangeOfString:@"_iPad"].location == NSNotFound) {
            fileName = [name stringByAppendingString:@"_iPad"];
        } else {
            fileName = name;
        }
    } else {
        if ([ITUtilities isRetinaDisplay] && [name rangeOfString:@"@2x"].location == NSNotFound) {
            fileName = [name stringByAppendingString:@"@2x"];
        } else {
            fileName = name;
        }
    }
	NSString *file = [[NSBundle mainBundle] pathForResource:fileName ofType:ext];
	if (file == nil) {
		file = [[NSBundle mainBundle] pathForResource:name ofType:ext];
	}
	return [[UIImage alloc] initWithContentsOfFile:file];
}

+ (UIImage *)imageWithContentsOfFile:(NSString *)fileName {
	return [[UIImage alloc] initWithContentsOfFile:fileName];
}

+ (UIImage *)imageWithData:(NSData *)data {
	return [[UIImage alloc] initWithData:data];
}

+ (UIImage *)imageFromColor:(UIColor *)color imageSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, (CGRect) { { 0, 0 }, size });
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (UIImage *)squareImageFromImage:(UIImage *)outputImage {
	if (outputImage.size.width > outputImage.size.height) {
		CGImageRef cgImage = CGImageCreateWithImageInRect(outputImage.CGImage,
														  [ITUtilities rectForCenter:(CGPoint) { outputImage.size.width * 0.5f, outputImage.size.height * 0.5f }
																			 andSize:(CGSize) { outputImage.size.height, outputImage.size.height }]);
		outputImage = [[UIImage alloc] initWithCGImage:cgImage scale:outputImage.scale orientation:outputImage.imageOrientation];
		CGImageRelease(cgImage);
	}
	return outputImage;
}

+ (UIImage *)squareResizedImageFromImage:(UIImage *)outputImage newSize:(float)sSize {
	if (outputImage.size.width > outputImage.size.height) {
		CGImageRef cgImage = CGImageCreateWithImageInRect(outputImage.CGImage,
														  [ITUtilities rectForCenter:(CGPoint) { outputImage.size.width * 0.5f, outputImage.size.height * 0.5f }
																			 andSize:(CGSize) { outputImage.size.height, outputImage.size.height }]);
		CGImageRef resizedImage = CGImageCreateWithImageInRect(cgImage, (CGRect) { {0, 0}, {sSize, sSize} });
		CGImageRelease(cgImage);
		outputImage = [[UIImage alloc] initWithCGImage:resizedImage scale:outputImage.scale orientation:outputImage.imageOrientation];
		CGImageRelease(resizedImage);
	} else {
		UIGraphicsBeginImageContext((CGSize) { sSize, sSize });
		CGContextDrawImage(UIGraphicsGetCurrentContext(), (CGRect) { {0, 0}, {sSize, sSize} }, outputImage.CGImage);
		outputImage = [[UIImage alloc] initWithCGImage:UIGraphicsGetImageFromCurrentImageContext().CGImage scale:outputImage.scale orientation:outputImage.imageOrientation];
		UIGraphicsEndImageContext();
	}
	return outputImage;
}

+ (UIColor *)colorWithRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(NSInteger)alpha {
    return [UIColor colorWithRed:(float)red / 255.0f green:(float)green / 255.0f blue:(float)blue / 255.0f alpha:(float)alpha / 255.0f];
}
    
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert {
	if ([stringToConvert hasPrefix:@"#"]) {
		stringToConvert = [stringToConvert stringByReplacingOccurrencesOfString:@"#" withString:@""];
	}
    NSScanner *scanner = [NSScanner scannerWithString:stringToConvert];
    unsigned hex;
    if ([scanner scanHexInt:&hex]) {
        int r = (hex >> 16) & 0xFF;
        int g = (hex >> 8) & 0xFF;
        int b = (hex) & 0xFF;
        return [ITUtilities colorWithRed:r green:g blue:b alpha:255];
    } else {
        unsigned long long hexLong;
        if ([scanner scanHexLongLong:&hexLong]) {
            int a = (hexLong >> 32) & 0xFF;
            int r = (hexLong >> 16) & 0xFF;
            int g = (hexLong >> 8) & 0xFF;
            int b = (hexLong) & 0xFF;
            
            return [ITUtilities colorWithRed:r green:g blue:b alpha:a];
        }
    }
    return nil;
}

+ (NSString *)hexStringForColor:(UIColor *)color {
	const CGFloat *components = CGColorGetComponents(color.CGColor);
	CGFloat r = components[0];
	CGFloat g = components[1];
	CGFloat b = components[2];
	NSString *hexString=[NSString stringWithFormat:@"%02X%02X%02X", (int)(r * 255), (int)(g * 255), (int)(b * 255)];
	return hexString;
}

+ (UIImage *)boxblurFromImage:(UIImage *)source withBlur:(CGFloat)blur {
    if (blur < 0.f || blur > 1.f) {
        blur = 0.5f;
    }
    int boxSize = (int)(blur * 50);
    boxSize = boxSize - (boxSize % 2) + 1;
    
    CGImageRef img = source.CGImage;
    
    vImage_Buffer inBuffer, outBuffer;
    
    vImage_Error error;
    
    void *pixelBuffer;
    
    
    //create vImage_Buffer with data from CGImageRef
    
    CGDataProviderRef inProvider = CGImageGetDataProvider(img);
    CFDataRef inBitmapData = CGDataProviderCopyData(inProvider);
    
    inBuffer.width = CGImageGetWidth(img);
    inBuffer.height = CGImageGetHeight(img);
    inBuffer.rowBytes = CGImageGetBytesPerRow(img);
    
    inBuffer.data = (void*)CFDataGetBytePtr(inBitmapData);
    
    //create vImage_Buffer for output
    
    pixelBuffer = malloc(CGImageGetBytesPerRow(img) * CGImageGetHeight(img));
    
    if(pixelBuffer == NULL)
        NSLog(@"No pixelbuffer");
    
    outBuffer.data = pixelBuffer;
    outBuffer.width = CGImageGetWidth(img);
    outBuffer.height = CGImageGetHeight(img);
    outBuffer.rowBytes = CGImageGetBytesPerRow(img);
    
    //perform convolution
    error = vImageBoxConvolve_ARGB8888(&inBuffer, &outBuffer, NULL, 0, 0, boxSize, boxSize, NULL, kvImageEdgeExtend);
    
    
    if (error) {
        NSLog(@"error from convolution %ld", error);
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef ctx = CGBitmapContextCreate(outBuffer.data,
                                             outBuffer.width,
                                             outBuffer.height,
                                             8,
                                             outBuffer.rowBytes,
                                             colorSpace,
                                             (enum CGBitmapInfo)kCGImageAlphaNoneSkipLast);
    CGImageRef imageRef = CGBitmapContextCreateImage (ctx);
    UIImage *returnImage = [UIImage imageWithCGImage:imageRef];
    
    //clean up
    CGContextRelease(ctx);
    CGColorSpaceRelease(colorSpace);
    
    free(pixelBuffer);
    CFRelease(inBitmapData);
    
    CGImageRelease(imageRef);
//	CGColorSpaceRelease(colorSpace);
    
    return returnImage;
}

+ (UIImage *)screenshotFromView:(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, YES, [UIScreen mainScreen].scale);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (CGRect)rectForCenter:(CGPoint)center andSize:(CGSize)size {
	return (CGRect) { {center.x - size.width * 0.5f, center.y - size.height * 0.5f}, {size.width, size.height} };
}

+ (CGPoint)centerForRect:(CGRect)rect {
    return (CGPoint) { rect.origin.x + rect.size.width * 0.5f, rect.origin.y + rect.size.height * 0.5f };
}

#elif TARGET_OS_MAC
#pragma mark TARGET_OS_MAC

+ (void)loadNibName:(NSString *)name forWindowController:(NSWindowController *)owner {
    [[NSBundle mainBundle] loadNibNamed:name owner:owner topLevelObjects:nil];
}

+ (void)setBackgroundColor:(NSColor *)color forView:(NSView *)view {
    [view.layer setBackgroundColor:color.CGColor];
}

+ (NSRect)rectForCenter:(NSPoint)center andSize:(NSSize)size {
	return (CGRect) { {center.x - size.width * 0.5f, center.y - size.height * 0.5f}, {size.width, size.height} };
}

+ (NSPoint)centerForRect:(NSRect)rect {
    return (NSPoint) { rect.origin.x + rect.size.width * 0.5f, rect.origin.y + rect.size.height * 0.5f };
}

#pragma mark -
#endif

+ (NSDictionary *)parseURLQuery:(NSURL *)url {
	NSString *query = [url query];
	NSArray *components = [query componentsSeparatedByString:@"&"];
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	for (NSString *component in components) {
		NSArray *subcomponents = [component componentsSeparatedByString:@"="];
		[parameters setObject:[[subcomponents objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
					   forKey:[[subcomponents objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	}
	return [NSDictionary dictionaryWithDictionary:parameters];
}

+ (BOOL)isRetinaDisplay
{
#if TARGET_OS_IPHONE
	return ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0f));
#else
    return [[NSScreen mainScreen] respondsToSelector:@selector(backingScaleFactor)] && ([[NSScreen mainScreen] backingScaleFactor] == 2.0f);
#endif
}

+ (NSString *)platform
{
	size_t size;
	sysctlbyname("hw.machine", NULL, &size, NULL, 0);
	char *machine = (char *)malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
	free(machine);
	return platform;
}

+ (NSURL *)documentsDirectoryURL {
	NSArray *appSupportDirs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
	return [appSupportDirs objectAtIndex:0];
}

+ (NSString *)documentsDirectoryString {
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

+ (NSURL *)libraryDirectoryURL {
	NSArray *appSupportDirs = [[NSFileManager defaultManager] URLsForDirectory:NSLibraryDirectory inDomains:NSUserDomainMask];
	return [appSupportDirs objectAtIndex:0];
}

+ (NSString *)libraryDirectoryString {
	return [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

+ (NSURL *)cachesDirectoryURL {
    NSArray *appSupportDirs = [[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask];
    return [appSupportDirs objectAtIndex:0];
}

+ (NSString *)cachesDirectoryString {
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

+ (void)deallocLog:(Class)c {
    NSLog(@"%@ - %@", NSStringFromClass(c), @"deallocated");
}

@end
















