//
//  ITThread.h
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 28/01/14.
//
//

#import <Foundation/Foundation.h>

typedef void (^Runnable)(void);
typedef void (^RunnableParam)(NSObject *obj);

@interface ITThread : NSThread

+ (void)waitForSeconds:(NSTimeInterval)timeInterval;

- (id)initWithRunnable:(Runnable)runnable;
- (id)initWithRunnable:(RunnableParam)runnable withParameter:(id)param;
- (void)performOnMainThread:(Runnable)runnable waitUntilDone:(BOOL)wait;
- (void)performOnMainThread:(RunnableParam)runnable withParameter:(id)param waitUntilDone:(BOOL)wait;

@end
