//
//  ITURLLoader.m
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 7/25/12.
//  Copyright (c) 2012 42Tecnologia. All rights reserved.
//

#import "ITURLLoader.h"

const ITURLEvent ITURLEventComplete = @"ITURLEventComplete";
const ITURLEvent ITURLEventIOError = @"ITURLEventIOError";
const ITURLEvent ITURLEventProgress = @"ITURLEventProgress";
const ITURLEvent ITURLEventStart = @"ITURLEventStart";

const ITURLMethod ITURLMethod_GET = @"GET";
const ITURLMethod ITURLMethod_POST = @"POST";
const ITURLMethod ITURLMethod_PUT = @"PUT";
const ITURLMethod ITURLMethod_POST_JSON = @"POST-JSON";

@implementation ITURLEventListener

@synthesize listener = _listener, selector = _selector;

- (void)dealloc
{
	self.listener = nil;
	self.selector = nil;
}

- (id)initWithListener:(id)l action:(SEL)sel
{
	self = [super init];
	if (self != nil)
	{
		self.listener = l;
		self.selector = sel;
	}
	return self;
}

@end

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

@implementation ITURLLoaderEvent
{
	id _data;
	ITURLEvent _type;
	__unsafe_unretained
	id _target;
}

@synthesize totalBytes, bytesLoaded, statusCode;
@synthesize data = _data, target = _target, type = _type;

- (void)dealloc
{
	self.target = nil;
	self.data = nil;
	self.type = nil;
}

- (id)initWithTarget:(id)tgt type:(ITURLEvent)tp andData:(id)dt
{
	self = [super init];
	if (self != nil)
	{
		self.target = tgt;
		self.type = tp;
		self.data = dt;
	}
	return self;
}

@end

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

@interface ITURLLoader ()

@property (nonatomic, strong) ITURLEventListener		*complete;
@property (nonatomic, strong) ITURLEventListener		*ioError;
@property (nonatomic, strong) ITURLEventListener		*progress;
@property (nonatomic, strong) ITURLEventListener		*start;
@property (nonatomic, strong) ITURLEventBlock			completeBlock;
@property (nonatomic, strong) ITURLEventBlock			ioErrorBlock;
@property (nonatomic, strong) ITURLEventBlock			progressBlock;
@property (nonatomic, strong) ITURLEventBlock			startBlock;

@property (nonatomic, strong) NSURLConnection			*connection;
@property (nonatomic, strong) NSMutableURLRequest		*request;
@property (nonatomic, strong) NSHTTPURLResponse 		*response;
@property (nonatomic, strong) NSMutableData				*connectionData;
@property (nonatomic, assign) long long 				totalBytes;
@property (nonatomic, assign) NSInteger 				statusCode;
@property (nonatomic, strong) NSDictionary 				*allHeaderFields;

- (NSString *)extractParams:(NSDictionary *)parameters;

@end

@implementation ITURLLoader
{
	ITURLEventListener					*_complete;
	ITURLEventListener					*_ioError;
	ITURLEventListener					*_progress;
	ITURLEventListener					*_start;
	ITURLEventBlock						_completeBlock;
	ITURLEventBlock						_ioErrorBlock;
	ITURLEventBlock						_progressBlock;
	ITURLEventBlock						_startBlock;
	
	NSURLConnection						*_connection;
	NSMutableURLRequest					*_request;
	NSHTTPURLResponse 					*_response;
	NSMutableData						*_connectionData;
	NSDictionary						*_allHeaderFields;
	
	long long _totalBytes;
	NSInteger _statusCode;
}

@synthesize complete = _complete, ioError = _ioError, progress = _progress, start = _start, connection = _connection, connectionData = _connectionData;
@synthesize totalBytes = _totalBytes, allHeaderFields = _allHeaderFields, completeBlock = _completeBlock, ioErrorBlock = _ioErrorBlock;
@synthesize progressBlock = _progressBlock, startBlock = _startBlock, response = _response;
@synthesize request = _request;
@synthesize statusCode = _statusCode, loading;

static ITURLEventBlock _connectionsBlock = nil;
static NSMutableArray *_connectionsArray = nil;

- (void)dealloc
{
	[self close];
	self.complete = nil;
	self.ioError = nil;
	self.progress = nil;
	self.start = nil;
	self.completeBlock = nil;
	self.ioErrorBlock = nil;
	self.progressBlock = nil;
	self.startBlock = nil;
	
	self.connection = nil;
	self.request = nil;
	self.response = nil;
	self.connectionData = nil;
	self.allHeaderFields = nil;
}

+ (void)setListenerToExistingConnections:(ITURLEventBlock)block {
    _connectionsBlock = block;
}

- (id) initWithURL:(NSString *)url parameters:(NSDictionary *)parameters
			method:(NSString *)method files:(NSDictionary *)files
{
	self = [super init];
    if(self != nil)
	{
		BOOL multipart = NO;
		
//		if ([url rangeOfString:@"http://"].location != NSNotFound) {
//			url = [url stringByReplacingOccurrencesOfString:@"http://" withString:@"https://"];
//		}
		
		if (parameters != nil && files == nil)
		{
			for (id key in parameters.allKeys)
			{
				id obj = [parameters objectForKey:key];
				if ([obj isKindOfClass:[NSString class]])
				{
					obj = [obj stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    obj = [obj stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
					NSMutableDictionary *newParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
					[newParameters setObject:obj forKey:key];
					parameters = newParameters;
				}
			}
		}
		
        if(files != nil) multipart = YES;
        
        if([@"POST" isEqualToString:method])
		{
            self.request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                          cachePolicy:NSURLRequestReloadIgnoringCacheData
                                      timeoutInterval:20];
            [self.request setHTTPMethod:@"POST"];
            
            if(multipart)
			{
                NSString *stringBoundary = @"bkLPIaj23aUYJdkj786def45vrLKJCBRTCQLPN42T3CN0L0G14";
                NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", stringBoundary];
                [self.request addValue:contentType forHTTPHeaderField: @"Content-Type"];
                
                // request body
                NSMutableData *body = [NSMutableData data];
                
                // file fields
                NSEnumerator *enumerator = [files keyEnumerator];
                id key;
                int nameIdx = 0;
                while (key = [enumerator nextObject])
				{
                    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"picture%d\"\r\n", key, nameIdx] dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
					
                    [body appendData:(NSData *) [files objectForKey:key]];
                    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                    nameIdx++;
                }
                
                // data fields
                
                enumerator = [parameters keyEnumerator];
                while (key = [enumerator nextObject])
				{
                    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[[NSString stringWithString:[parameters objectForKey:key]] dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                }
                if([body length] > 0)
                    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                // set request body
                [self.request setHTTPBody:body];
            }
			else
			{
                [self.request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                NSString *params = [self extractParams:parameters];
                if(params != nil)
				{
                    NSData *data = [params dataUsingEncoding:NSUTF8StringEncoding];
                    [self.request setHTTPBody:data];
                }
            }
        }
		else if ([@"GET" isEqualToString:method]) // GET
		{
            if(multipart)
			{
                @throw [[NSException alloc] initWithName:@"Invalid behavior"
                                                  reason:@"Cannot use multipart/form-data with a GET request"
                                                userInfo:nil];
            }
            
            NSString *params = [self extractParams:parameters];
			if(params != nil) {
                url = [NSString stringWithFormat:@"%@?%@", url, params];
			}
			
            self.request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
            [self.request setHTTPMethod:@"GET"];
		} else if ([@"POST-JSON" isEqualToString:method]) {
			self.request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
			[self.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
			[self.request setHTTPMethod:@"POST"];
			
			if (parameters != nil) {
				NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:nil];
				[self.request setHTTPBody:data];
			}
		} else if ([@"PUT" isEqualToString:method]) {
			self.request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
			[self.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
			[self.request setHTTPMethod:@"PUT"];
			
			if (parameters != nil) {
				NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:nil];
				[self.request setHTTPBody:data];
			}
		}
		
		self.connectionData = [[NSMutableData alloc] init];
    }
    return self;
}

- (id)initWithURL:(NSString *)url parameters:(NSDictionary *)parameters method:(NSString *)method {
    return [self initWithURL:url parameters:parameters method:method files:nil];
}

- (NSString *)extractParams:(NSDictionary *)parameters
{
    NSString *params = nil;
    if(parameters != nil)
	{
        params = @"";
        NSEnumerator *enumerator = [parameters keyEnumerator];
        id key;
        while (key = [enumerator nextObject])
		{
            if([params length] == 0)
                params = [NSString stringWithFormat:@"%@=%@", key, [parameters objectForKey:key]];
            else
                params = [NSString stringWithFormat:@"%@&%@=%@", params, key, [parameters objectForKey:key]];
        }
    }
    return params;
}

- (void)setListenerToEvent:(ITURLEvent)event listener:(id)listener action:(SEL)selector {
	if (event == ITURLEventComplete) {
		self.complete = [[ITURLEventListener alloc] initWithListener:listener action:selector];
		self.completeBlock = nil;
	} else if (event == ITURLEventIOError) {
		self.ioError = [[ITURLEventListener alloc] initWithListener:listener action:selector];
		self.ioErrorBlock = nil;
	} else if (event == ITURLEventProgress) {
		self.progress = [[ITURLEventListener alloc] initWithListener:listener action:selector];
		self.progressBlock = nil;
	} else if (event == ITURLEventStart) {
		self.start = [[ITURLEventListener alloc] initWithListener:listener action:selector];
		self.startBlock = nil;
	}
}

- (void)setListenerToEvent:(const ITURLEvent)event withBlock:(ITURLEventBlock)block {
	if (event == ITURLEventComplete) {
		self.completeBlock = block;
		self.complete = nil;
	} else if (event == ITURLEventIOError) {
		self.ioErrorBlock = block;
		self.ioError = nil;
	} else if (event == ITURLEventProgress) {
		self.progressBlock = block;
		self.progress = nil;
	} else if (event == ITURLEventStart) {
		self.startBlock = block;
		self.start = nil;
	}
}

- (void)setBasicAuthWithLogin:(NSString *)userName andPassword:(NSString *)password {
	NSString *authStr = [NSString stringWithFormat:@"%@:%@", userName, password];
	NSData *authData = [authStr dataUsingEncoding:NSASCIIStringEncoding];
	NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn]];
	[self.request setValue:authValue forHTTPHeaderField:@"Authorization"];
}

- (void)setBearerAuthWithToken:(NSString *)token {
	[self.request setValue:[NSString stringWithFormat:@"Bearer %@", token] forHTTPHeaderField:@"Authorization"];
}

- (void)addHeaderField:(NSString *)key andValue:(NSString *)value {
	[self.request setValue:value forHTTPHeaderField:key];
}

- (void)open
{
    if (_connectionsArray == nil) {
        _connectionsArray = [[NSMutableArray alloc] init];
        
        if (_connectionsBlock != nil) {
            _connectionsBlock([[ITURLLoaderEvent alloc] initWithTarget:self type:ITURLEventStart andData:nil]);
        }
    }
	
	self.connection = [[NSURLConnection alloc] initWithRequest:self.request delegate:self startImmediately:NO];
	[self.connection start];
	loading = YES;
    
    [_connectionsArray addObject:self];
}

- (void)removeFromConnections {
    if (_connectionsArray != nil) {
        [_connectionsArray removeObject:self];
        
        if (_connectionsArray.count == 0) {
            if (_connectionsBlock != nil) {
				_connectionsBlock([[ITURLLoaderEvent alloc] initWithTarget:self type:ITURLEventIOError andData:nil]);
			}
            _connectionsArray = nil;
        }
    }
}

- (void)close
{
    loading = NO;
	[self.connection cancel];
    
    [self removeFromConnections];
}

- (void)executeEvent:(ITURLLoaderEvent *)event withListener:(ITURLEventListener *)listener
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
	if ([listener.listener respondsToSelector:listener.selector])
		[listener.listener performSelectorOnMainThread:listener.selector withObject:event waitUntilDone:YES];
#pragma clang diagnostic pop
}

- (void)executeEvent:(ITURLLoaderEvent *)event withBlock:(ITURLEventBlock)block {
	block(event);
}

- (void)dispatchEvent:(ITURLLoaderEvent *)event
{
	if (event.type == ITURLEventStart) {
		if (self.start != nil) {
			[self executeEvent:event withListener:self.start];
		} else if (self.startBlock != nil) {
			[self executeEvent:event withBlock:self.startBlock];
			self.startBlock = nil;
		}
		self.start = [[ITURLEventListener alloc] initWithListener:self.start.listener action:self.start.selector];
	} else if (event.type == ITURLEventComplete) {
		if (self.complete != nil) {
			[self executeEvent:event withListener:self.complete];
		} else if (self.completeBlock != nil) {
			[self executeEvent:event withBlock:self.completeBlock];
			self.completeBlock = nil;
		}
		self.complete = [[ITURLEventListener alloc] initWithListener:self.complete.listener action:self.complete.selector];
	} else if (event.type == ITURLEventIOError) {
		if (self.ioError != nil) {
			[self executeEvent:event withListener:self.ioError];
		} else if (self.ioErrorBlock != nil) {
			[self executeEvent:event withBlock:self.ioErrorBlock];
			self.ioErrorBlock = nil;
		}
		self.ioError = [[ITURLEventListener alloc] initWithListener:self.ioError.listener action:self.ioError.selector];
	} else if (event.type == ITURLEventProgress) {
		if (self.progress != nil) {
			[self executeEvent:event withListener:self.progress];
		} else if (self.progressBlock != nil) {
			[self executeEvent:event withBlock:self.progressBlock];
			self.progressBlock = nil;
		}
		self.progress = [[ITURLEventListener alloc] initWithListener:self.progress.listener action:self.progress.selector];
	}
}

#pragma mark Connection Delegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [self.connectionData setLength:0];
	self.response = (NSHTTPURLResponse *)response;
    NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
    self.totalBytes = resp.expectedContentLength;
    self.statusCode = resp.statusCode;
    self.allHeaderFields = resp.allHeaderFields;
    
    ITURLLoaderEvent *evt = [[ITURLLoaderEvent alloc] initWithTarget:self type:ITURLEventStart andData:resp];
    evt.totalBytes = self.totalBytes;
    evt.statusCode = self.statusCode;
	
	[self dispatchEvent:evt];
}

- (void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten
 totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite
{
	ITURLLoaderEvent *evt = [[ITURLLoaderEvent alloc] initWithTarget:self type:ITURLEventProgress andData:nil];
    evt.totalBytes = totalBytesExpectedToWrite;
    evt.bytesLoaded = totalBytesWritten;
    [self dispatchEvent:evt];
}

- (void) connection:(NSURLConnection *) connection didReceiveData:(NSData *)data {
    [self.connectionData appendData:data];
	ITURLLoaderEvent *evt = [[ITURLLoaderEvent alloc] initWithTarget:self type:ITURLEventProgress andData:self.connectionData];
	evt.totalBytes = self.response.expectedContentLength;
    evt.bytesLoaded = [self.connectionData length];
    [self dispatchEvent:evt];
}

- (void) connection:(NSURLConnection *) connection didFailWithError:(NSError *)error {
	loading = NO;
	ITURLLoaderEvent *evt = [[ITURLLoaderEvent alloc] initWithTarget:self type:ITURLEventIOError andData:error];
    [self dispatchEvent:evt];
    [self removeFromConnections];
}

- (void) connectionDidFinishLoading:(NSURLConnection *) connection {
	loading = NO;
	ITURLLoaderEvent *evt = [[ITURLLoaderEvent alloc] initWithTarget:self type:ITURLEventComplete andData:self.connectionData];
    [self dispatchEvent:evt];
    [self removeFromConnections];
}

@end































