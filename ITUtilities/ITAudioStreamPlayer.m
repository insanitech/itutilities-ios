//
//  AudioPlayer.m
//  Shot
//
//  Created by Anderson Lucas C. Ramos on 9/1/11.
//  Copyright 2011 Tangivel Tecnologia. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <SpeexLib/ITSpeexEncDec.h>
#import "ITAudioQueueCore.h"
#import "ITQueue.h"
#import "ITAudioStreamPlayer.h"
#import "ITAudioStreamRecorder.h"

#pragma mark TTAudioChunk Implementation

@implementation ITAudioChunk

- (id)initWithData:(short *)data size:(int)size
{
    self = [super init];
    if (self != nil)
    {
        audioData = data;
        audioSize = size;
    }
    return self;
}

- (void)dealloc 
{
    free(audioData);
}

@end

#pragma mark -

#pragma mark AudioQueue Callback Methods Definition

static void HandleOutputBuffer(void *aqData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer);
static void IsRunningCallback(void *inUserData, AudioQueueRef inAQ, AudioQueuePropertyID inID);

#pragma mark -

#pragma mark TTAudioStreamPlayer Implementation

ITAudioStreamPlayer *_self;

@implementation ITAudioStreamPlayer

- (id)init
{
    self = [super init];
    if (self != nil) 
    {
        _playerState = malloc(sizeof(AQPlayerState));
		AQPlayerState *playerState = (AQPlayerState *)_playerState;
        playerState->mDataFormat.mFormatID = AUDIO_FORMAT;
        playerState->mDataFormat.mSampleRate = AUDIO_SAMPLE_RATE;
        playerState->mDataFormat.mChannelsPerFrame = AUDIO_CHANNELS_PER_FRAME;
        playerState->mDataFormat.mBitsPerChannel = AUDIO_BITS_PER_CHANNEL;
        playerState->mDataFormat.mBytesPerPacket = AUDIO_BYTES_PER_PACKET;
        playerState->mDataFormat.mBytesPerFrame = AUDIO_BYTES_PER_FRAME;
        playerState->mDataFormat.mFramesPerPacket = AUDIO_FRAMES_PER_PACKET;
        playerState->mDataFormat.mFormatFlags = AUDIO_FLAG_CANONICAL;
        playerState->mNumFreeBuffers = kNumberBuffers;
        playerState->mQueue = NULL;
        _initialized = NO;
        _mArray = [[ITQueue alloc] init];
        _decoder = [[ITSpeexDecoder alloc] initWithBandType:SpeexBandType_Narrow];
		playerState->mIsRunning = NO;
    }
    
    return _self = self;
}

- (void)playStream:(UInt32)bufferByteSize
{
	AQPlayerState *playerState = (AQPlayerState *)_playerState;
	
    if (!_initialized)
    {		
        playerState->bufferByteSize = bufferByteSize;
        
        if (AudioQueueNewOutput(&playerState->mDataFormat, 
								HandleOutputBuffer, 
								playerState, 
								CFRunLoopGetCurrent(), 
								kCFRunLoopCommonModes,
								0, 
								&playerState->mQueue) == noErr)
        {
            for (int i = 0; i < kNumberBuffers; ++i) 
            {
                AudioQueueAllocateBuffer(playerState->mQueue, playerState->bufferByteSize, &playerState->mBuffers[i]);
                HandleOutputBuffer(&_playerState, playerState->mQueue, playerState->mBuffers[i]);
            }
			playerState->mNumFreeBuffers = 0;
            
            UInt32 gain = 2.5f;
            AudioQueueSetParameter(playerState->mQueue, kAudioQueueParam_Volume, gain);
            
            _initialized = YES;
            playerState->mIsRunning = YES;
            AudioQueueStart(playerState->mQueue, NULL);
        }
    }
    else if (_mArray.count >= kNumberBuffers)
    {
		for (int i = 0; i < kNumberBuffers; i++)
			HandleOutputBuffer(playerState, playerState->mQueue, playerState->mBuffers[i]);
		playerState->mNumFreeBuffers = 0;
		playerState->mIsRunning = YES;
        AudioQueueStart(playerState->mQueue, NULL);
    }
}

- (void)setAudioOutputSpeaker:(BOOL)spk
{
    if (spk)
    {
        UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
        AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute, sizeof(audioRouteOverride), &audioRouteOverride);
    }
    else
    {
        UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_None;
        AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute, sizeof(audioRouteOverride), &audioRouteOverride);
    }
}

- (void)playStream
{
    [self playStream:AUDIO_BUFFER_BYTE_SIZE];
}

- (void)stopStream
{
    ((AQPlayerState *)_playerState)->mIsRunning = NO;
    if (((AQPlayerState *)_playerState)->mQueue != NULL)
        AudioQueuePause(((AQPlayerState *)_playerState)->mQueue);
}

- (BOOL)isPlaying
{
    return ((AQPlayerState *)_playerState)->mIsRunning;
}

- (void)setDelegate:(id<ITAudioStreamPlayerDelegate>)del
{
    _delegate = del;
}

- (ITAudioChunk *)receivedBuffer:(byte *)data length:(int)len speexEncoded:(BOOL)enc
{
    ITAudioChunk *chunk = nil;
	AQPlayerState *playerState = (AQPlayerState *)_playerState;
    if (data != NULL && len > 0)
    {
        if (enc)
        {
            SpeexEncodedPackets *encData = malloc(sizeof(SpeexEncodedPackets));
            int spxSize = (([_decoder bandType] == SpeexBandType_Narrow) ? SPEEX_NB_SIZE : SPEEX_WB_SIZE);
            encData->length = len / spxSize;
            encData->byteAmount = encData->length * 2 * FRAME_SIZE;
            
            if (encData->byteAmount <= AUDIO_BUFFER_BYTE_SIZE)
            {
                encData->spxData = calloc(encData->length, sizeof(SpeexData *));
                int current = 0;
                for (int i = 0; i < encData->length; i++)
                {
                    SpeexData *sData = malloc(sizeof(SpeexData));
                    sData->cbits = calloc(spxSize, sizeof(char));
                    for (int j = 0; j < spxSize; j++)
                    {
                        sData->cbits[j] = data[current];
                        current++;
                    }
                    sData->nBytes = spxSize;
                    encData->spxData[i] = sData;
                }
                short *chunkData;
                [_decoder decodePacket:encData outData:&chunkData];
                chunk = [[ITAudioChunk alloc] initWithData:chunkData size:encData->byteAmount];
                [_mArray enqueue:(__bridge_retained void *)chunk];
                
                FreeSpeexEncPacket(encData);
            }
            else
            {
                int current = 0;
                int lastLength = encData->length;
                encData->length = 25;
                encData->spxData = calloc(encData->length, sizeof(SpeexData *));
                for (int i = 0; i < encData->length; i++)
                {
                    SpeexData *sData = malloc(sizeof(SpeexData));
                    sData->cbits = calloc(spxSize, sizeof(char));
                    for (int j = 0; j < spxSize; j++)
                    {
                        sData->cbits[j] = data[current];
                        current++;
                    }
                    sData->nBytes = spxSize;
                    encData->spxData[i] = sData;
                }
                short *chunkData;
                [_decoder decodePacket:encData outData:&chunkData];
                chunk = [[ITAudioChunk alloc] initWithData:chunkData size:encData->byteAmount];
                [_mArray enqueue:(__bridge_retained void *)chunk];
                
                FreeSpeexEncPacket(encData);
                
                SpeexEncodedPackets *encData = malloc(sizeof(SpeexEncodedPackets));
                encData->length = lastLength - 25;
                encData->byteAmount = encData->length * 2 * FRAME_SIZE;
                encData->spxData = calloc(encData->length, sizeof(SpeexData *));
                for (int i = 0; i < encData->length; i++)
                {
                    SpeexData *sData = malloc(sizeof(SpeexData));
                    sData->cbits = calloc(spxSize, sizeof(char));
                    for (int j = 0; j < spxSize; j++)
                    {
                        sData->cbits[j] = data[current];
                        current++;
                    }
                    sData->nBytes = spxSize;
                    encData->spxData[i] = sData;
                }

                [_decoder decodePacket:encData outData:&chunkData];
                chunk = [[ITAudioChunk alloc] initWithData:chunkData size:encData->byteAmount];
                [_mArray enqueue:(__bridge_retained void *)chunk];
                
                FreeSpeexEncPacket(encData);
            }
        }
        else
        {
            chunk = [[ITAudioChunk alloc] initWithData:(short *)data size:len];
            [_mArray enqueue:(__bridge_retained void *)chunk];
        }
        
        if (!_initialized && _mArray.count >= kNumberBuffers)
            [self playStream:AUDIO_BUFFER_BYTE_SIZE];
        else if (!playerState->mIsRunning && _mArray.count >= kNumberBuffers)
        {
			for (int i = 0; i < kNumberBuffers; i++)
				HandleOutputBuffer(playerState, playerState->mQueue, playerState->mBuffers[i]);
			playerState->mNumFreeBuffers = 0;
			playerState->mIsRunning = YES;
			AudioQueueStart(playerState->mQueue, NULL);
        }
    }
    return chunk;
}

- (void)clearBuffers
{
    if (((AQPlayerState *)_playerState)->mQueue != NULL)
    {
        AudioQueueFlush(((AQPlayerState *)_playerState)->mQueue);
        AudioQueueStop(((AQPlayerState *)_playerState)->mQueue, YES);
    }
    while (_mArray.count >  0)
        [_mArray dequeue];
}

- (BOOL)hasPacketsOnQueue
{
    if (_mArray.count > 0)
        return YES;
    return NO;
}

- (void)dealloc 
{
    AudioQueueDispose(((AQPlayerState *)_playerState)->mQueue, YES);
	free((void *)_playerState);
}

void HandleOutputBuffer(void *aqData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer) 
{
    AQPlayerState *playerState = (AQPlayerState *)aqData;
    ITAudioChunk *deq = (__bridge ITAudioChunk *)[_self->_mArray dequeue];
    if (deq != nil)
    {
		memcpy(inBuffer->mAudioData, deq->audioData, deq->audioSize);
        inBuffer->mAudioDataByteSize = deq->audioSize;
        AudioQueueEnqueueBuffer(inAQ, inBuffer, 0, NULL);
    }
	else 
		playerState->mNumFreeBuffers++;
    
//	NSLog(@"\nNum free Buffers: %lu - Num Packets to Read: %i", playerState->mNumFreeBuffers, _self->_mArray.count);
	
    if (playerState->mNumFreeBuffers == kNumberBuffers &&
        _self->_mArray.count == 0)
    {
        AudioQueuePause(inAQ);
        playerState->mIsRunning = NO;
        if ([_self->_delegate respondsToSelector:@selector(didFinishPlayingAudioData)])
            [_self->_delegate didFinishPlayingAudioData];
    }
}

@end




