//
//  ITTimer.h
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 4/2/12.
//  Copyright (c) 2012 Tangível Tecnologia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ITTimer;

typedef void (^BlockTarget)(ITTimer *timer);

@interface ITTimer : NSObject

@property (nonatomic, strong, readonly) NSTimer *timer;
@property (nonatomic, strong, readonly) id userInfo;
@property (nonatomic, assign, readonly) BOOL isValid;

- (instancetype)initWithTimeInterval:(NSTimeInterval)ti userInfo:(id)userInfo repeats:(BOOL)yesOrNo;
- (instancetype)setBlockTarget:(BlockTarget)target;
- (void)start;
- (void)stop;

@end
