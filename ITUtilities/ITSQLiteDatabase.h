//
//  QDSliteDatabase.h
//  QDDatabase
//
//  Created by Anderson Lucas C. Ramos on 3/28/12.
//  Copyright (c) 2012 Tangível Tecnologia. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NSString *ITDatabaseEvent;
extern const ITDatabaseEvent ITDatabaseEventComplete;
extern const ITDatabaseEvent ITDatabaseEventIOError;
extern const ITDatabaseEvent ITDatabaseEventProgress;
extern const ITDatabaseEvent ITDatabaseEventStart;

@class ITSQLiteDataReader;
@class ITSQLiteDatabase;

typedef void (^ITDatabaseEventBlock)(ITDatabaseEvent event, ITSQLiteDataReader *reader);

typedef NS_ENUM(NSInteger, ITSQLiteDataType) {
	ITSQLiteDataTypeInteger,
	ITSQLiteDataTypeText,
	ITSQLiteDataTypeFloat,
	ITSQLiteDataTypeBlob,
	ITSQLiteDataTypeNull
};

@interface ITSQLiteDataReader: NSObject

@property (nonatomic, readonly) float percent;
@property (nonatomic, strong, readonly) NSString *errorMessage;

- (id)init;
- (BOOL)next;
- (NSNumber *)getInt:(NSString *)colummName;
- (NSNumber *)getInt:(NSString *)colummName atIndex:(NSUInteger)index;
- (NSNumber *)getInteger:(NSString *)columnName;
- (NSNumber *)getInteger:(NSString *)columnName atIndex:(NSUInteger)index;
- (NSNumber *)getLong:(NSString *)columnName;
- (NSNumber *)getLong:(NSString *)columnName atIndex:(NSUInteger)index;
- (NSNumber *)getFloat:(NSString *)columnName;
- (NSNumber *)getFloat:(NSString *)columnName atIndex:(NSUInteger)index;
- (NSNumber *)getDouble:(NSString *)columnName;
- (NSNumber *)getDouble:(NSString *)columnName atIndex:(NSUInteger)index;
- (NSString *)getString:(NSString *)columnName;
- (NSString *)getString:(NSString *)columnName atIndex:(NSUInteger)index;
- (NSData *)getData:(NSString *)columnName;
- (NSData *)getData:(NSString *)columnName atIndex:(NSUInteger)index;
- (ITSQLiteDataType)getColumnDataType:(NSString *)columnName;
- (NSArray *)columnNames;
- (NSUInteger)rowsCount;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

@interface ITSQLiteDatabase : NSObject

@property (nonatomic, readonly) BOOL connected;
@property (nonatomic, readonly) BOOL querying;
@property (strong, nonatomic, readonly) NSString *query;

+ (BOOL)databaseExists:(NSString *)dbFileName;
+ (ITSQLiteDatabase *)createDatabase:(NSString *)dbFileName;
+ (NSString *)databaseFilePath:(NSString *)dbFileName;

- (id)initWithDBName:(NSString *)dbFileName;
- (void)setListenerToEvent:(ITDatabaseEvent)eventType resultReader:(ITDatabaseEventBlock)resultBlock;
/* If willing to send sequencial non sync queries, needs to review implementation of this method. */
- (void)executeQuery:(NSString *)query;
- (void)executeNonQuery:(NSString *)query;
- (ITSQLiteDataReader *)executeSynchronousQuery:(NSString *)query;
- (NSInteger)lastInsertRowId;

@end