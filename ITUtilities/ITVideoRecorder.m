//
//  ITVideoRecorder.m
//  ITUtilities
//
//  Created by Anderson Lucas C. Ramos on 25/03/13.
//
//

#import "ITVideoRecorder.h"
#import <AVFoundation/AVFoundation.h>
#import "ITUtilities.h"

@interface ITVideoRecorder ()

@property (nonatomic, assign) BOOL recording;
@property (nonatomic, strong) AVAssetWriter *writer;
@property (nonatomic, strong) AVAssetWriterInput *writerInput;
@property (nonatomic, strong) AVAssetWriterInputPixelBufferAdaptor *writerInputAdaptor;
@property (nonatomic, strong) NSDate *startTime;

@end

@implementation ITVideoRecorder

@synthesize recording;
@synthesize startTime = _startTime;
@synthesize writer = _writer;
@synthesize writerInput = _writerInput;
@synthesize writerInputAdaptor = _writerInputAdaptor;

- (void)dealloc {
	self.writer = nil;
	self.writerInput = nil;
	self.writerInputAdaptor = nil;
	self.startTime = nil;
}

- (id)initWithOutputVideoSize:(CGSize)size inputVideoSize:(CGSize)inputSize
			 videoOrientation:(CGAffineTransform)orient {
	self = [super init];
	if (self != nil) {
		NSError *err = nil;
		self.writer = [[AVAssetWriter alloc] initWithURL:[self videoPath] fileType:AVFileTypeMPEG4 error:&err];
		if (err != nil) {
			@throw [[NSException alloc] initWithName:err.localizedFailureReason reason:err.localizedDescription userInfo:err.userInfo];
		}
		
		NSDictionary *outputSettings = [NSDictionary dictionaryWithObjectsAndKeys:
										AVVideoCodecH264, AVVideoCodecKey,
										@(size.width), AVVideoWidthKey,
										@(size.height), AVVideoHeightKey, nil];
		self.writerInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:outputSettings];
		[self.writerInput setExpectsMediaDataInRealTime:YES];
		[self.writerInput setTransform:orient];
		
		NSDictionary *sourcePixelBufferAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
													 @(kCVPixelFormatType_32BGRA), kCVPixelBufferPixelFormatTypeKey,
													 @(inputSize.width), kCVPixelBufferWidthKey,
													 @(inputSize.height), kCVPixelBufferHeightKey,
													 nil];
		self.writerInputAdaptor = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:self.writerInput
																								   sourcePixelBufferAttributes:sourcePixelBufferAttributes];
		
		if ([self.writer canAddInput:self.writerInput]) {
			[self.writer addInput:self.writerInput];
		} else {
			@throw [[NSException alloc] initWithName:self.writer.error.localizedFailureReason
											  reason:self.writer.error.localizedDescription
											userInfo:self.writer.error.userInfo];
		}
		
		if ([[NSFileManager defaultManager] fileExistsAtPath:[[self videoPath] path]]) {
			NSError *error = nil;
			[[NSFileManager defaultManager] removeItemAtURL:[self videoPath] error:&error];
			NSLog(@"%@", error);
		}
		
		[self.writer startWriting];
	}
	return self;
}

//- (void)setVideoOrientation:(CGAffineTransform)videoOrientation {
//	[self.writerInput setTransform:videoOrientation];
//}
//
//- (CGAffineTransform)videoOrientation {
//	return self.writerInput.transform;
//}

- (NSURL *)videoPath {
	return [[ITUtilities documentsDirectoryURL] URLByAppendingPathComponent:@"movie.mp4"];
}

- (CMTime)elapsedCMTime {
	float elapsedMiliseconds = [[NSDate date] timeIntervalSinceDate:self.startTime] * 1000.0f;
	return CMTimeMake((int)elapsedMiliseconds, 1000);
}

- (BOOL)startRecording {
	if (!self.recording) {
		[self.writer startSessionAtSourceTime:CMTimeMake(0, 1000)];
		self.startTime = [NSDate date];
		self.recording = YES;
		return YES;
	} else {
		@throw [[NSException alloc] initWithName:self.writer.error.localizedFailureReason
										  reason:self.writer.error.localizedDescription
										userInfo:self.writer.error.userInfo];
	}
	return NO;
}

- (BOOL)finishRecording {
	if (self.recording) {
		[self.writer endSessionAtSourceTime:[self elapsedCMTime]];
		[self.writerInput markAsFinished];
		self.recording = NO;
	}
	[NSThread detachNewThreadSelector:@selector(finishWriting) toTarget:self.writer withObject:nil];
	return YES;
}

- (void)addFrameFrom:(CVPixelBufferRef)frame {
	if ([self.writerInput isReadyForMoreMediaData]) {
		CVPixelBufferLockBaseAddress(frame, 0);
		[self.writerInputAdaptor appendPixelBuffer:frame withPresentationTime:[self elapsedCMTime]];
		CVPixelBufferUnlockBaseAddress(frame, 0);
	}
}

@end













