//
//  NSMutableArray+Queue.h
//  Shot
//
//  Created by Anderson Lucas C. Ramos on 9/6/11.
//  Copyright 2011 Tangivel Tecnologia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ITQueue : NSObject {
@protected
	NSInteger _count;
}

@property (nonatomic, getter = count) NSInteger count;

- (id)init;
- (id)dequeue;
- (void)enqueue:(id)object;
- (id)objectAtIndex:(NSInteger)index;

@end

@interface ITMutableQueue : ITQueue 

- (id)removeObject:(id)object;

@end
