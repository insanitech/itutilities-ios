//
//  TTDateFormatter.h
//  Shot
//
//  Created by Anderson Lucas C. Ramos on 12/21/11.
//  Copyright (c) 2011 Tangível Tecnologia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ITDateFormatter : NSObject

+ (NSString *)weekDayForDate:(NSDate *)date;
+ (NSString *)weekPriorityDayForDate:(NSDate *)date;
+ (NSString *)localizedDateStringForDate:(NSDate *)date;
+ (NSString *)hoursForDate:(NSDate *)date;
+ (NSString *)dateStringForDate:(NSDate *)date;
+ (NSString *)shortDateForDate:(NSDate *)date;
+ (NSDictionary *)howLateForDate:(NSDate *)date;

@end
